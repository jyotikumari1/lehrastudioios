//
//  AboutViewController.h
//  eLehra
//
//  Created by Jukka Rauhala on 28/05/15.
//  Copyright (c) 2015 AK Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController
- (IBAction)backPressed:(id)sender;
- (IBAction)tcPressed:(id)sender;

@end
