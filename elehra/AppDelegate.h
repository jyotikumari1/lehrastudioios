//
//  AppDelegate.h
//  eLehra
//
//  Created by Aman Kalyan on 21/04/2015.
//  Copyright (c) 2015 AK Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "AudioProcessor.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
//@property (strong, nonatomic) AudioProcessor* audioProcessor;
@property (strong, nonatomic) NSMutableDictionary* lehraSettings;
@property (strong, nonatomic) NSObject* mainViewController;
@property (strong, nonatomic) NSDictionary* lehraData;

@end

