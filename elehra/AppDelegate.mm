//
//  AppDelegate.m
//  eLehra
//
//  Created by Aman Kalyan on 21/04/2015.
//  Copyright (c) 2015 AK Studios. All rights reserved.
//

#import "AppDelegate.h"
#import "Flurry.h"
#import "ViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [Flurry startSession:@"3GWD5V3J48CVCN4THFYP"];
    //self.lehraSettings = [[NSMutableDictionary alloc] init];
    
    NSURL *defaultPrefsFile = [[NSBundle mainBundle]
                               URLForResource:@"DefaultPreferences" withExtension:@"plist"];
    NSDictionary *defaultPrefs =
    [NSDictionary dictionaryWithContentsOfURL:defaultPrefsFile];
    [[NSUserDefaults standardUserDefaults] registerDefaults:defaultPrefs];
    
    self.lehraSettings = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"lehraSettings"] mutableCopy];
    
    
/*
        NSURL *defaultPrefsFile = [[NSBundle mainBundle]
                                   URLForResource:@"Data_paid" withExtension:@"plist"];
        
#else*/
#ifdef LEHRA_PRO
        NSURL *dataPrefsFile = [[NSBundle mainBundle]
                                   URLForResource:@"data_pro" withExtension:@"plist"];
#else
#ifdef LEHRA_FREE
    NSURL *dataPrefsFile = [[NSBundle mainBundle]
                            URLForResource:@"data_free" withExtension:@"plist"];
#else
    NSURL *dataPrefsFile = [[NSBundle mainBundle]
                            URLForResource:@"data" withExtension:@"plist"];
#endif
#endif
    //#endif
        self.lehraData =
        [NSDictionary dictionaryWithContentsOfURL:dataPrefsFile];
    
        
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DismissTime" object:nil];
    [self saveSettings];
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"dismissSound" object:nil];
    
}

- (void) saveSettings {
    
    if (self.mainViewController != nil)
    {
        ViewController* vc = (ViewController*)self.mainViewController;
        /*pitchIndex = [[appDelegate.lehraSettings objectForKey:@"TunerSemitone"] integerValue];
        pitchFinetune =[[appDelegate.lehraSettings objectForKey:@"FineTunerValue"] integerValue];
        bmpValue =[[appDelegate.lehraSettings objectForKey:@"Tempo"] integerValue];
        lehraVolume =[[appDelegate.lehraSettings objectForKey:@"LehraVolume"] floatValue];
        tanpuraVolume =[[appDelegate.lehraSettings objectForKey:@"TanpuraVolume"] floatValue];
        upperCatIndex =[[appDelegate.lehraSettings objectForKey:@"SelectedInstrument"] integerValue];
        middleCatIndex =[[appDelegate.lehraSettings objectForKey:@"SelectedTaal"] integerValue];
        lowerCatIndex =[[appDelegate.lehraSettings objectForKey:@"SelectedRaag"] integerValue];*/
        self.lehraSettings = [[NSMutableDictionary alloc] init];
        [self.lehraSettings setValue:[NSString stringWithFormat:@"%d",vc.pitchIndex] forKey:@"TunerSemitone"];
        [self.lehraSettings setValue:[NSString stringWithFormat:@"%d",vc.pitchFinetune] forKey:@"FineTunerValue"];
        [self.lehraSettings setValue:[NSString stringWithFormat:@"%d",vc.bmpValue] forKey:@"Tempo"];
        [self.lehraSettings setValue:[NSString stringWithFormat:@"%f",vc.lehraVolume] forKey:@"LehraVolume"];
        [self.lehraSettings setValue:[NSString stringWithFormat:@"%f",vc.tanpuraVolume] forKey:@"TanpuraVolume"];
        [self.lehraSettings setValue:[NSString stringWithFormat:@"%f",vc.metronomeVolume] forKey:@"MetronomeVolume"];
        [self.lehraSettings setValue:[NSString stringWithFormat:@"%d",vc.upperCatIndex] forKey:@"SelectedInstrument"];
        [self.lehraSettings setValue:[NSString stringWithFormat:@"%d",vc.middleCatIndex] forKey:@"SelectedTaal"];
        [self.lehraSettings setValue:[NSString stringWithFormat:@"%d",vc.lowerCatIndex] forKey:@"SelectedRaag"];

        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:self.lehraSettings forKey:@"lehraSettings"];
        [defaults synchronize];
        
        // stop audio engine if we are not playing
        if (!vc.isPlaying && vc->audioProcessor != nil)
        {
            [vc->audioProcessor StopAudio];
        }
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
   // [[NSNotificationCenter defaultCenter] postNotificationName:@"activeForeground" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ActiveForeground" object:nil];
    ViewController* vc = (ViewController*)self.mainViewController;
    if (!vc.isPlaying && vc->audioProcessor != nil)
    {
        [vc->audioProcessor StartAudio];
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    self.lehraSettings = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"lehraSettings"];
    // start audio engine if we are not playing


}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [self saveSettings];
}

@end
