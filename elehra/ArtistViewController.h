//
//  ArtistViewController.h
//  eLehra
//
//  Created by Jukka Rauhala on 28/05/15.
//  Copyright (c) 2015 AK Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArtistViewController : UIViewController
- (IBAction)backPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *artistImage;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@end
