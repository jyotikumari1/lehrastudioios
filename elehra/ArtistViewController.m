//
//  ArtistViewController.m
//  eLehra
//
//  Created by Jukka Rauhala on 28/05/15.
//  Copyright (c) 2015 AK Studios. All rights reserved.
//

#import "ArtistViewController.h"

@interface ArtistViewController ()

@end

@implementation ArtistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //self.contentView.frame = CGRectMake(0,0,self.scrollView.frame.size.width, self.scrollView.frame.size.width*47.0/13.0);
    //[self.artistImage setFrame:CGRectMake(0,0,self.contentView.frame.size.width,self.contentView.frame.size.height)];
    //[self.scrollView setContentOffset: CGPointMake(0, self.scrollView.contentOffset.y)];
    //self.scrollView.directionalLockEnabled = YES;
    //self.scrollView.contentSize = self.contentView.frame.size;
    
    //tipScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(20, 10, 280, 1200)];
    self.scrollView.showsVerticalScrollIndicator = YES;
    self.scrollView.scrollEnabled = YES;
    self.scrollView.userInteractionEnabled = YES;
    
 // UIImageView* image = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.width*47.0/13.0)];
    UIImageView* image = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.width*61.0/13.0)];
    image.image = [UIImage imageNamed:@"artists"];
    [self.scrollView addSubview:image];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:image
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.scrollView
                                                     attribute:NSLayoutAttributeLeft
                                                    multiplier:1
                                                      constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:image
                                                      attribute:NSLayoutAttributeTop
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:self.scrollView
                                                      attribute:NSLayoutAttributeTop
                                                     multiplier:1
                                                       constant:0]];    self.scrollView.contentSize = image.frame.size;

    //[self.view addSubview:tipScroll];
}

- (void)updateViewConstraints {  // for view controllers, use -updateViewConstraints
    

    [super updateViewConstraints];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backPressed:(id)sender {
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"ArtistPressed"];
    [[NSUserDefaults standardUserDefaults] synchronize];
[self dismissViewControllerAnimated:YES completion:nil];
}
@end
