//
//  TcViewController.h
//  eLehra
//
//  Created by Jukka Rauhala on 29/06/15.
//  Copyright (c) 2015 AK Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TcViewController : UIViewController
- (IBAction)backButton:(id)sender;

@end
