//
//  UpgradeViewController.h
//  eLehra
//
//  Created by Ngamacmini8 on 04/08/16.
//  Copyright © 2016 AK Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>


@interface UpgradeViewController : UIViewController<SKProductsRequestDelegate,SKPaymentTransactionObserver>
{

    SKProductsRequest *productsRequest;
    NSArray *validProducts;
    UIActivityIndicatorView *activityIndicatorView;
    
    
    IBOutlet UIButton *buyButton;
    IBOutlet UIButton *restoreButton;
    IBOutlet UIButton *purchasedButton;
    
    
    
    IBOutlet UILabel *productTitleLabel;
    IBOutlet UILabel *productDescriptionLabel;
    IBOutlet UILabel *productPriceLabel;
    IBOutlet UIButton *purchaseButton;
    
   
}
- (IBAction)purchase:(id)sender;
- (IBAction)restoreAction:(id)sender;
- (IBAction)backPressed:(id)sender;

- (void)fetchAvailableProducts;
- (BOOL)canMakePurchases;
- (void)purchaseMyProduct:(SKProduct*)product;



@end
