//
//  UpgradeViewController.m
//  eLehra
//
//  Created by Ngamacmini8 on 04/08/16.
//  Copyright © 2016 AK Studios. All rights reserved.
//

#import "UpgradeViewController.h"

#define kProductID @"com.lehrastudiopro"

@interface UpgradeViewController ()

@end

@implementation UpgradeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    purchasedButton.hidden = YES;
    // Adding activity indicator
    activityIndicatorView = [[UIActivityIndicatorView alloc]
                             initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicatorView.center = buyButton.center;
    [activityIndicatorView hidesWhenStopped];
    [buyButton.superview insertSubview:activityIndicatorView aboveSubview:buyButton];
    //    [self.view addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    //Hide purchase button initially
    purchaseButton.hidden = YES;
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *upgraded_app_status = [prefs stringForKey:@"Upgraded"];
    if (upgraded_app_status == nil){
        [self fetchAvailableProducts];
    }
    else{
        [activityIndicatorView stopAnimating];
        [self paymentSucceeded];
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


-(void)fetchAvailableProducts{
    //    NSSet *productIdentifiers = [NSSet
    //                                 setWithObjects:kProductID, nil];
    //    productsRequest = [[SKProductsRequest alloc]
    //                       initWithProductIdentifiers:productIdentifiers];
    productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:kProductID]];
    productsRequest.delegate = self;
    [productsRequest start];
}

- (BOOL)canMakePurchases
{
    return [SKPaymentQueue canMakePayments];
}
- (void)purchaseMyProduct:(SKProduct*)product{
    if ([self canMakePurchases]) {
        SKPayment *payment = [SKPayment paymentWithProduct:product];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }
    else{
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                  @"Purchases are disabled in your device" message:nil delegate:
                                  self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alertView show];
    }
}
-(IBAction)purchase:(id)sender{
    [self purchaseMyProduct:[validProducts objectAtIndex:0]];
    //    purchaseButton.enabled = NO;
}
- (void)failedTransaction:(SKPaymentTransaction *)transaction {
    
    NSLog(@"error code = %ld", transaction.error.code);
    
    // If IAP fails, display an alertView.
    NSString *title = @"Purchase Failed";
    NSString *messageString = @"In-app Purchased failed. Could not contact iTunes store or iTunes login not valid";
    
    if (transaction.error.code != SKErrorPaymentCancelled) {
        UIAlertView *iapFailedAlertView = [[UIAlertView alloc] initWithTitle:title message:messageString delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [iapFailedAlertView show];
        iapFailedAlertView = nil;
    }
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    NSLog(@"received restored transactions: %lu", (unsigned long)queue.transactions.count);
    if (queue.transactions.count>0){
        for(SKPaymentTransaction *transaction in queue.transactions){
            if(transaction.transactionState == SKPaymentTransactionStateRestored){
                //called when the user successfully restores a purchase
                NSLog(@"Transaction state -> Restored");
                
                //if you have more than one in-app purchase product,
                //you restore the correct product for the identifier.
                //For example, you could use
                //if(productID == kRemoveAdsProductIdentifier)
                //to get the product identifier for the
                //restored purchases, you can use
                //NSString *productID = transaction.payment.productIdentifier;
                
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                [self paymentSucceeded];
                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                          @"Product restored succesfully." message:nil delegate:
                                          self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alertView show];
                
                break;
            }
        }
    }
    else{
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                  @"No Products to be restored." message:nil delegate:
                                  self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alertView show];
    }
}


- (void)paymentSucceeded{
    buyButton.hidden = YES;
    restoreButton.hidden = YES;
    purchasedButton.hidden = NO;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *upgraded_app_status = [prefs stringForKey:@"Upgraded"];
    if (upgraded_app_status == nil){
        [self saveValuePreference:@"Upgraded" value:@"true"];
        [self saveValuePreference:@"ShortTimer" value:@"Completed"];
    }
}

- (IBAction)restoreAction:(id)sender {
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (IBAction)backPressed:(id)sender {

    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark StoreKit Delegate

-(void)paymentQueue:(SKPaymentQueue *)queue
updatedTransactions:(NSArray *)transactions {
    for (SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchasing:
                NSLog(@"Purchasing");
                break;
            case SKPaymentTransactionStatePurchased:
                if ([transaction.payment.productIdentifier
                     isEqualToString:kProductID]) {
                    NSLog(@"Purchased ");
                    [self paymentSucceeded];
                    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                              @"Purchase is completed succesfully" message:nil delegate:
                                              self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                    [alertView show];
                    
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                if ([transaction.payment.productIdentifier
                     isEqualToString:kProductID]) {
                    NSLog(@"Restored ");
                    [self paymentSucceeded];
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                
                break;
            case SKPaymentTransactionStateFailed:
                NSLog(@"Purchase failed ");
                [self failedTransaction:transaction];
                break;
            default:
                break;
        }
    }
}


-(NSString *)getPrice:(SKProduct *)product{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numberFormatter setLocale:product.priceLocale];
    NSString *priceString = [numberFormatter stringFromNumber:product.price];
    return priceString;
}

-(void)productsRequest:(SKProductsRequest *)request
    didReceiveResponse:(SKProductsResponse *)response
{
    SKProduct *validProduct = nil;
    int count = [response.products count];
    if (count>0) {
        validProducts = response.products;
        
        
        validProduct = [response.products objectAtIndex:0];
        NSLog(@"Found product: %@ %@ %0.2f",
              validProduct.productIdentifier,
              validProduct.localizedTitle,
              validProduct.price.floatValue);
        if ([validProduct.productIdentifier
             isEqualToString:kProductID]) {
            
            NSString *priceStr = [self getPrice:validProduct];
            if (priceStr) {
                [buyButton setTitle:priceStr forState:UIControlStateNormal];
            }else{
                [buyButton setTitle:[NSString stringWithFormat:
                                     @"Buy: ₹%@",validProduct.price] forState:UIControlStateNormal];
            }
        }
    } else {
        UIAlertView *tmp = [[UIAlertView alloc]
                            initWithTitle:@"Not Available"
                            message:@"No products to purchase"
                            delegate:self
                            cancelButtonTitle:nil
                            otherButtonTitles:@"Ok", nil];
        [tmp show];
    }
    [activityIndicatorView stopAnimating];
    purchaseButton.hidden = NO;
}

- (void) saveValuePreference:(NSString *)key value:(NSString*)value {
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    if (standardUserDefaults) {
        [standardUserDefaults setObject:value forKey:key];
        [standardUserDefaults synchronize];
    }
}

- (NSString*) getValuePreference:(NSString *)key{
    NSString *valueForKey = @"";
    NSUserDefaults *data = [NSUserDefaults standardUserDefaults];
    NSString *myString = [data objectForKey:key];
    if(myString != nil){
        //object is there
        valueForKey = myString;
    }
    return valueForKey;
}

@end
