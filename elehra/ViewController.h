#import <UIKit/UIKit.h>
#import "AudioProcessorData.h"
#import "AudioProcessor.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import <StoreKit/StoreKit.h>
@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, SKPaymentTransactionObserver,SKProductsRequestDelegate>
{
    NSTimer *viewUpdater;
    NSTimer *tempoButtonTimer;
    NSTimer *screenSaverTimer;
    
    NSDate* tempoButtonTimerStart;
    AudioProcessorData* audioData;
    int tempoMin[MAX_INSTRUMENT*MAX_TAAL*MAX_RAAG];
    int tempoMax[MAX_INSTRUMENT*MAX_TAAL*MAX_RAAG];
    
#ifdef LEHRA_FREE
    NSTimer *freeAdTimer;
#endif
    @public
    AudioProcessor* audioProcessor;
}
@property (nonatomic, retain) IBOutlet UISlider *bmpSlider;
@property (nonatomic, retain) IBOutlet UISlider *pitchSlider;
@property (nonatomic, retain) IBOutlet UISlider *slider3;
@property (nonatomic, retain) IBOutlet UISlider *slider4;
@property (nonatomic, retain) IBOutlet UISlider *slider5;
@property (nonatomic, retain) IBOutlet UIButton *playBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lehraMenuConstraint;
@property (weak, nonatomic) IBOutlet UITableView *lehraMenuTable;
@property (weak, nonatomic) IBOutlet UIView *lehraMenuView;
@property (weak, nonatomic) IBOutlet UISlider *metronomeSlider;
- (IBAction)metronomeVolumeChanged:(id)sender;
    
@property (nonatomic, retain) IBOutlet UILabel *bmpValueLbl;
@property (nonatomic) int bmpValue;

@property (nonatomic, retain) IBOutlet UILabel *pitchSymbolLbl;
@property (nonatomic, retain) IBOutlet UITextField *pitchFreqLbl;
- (IBAction)plusButtonTouchUp:(id)sender;
- (IBAction)plusButtonTouchDown:(id)sender;
- (IBAction)minusButtonTouchDown:(id)sender;

@property (nonatomic, retain) IBOutlet UITextField *upperCatLbl;
@property (nonatomic, retain) IBOutlet UITextField *lowerCatLbl;
@property (nonatomic, retain) IBOutlet UITextField *middleCatLbl;
@property (weak, nonatomic) IBOutlet UIButton *plusButton;
- (IBAction)minusButtonTouchUp:(id)sender;
- (IBAction)plusButtonDoubleTap:(id)sender;
- (IBAction)minusButtonDoubleTap:(id)sender;

//@property (nonatomic, retain) NSArray *catRecords;
@property (nonatomic, retain) NSMutableArray *upperCatRecord;
@property (nonatomic, retain) NSMutableArray *lowerCatRecord;
@property (nonatomic, retain) NSMutableArray *middleCatRecord;
@property (nonatomic, retain) NSMutableArray *middleCatSortRecord;

@property (nonatomic, retain) NSMutableArray *lowerCatFirstRecord;
@property (nonatomic, retain) NSMutableArray *middleCatFirstRecord;


@property (nonatomic) int upperCatIndex;
@property (nonatomic) int lowerCatIndex;
@property (nonatomic) int middleCatIndex;
@property (weak, nonatomic) IBOutlet UIView *menuSinkView;
@property (unsafe_unretained, nonatomic) IBOutlet NSLayoutConstraint *playbarLeading;
@property (unsafe_unretained, nonatomic) IBOutlet NSLayoutConstraint *playbarTop;

@property (nonatomic) int upperCatCount;
@property (nonatomic) int lowerCatCount;
@property (nonatomic) int middleCatCount;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *menuViewTopConstraint;
@property (weak, nonatomic) IBOutlet UITextField *centLabel;


@property (nonatomic, retain) NSArray *pitchRecords;
@property (nonatomic, retain) NSArray *pitchFreqs;
@property (nonatomic) int pitchIndex;
@property (nonatomic) float pitchFreq;
@property (nonatomic) float pitchCoeff;
@property (nonatomic) int pitchFinetune;
- (IBAction)aboutPressed:(id)sender;
- (IBAction)artistPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *menuView;
- (IBAction)menuPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *volumeDisplay;
@property (weak, nonatomic) IBOutlet UILabel *beatLabel;


@property (nonatomic) float lehraVolume;
@property (nonatomic) float tanpuraVolume;
@property (nonatomic) float metronomeVolume;
- (IBAction)lehraVolumeChanged:(id)sender;
- (IBAction)tanpuraVolumeChanged:(id)sender;


@property (nonatomic) BOOL isPlaying;
@property (nonatomic) BOOL isPaused;
@property (nonatomic) BOOL isAbout;
@property (nonatomic) BOOL isArtist;
@property (nonatomic) long screenSaverCounter;
@property (nonatomic) long lastEvent;
@property (nonatomic) BOOL isTapTempoOn;
@property (nonatomic) UInt64 lastTapTempoEvent;
@property (nonatomic) UInt64 maxTapTempoInterval;
@property (nonatomic) UInt64 minTapTempoInterval;
@property (nonatomic, retain) NSMutableArray *tapTempoEvents;
@property (weak, nonatomic) IBOutlet UIButton *plusBtn;
@property (weak, nonatomic) IBOutlet UIButton *minusBtn;
@property (weak, nonatomic) IBOutlet UIButton *peviousPitchBtn;
@property (weak, nonatomic) IBOutlet UIButton *nextPitchBtn;

-(IBAction) plusBMP:(id)sender;
-(IBAction) minusBMP:(id)sender;
-(IBAction) bmpSliderValueChanged:(id)sender;

-(IBAction) plusPitch:(id)sender;
-(IBAction) minusPitch:(id)sender;
-(IBAction) pitchSliderValueChanged:(id)sender;

-(IBAction) nextPitch:(id)sender;
-(IBAction) previousPitch:(id)sender;
- (IBAction)purchasedClicked:(id)sender;

-(IBAction) playPressed:(id)sender;

-(IBAction) upperPressed:(id)sender;
-(IBAction) lowerPressed:(id)sender;
-(IBAction) middlePressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *screenSaverView;
@property (weak, nonatomic) IBOutlet UILabel *screenSaverBpmLabel;
@property (weak, nonatomic) IBOutlet UILabel *screenSaverMatraLabel;
- (IBAction)tapTempoPressed:(id)sender;
- (IBAction)onScreenSaverTapped:(id)sender;
//Timer Changes Start
@property (nonatomic, weak) NSTimer *timer;
@property (nonatomic, weak) NSTimer *shortTimer;
@property (nonatomic) BOOL onCreateCalled;
@property (nonatomic) long long FIVE_DAYS_FOR_TIMER;
@property (nonatomic) long ONE_MINUTE;
@property (nonatomic) BOOL timerStart;
//Timer Changes End

@end

