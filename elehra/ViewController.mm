#import "ViewController.h"
#import "AudioProcessor.h"
#import "AppDelegate.h"
#import "AESTool.h"
#import "FBEncryptorAES.h"
#import "GTMBase64.h"

@interface ViewController ()
{
    int defMiddleCatIndex;
}

@end

@implementation ViewController

@synthesize bmpSlider, pitchSlider, slider3, slider4, slider5, bmpValueLbl, bmpValue, pitchIndex, pitchRecords, pitchFreqLbl, pitchSymbolLbl, pitchFreq, pitchFreqs,playBtn, isPlaying, isPaused, isAbout, isArtist, upperCatIndex, upperCatLbl, upperCatRecord, lowerCatIndex, lowerCatLbl, lowerCatRecord, middleCatIndex, middleCatLbl, middleCatRecord, middleCatCount, lowerCatCount, upperCatCount, pitchCoeff,lehraVolume,tanpuraVolume,centLabel,pitchFinetune,lehraMenuTable,middleCatSortRecord,lowerCatFirstRecord,middleCatFirstRecord,screenSaverBpmLabel,screenSaverMatraLabel,screenSaverCounter,lastEvent,isTapTempoOn,lastTapTempoEvent,maxTapTempoInterval,minTapTempoInterval,tapTempoEvents,metronomeVolume;

#define LEHRA_MAIN_MENU 900
#define LEHRA_INSTRUMENT_MENU 901
#define LEHRA_RAAG_MENU 902
#define LEHRA_TAAL_MENU 903
#define pitchIndexMaxValue 6
#define pitchIndexMinValue 0

#define kProductID @"com.lehrastudiopro"

float centTable[201];

long long FIVE_BACKUP_MINUTE_TIMER;
long long TWO_BACKUP_MINUTE_TIMER;
long long TWO_MINUTE_TIMER;
long TWO_MINUTE_TIMER_;
bool inventoryDetailsStarted = FALSE;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Load the receipt from the app bundle.
   
   // if (!receipt) {
        /* No local receipt -- handle the error.  }
    
     ... Send the receipt data to your server ... */

    
    
    _onCreateCalled = YES;
    isPaused = YES;
    //Timer Changes Start
    // 2 min timer changed to 1 minute and 5 days trial to 3 days
    
    TWO_MINUTE_TIMER = 1 * 60 * 1000;
    TWO_MINUTE_TIMER_ = 1 * 60 * 1000;

    _ONE_MINUTE = 1 * 60 * 1000;
    
    
    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    NSLog(@"receiptUrl %@",[receiptURL path]);
    NSError* err = nil;
    if (![receiptURL checkResourceIsReachableAndReturnError:&err]){
        SKReceiptRefreshRequest* request = [[SKReceiptRefreshRequest alloc] initWithReceiptProperties:nil];
        request.delegate = self;
        [request start];
    }

    
    //Timer Changes End
    [self saveValuePreference:@"isPaused" value:@"Yes"];
    
   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(DismissTime) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ActiveForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
  //  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismissAudioSound) name:@"dismissSound" object:nil];
    
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ActivateForeground) name:@"activeForeground" object:nil];


    minTapTempoInterval = 60000 / 500;
    maxTapTempoInterval = 60000 / 30;

    tapTempoEvents = [[NSMutableArray alloc] init];
    isPlaying = NO;
    AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    pitchIndex = (int)[[appDelegate.lehraSettings objectForKey:@"TunerSemitone"] integerValue];
    pitchFinetune =(int)[[appDelegate.lehraSettings objectForKey:@"FineTunerValue"] integerValue];
    bmpValue =(int)[[appDelegate.lehraSettings objectForKey:@"Tempo"] integerValue];
    lehraVolume =[[appDelegate.lehraSettings objectForKey:@"LehraVolume"] floatValue];
    tanpuraVolume =[[appDelegate.lehraSettings objectForKey:@"TanpuraVolume"] floatValue];
    metronomeVolume =[[appDelegate.lehraSettings objectForKey:@"MetronomeVolume"] floatValue];
    upperCatIndex =(int)[[appDelegate.lehraSettings objectForKey:@"SelectedInstrument"] integerValue];
    middleCatIndex =(int)[[appDelegate.lehraSettings objectForKey:@"SelectedTaal"] integerValue];
    lowerCatIndex =(int)[[appDelegate.lehraSettings objectForKey:@"SelectedRaag"] integerValue];
    
    
    
    appDelegate.mainViewController = self;

    lehraMenuTable.delegate = self;
    lehraMenuTable.dataSource = self;
    
    lastEvent = 0;
    screenSaverCounter = 0;
    
    //int tempoMax_[] = {240,240,120,120,120,150,120,150};
    //int tempoMin_[] = {30,110,30,30,30,70,30,70};
    //memcpy(&tempoMax[0],&tempoMax_[0],sizeof(int)*8);
    //memcpy(&tempoMin[0],&tempoMin_[0],sizeof(int)*8);
    audioProcessor = [[AudioProcessor alloc] init];
    bool areFilesDecoded = [audioProcessor Initialize];
    audioData = audioProcessor->data;
    audioProcessor->dataWrapper->parentInstance = self;

    
    // Load settings for files
    NSArray* instruments = appDelegate.lehraData.allKeys;
    upperCatRecord = [[NSMutableArray alloc] init];
    middleCatRecord= [[NSMutableArray alloc] init];
    lowerCatRecord= [[NSMutableArray alloc] init];
    middleCatSortRecord= [[NSMutableArray alloc] init];
    middleCatFirstRecord= [[NSMutableArray alloc] init];
    lowerCatFirstRecord= [[NSMutableArray alloc] init];
    
    for (int i=0; i < instruments.count; i++)
    {
        NSString* instrument =[instruments objectAtIndex:i];
        [upperCatRecord addObject:[instrument uppercaseString]];
        NSDictionary* instrumentData = [appDelegate.lehraData objectForKey:instrument];
        NSDictionary* tData = [instrumentData objectForKey:@"Taals"];
        NSString* tuningCoeff = [instrumentData objectForKey:@"TuningCoeff"];
        audioData->tuningCoeffs[i] = [tuningCoeff floatValue];
        NSArray* taals = tData.allKeys;
        NSMutableArray* taalArray = [[NSMutableArray alloc] init];
        NSMutableArray* lowerTaalArray = [[NSMutableArray alloc] init];
        NSMutableArray* sortedTaalIndexArray = [[NSMutableArray alloc] init];
        NSMutableArray* beatsArray = [[NSMutableArray alloc] init];
        NSMutableArray* firstRaagArray = [[NSMutableArray alloc] init];
        
        int firstTaal = -1;
        
        for (int t=0; t < taals.count; t++)
        {
            NSString* taal =[taals objectAtIndex:t];
            NSDictionary* taalData = [tData objectForKey:taal];
            NSNumber *isHidden = @NO;
            NSString* o = [taalData objectForKey:@"Hidden"];
            if (o != nil && [o boolValue])
            {
                isHidden = @YES;
            }
            if(![isHidden boolValue] && firstTaal == -1)
                firstTaal = t;
            [taalArray addObject:@{@"Name":[taal uppercaseString],@"Hidden":isHidden}];//[raag
            //[taalArray addObject:[taal uppercaseString]];
            NSArray* bpms = [taalData objectForKey:@"Tempos"];
            NSString* beats = [taalData objectForKey:@"Beats"];
            
            [beatsArray addObject:[NSNumber numberWithFloat:[beats floatValue]+(float)t*0.01f ]];
            NSString* maxtempo = [taalData objectForKey:@"MaxTempo"];
            NSString* mintempo = [taalData objectForKey:@"MinTempo"];
            int imaxtempo = (int)[maxtempo integerValue];
            int imintempo = (int)[mintempo integerValue];
            audioData->beatsPerFile[i*MAX_TAAL+t] = (int)[beats integerValue];
            
            NSString* cycles = [taalData objectForKey:@"Cycles"];
            if (cycles != nil)
                audioData->cyclesPerFile[i*MAX_TAAL+t] = (int)[cycles integerValue];
            else
                audioData->cyclesPerFile[i*MAX_TAAL+t] = 1;
            
            NSDictionary* rData = [taalData objectForKey:@"Raags"];
            NSArray* raags = rData.allKeys;
            NSMutableArray* raagArray = [[NSMutableArray alloc] init];
            int firstRaag = -1;
            for (int r=0; r < raags.count; r++)
            {
                NSString* raag =[raags objectAtIndex:r];
                 NSDictionary* raagData = [rData objectForKey:[raags objectAtIndex:r]];
                NSNumber *isHidden = @NO;
                NSString* o = [raagData objectForKey:@"Hidden"];
                if (o != nil && [o boolValue])
                {
                    isHidden = @YES;
                }
                if(![isHidden boolValue] && firstRaag == -1)
                    firstRaag = r;
                [raagArray addObject:@{@"Name":[raag uppercaseString],@"Hidden":isHidden}];//[raag uppercaseString]];
               NSArray* rbpms = [raagData objectForKey:@"Tempos"];
                NSString* maxtempo = [raagData objectForKey:@"MaxTempo"];
                NSString* mintempo = [raagData objectForKey:@"MinTempo"];
                int rmaxtempo = imaxtempo;
                int rmintempo = imintempo;
                if (rbpms != nil && maxtempo != nil && mintempo != nil)
                {
                    rmaxtempo = (int)[maxtempo integerValue];
                    rmintempo = (int)[mintempo integerValue];
                } else {
                    rbpms = bpms;
                }
                tempoMax[BPM(i,t,r)]=rmaxtempo;
                tempoMin[BPM(i,t,r)]=rmintempo;
                audioData->maxBpmValues[BPM(i,t,r)]=(int)rbpms.count;
                for (int k=0; k < rbpms.count; k++)
                {
                    int ii =BPM(i,t,r);
                    audioData->bpmValues[ii][k] = (int)[[rbpms objectAtIndex:k] integerValue];
                    if (k >0 )
                        audioData->bpmEdges[ii][k-1] =audioData->bpmValues[ii][k-1]+(int)(0.5*(audioData->bpmValues[ii][k]-audioData->bpmValues[ii][k-1]));
                }
                
                
            }
            [lowerTaalArray addObject:raagArray];
            [firstRaagArray addObject:[NSNumber numberWithInt:firstRaag]];
        }
        [middleCatFirstRecord addObject:[NSNumber numberWithInt:firstTaal]];
        [lowerCatFirstRecord addObject:firstRaagArray];
        NSArray* a = [beatsArray sortedArrayUsingSelector: @selector(compare:)];
        for (int b=0; b < a.count; b++)
        {
            
            [sortedTaalIndexArray addObject:[NSNumber numberWithInteger:[beatsArray indexOfObject:[a objectAtIndex:b]]]];
//            [sortedTaalIndexArray addObject:[NSNumber numberWithInteger:[a indexOfObject:[beatsArray objectAtIndex:b]]]];

        }
        
        [middleCatRecord addObject:taalArray];
        [lowerCatRecord addObject:lowerTaalArray];
        [middleCatSortRecord addObject:sortedTaalIndexArray];
    }
    
    [self checkForDefaultCat];
    
    
    if (areFilesDecoded)
    {
        [audioProcessor LoadFiles];
        [audioProcessor StartAudio];

        
    } else {
        // We need to decode the ogg files from the binary and store them into the Library folder
        NSLog(@"Decode files...");
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [audioProcessor DecodeFiles];
            [audioProcessor LoadFiles];
            [audioProcessor StartAudio];
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Decode ended.");
            });
        });
    }
#ifdef USE_TAAE
    [audioProcessor setMetronomeVolume:metronomeVolume];
#endif

#ifdef LEHRA_FREE
    int targetMiddleCatIndex = (int)[[middleCatFirstRecord objectAtIndex:upperCatIndex] integerValue];
    if (middleCatIndex != targetMiddleCatIndex)
        middleCatIndex = targetMiddleCatIndex;
    int targetLowerCatIndex = (int)[[[lowerCatFirstRecord objectAtIndex:upperCatIndex] objectAtIndex:middleCatIndex] integerValue];
    if(lowerCatIndex != targetLowerCatIndex)
        lowerCatIndex = targetLowerCatIndex;
    
#endif
    
    [self calculationForInstrumentRecord];
    [self updateInstrumentCatRelatedUI];
    [self initPitchRecordsAndUI];
    
    [self initBMPRecordsAndUI];
    
    [self initOtherUI];
    
    [self initInstrumentRecordsAndUI];

    self.lehraMenuView.layer.shadowOffset = CGSizeMake(1, 1);
    self.lehraMenuView.layer.shadowRadius = 32;
    self.lehraMenuView.layer.shadowOpacity = 0.15;
    
    self.menuView.layer.shadowOffset = CGSizeMake(1, 1);
    self.menuView.layer.shadowRadius = 32;
    self.menuView.layer.shadowOpacity = 0.15;
    //self.menuView.layer.borderWidth = 1;
    //self.menuView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    //self.menuView.layer.cornerRadius = 12;

    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSinkTap)];
    [self.menuSinkView addGestureRecognizer:singleFingerTap];

    
    viewUpdater = [NSTimer scheduledTimerWithTimeInterval:0.05f
                                                   target:self
                                                 selector:@selector(UpdateView)
                                                 userInfo:nil
                                                  repeats:YES];
    
    [self.plusButton addTarget:self action:@selector(plusButtonTouchDown:) forControlEvents:UIControlEventTouchDown];
   /* UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(doubleTapOnPlusButton)];
    doubleTap.numberOfTapsRequired = 2;
    [self.plusButton addGestureRecognizer:doubleTap];*/
    UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bpmSliderTapped:)];
    [bmpSlider addGestureRecognizer:gr];

    screenSaverTimer = [NSTimer scheduledTimerWithTimeInterval:60.0f
                                                   target:self
                                                 selector:@selector(screenSaverTimerEvent)
                                                 userInfo:nil
                                                  repeats:YES];
}


- (void)checkForDefaultCat{
    if (middleCatIndex==-1) {
        NSArray* sortedA = ((NSArray*)[middleCatSortRecord objectAtIndex:upperCatIndex]);
        middleCatIndex = [[sortedA objectAtIndex:0] intValue];
    }
  }









-(void)DismissTime {
    NSLog(@"Time invalidate");
    if (_timer!= nil){
        [_timer invalidate];
    }
    if (_shortTimer!= nil){
        [_shortTimer invalidate];
        _timerStart = false;
        //[self updateUIShortTimerInterval];
    }
    
    
}
-(void)ActiveForeground {
    NSLog(@"Time activate");
    _timerStart = true;
    [self viewWillAppear:true];
}

-(void) getResponseFromApp{
    NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
    NSString *upgraded_app_status = [prefs stringForKey:@"Upgraded"];
    if (upgraded_app_status == nil) {
        if ([self isNetworkAvailable]) {
            [self inventoryDetails];
        } else {
            NSString *upgraded_app = [prefs stringForKey:@"Upgraded"];
            NSString *expired_status = [prefs stringForKey:@"Expired"];
            
            if (upgraded_app == nil){
                if (expired_status == nil) {
                    [self registerTimer];
                }
                else {
                    [self updateUI];
                }
            }
        }
        // }
        
    }

}
-(void)requestDidFinish:(SKRequest*)request{
    if([request isKindOfClass:[SKReceiptRefreshRequest class]]){
        
        NSLog(@"YES, You purchased this app");
        
        
        [self saveValuePreference:@"Upgraded" value:@"true"];
        
    }
}



- (void)request:(SKRequest*)request didFailWithError:(NSError *)error{
    
    
    NSLog(@"NO, you need to buy it ");
    [self getResponseFromApp];
    
}
-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
#ifdef LEHRA_FREE
/*
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"freeVersionTime"];
    if (savedValue != nil)
    {
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AdView"];
        [self presentModalViewController:vc animated:YES ];
    } else {
        freeAdTimer = [NSTimer scheduledTimerWithTimeInterval:30.0f
                                                       target:self
                                                     selector:@selector(showFreeVersionAd)
                                                     userInfo:nil
                                                      repeats:NO];
    }*/
#endif
    
}

#ifdef LEHRA_FREE
-(void) showFreeVersionAd
{
    @try {
    NSString *valueToSave = @"1";
    [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"freeVersionTime"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AdView"];
    [self presentModalViewController:vc animated:YES ];
    } @catch (NSException* e)
    {
        
    }
}
#endif

-(void) handleSinkTap
{
    lastEvent = screenSaverCounter;
    if (lehraMenuTable.tag == LEHRA_MAIN_MENU)
        [self CloseMenu];
    else
        [self CloseLehraMenu];
}

-(void) UpdateView
{
    @synchronized(self) {
#ifdef USE_TAAE
        int beats = 0;
        float vol = 0;
        [audioProcessor getMetrics:&beats :&vol ];
        beats++;
#else
        int beats = (int)(1.0f + floor(audioData->beatCounter));
    float vol = audioData->audioLevel * 0.08f;
#endif
        [self.beatLabel setText:[NSString stringWithFormat:@"%d",beats]];
        [screenSaverMatraLabel setText:[NSString stringWithFormat:@"%d",beats]];
    
        
    if (vol > 2.8f)
        vol = 2.8f;
        //NSLog(@"vol:%f",vol);
       // [self.volumeDisplay setTranslatesAutoresizingMaskIntoConstraints:YES];
   //     CGAffineTransform transform = CGAffineTransformRotate(CGAffineTransformIdentity, vol);
   // self.volumeDisplay.transform = transform;
        NSString *zRotationKeyPath = @"transform.rotation.z";
        
        // Change the model to the new "end value" (key path can work like this but properties don't)
        CGFloat currentAngle = [[self.volumeDisplay.layer valueForKeyPath:zRotationKeyPath] floatValue];
        //CGFloat angleToAdd   = vol; // 90 deg = pi/2
        //[self.volumeDisplay.layer setValue:@(angleToAdd) forKeyPath:zRotationKeyPath];
      
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:zRotationKeyPath];
        animation.duration = 1;
        // @( ) is fancy NSNumber literal syntax ...
        animation.toValue = @(vol);        // model value was already changed. End at that value
        animation.byValue = @(currentAngle); // start from - this value (it's toValue - byValue (see above))
        
        // Add the animation. Once it completed it will be removed and you will see the value
        // of the model layer which happens to be the same value as the animation stopped at.
        [self.volumeDisplay.layer addAnimation:animation forKey:@"90rotation"];
    }
  //  [self.view layoutIfNeeded];
}

-(void) CloseMenu
{
    self.menuSinkView.hidden = true;

    [UIView animateWithDuration:0.25
                     animations:^{
                         self.menuViewTopConstraint.constant = 200;
                         [self.view layoutIfNeeded];
                     }];
   // self.menuViewTopConstraint.constant = -190;
}
-(void) OpenMenu
{
    self.menuSinkView.hidden = false;

    lehraMenuTable.tag = LEHRA_MAIN_MENU;
    [UIView animateWithDuration:0.25
                     animations:^{
                         self.menuViewTopConstraint.constant = 50;
                         [self.view layoutIfNeeded];
                     }];
   // self.menuViewTopConstraint.constant = 0;
}

-(void) OpenLehraMenu
{
    self.menuSinkView.hidden = false;
    
    [lehraMenuTable reloadData];
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         self.lehraMenuConstraint.constant = -270;
                         [self.view layoutIfNeeded];
                     }];
}

-(void)CloseLehraMenu
{
    self.menuSinkView.hidden = true;
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         self.lehraMenuConstraint.constant = 150;
                         [self.view layoutIfNeeded];
                     }];
}


-(void) viewWillAppear:(BOOL)animated
{
//    self.menuView.hidden = true;
//    NSLog(@"Volume Lehra = %f",lehraVolume);
//    NSLog(@"Volume Tanpura = %f",tanpuraVolume);
//    NSLog(@"Volume Metronome = %f",metronomeVolume);

    [self CloseMenu];
    [self CloseLehraMenu];
    
    // timer changes start
    if (!_onCreateCalled) {
        //_timerStart = true;
        if ([[self getValuePreference:@"ShortTimer"]  isEqual: @"Running"]) {
            long long shortTimerStart = [[self getValuePreference:@"ShortTimerStart"] longLongValue];
            long long shortTimerEnd = [[self getValuePreference:@"ShortTimerEnd"] longLongValue];
            
            long long currentTime = (long long)([[NSDate date] timeIntervalSince1970] * 1000.0);
            long long diffInTime = currentTime - shortTimerStart;
            //2 min changed to 1 min
            if (diffInTime / _ONE_MINUTE >= 1) {
                [self saveValuePreference:@"ShortTimer" value:@"Completed"];
            } else {
                TWO_MINUTE_TIMER = shortTimerEnd - currentTime;
                [self shortTempTimer];
            }
        }
        
        if (![[self getValuePreference:@"ShortTimer"]  isEqual: @"Running"]) {
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *upgraded_app_status = [prefs stringForKey:@"Upgraded"];
            
            if (upgraded_app_status == nil) {
                if ([self isNetworkAvailable]) {
                    [self inventoryDetails];
                } else {
                    NSString *upgraded_app = [prefs stringForKey:@"Upgraded"];
                    if (upgraded_app == nil) {
                        [self registerTimer];
                    }
                }
            }
        }
    }
    _onCreateCalled = NO;
    // timer changes end
    
    
//    if ([[self getValuePreference:@"isPaused"]  isEqual: @"No"]) {
//        NSError* error = nil;
//#ifdef USE_TAAE
//        [audioProcessor startAudioEngine:error];
//#endif
//        
//    }

}
-(void) viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:(BOOL)animated];
    inventoryDetailsStarted = false;
    //Timer Changes Start
    NSLog(@"viewDidDisappear");
    isAbout = [[NSUserDefaults standardUserDefaults]boolForKey:@"AboutPressed"];
    isArtist = [[NSUserDefaults standardUserDefaults]boolForKey:@"ArtistPressed"];
    //Timer Changes Start
    NSLog(@"viewDidDisappear");
    if (isArtist) {
        NSLog(@"Artist pressed");
    }
    else if (isAbout){
        NSLog(@"About pressed");
    }
    else{
        NSLog(@"Purchase pressed or enter background");
//        if (_timer!= nil){
//                    [_timer invalidate];
//                }
//                if (_shortTimer!= nil){
//                    [_shortTimer invalidate];
//                }

        
    }
    //Timer Changes End

//    if (_timer!= nil){
//        [_timer invalidate];
//    }
//    if (_shortTimer!= nil){
//        [_shortTimer invalidate];
//    }
    //Timer Changes End
//    if ([[self getValuePreference:@"isPaused"]  isEqual: @"No"]) {
//        [playBtn setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
//        isPlaying = NO;
//
//#ifdef USE_TAAE
//        [audioProcessor stopAudioEngine];
//#endif
//        
//    }

}

-(void) initPitchRecordsAndUI {
    
    // initialize table of cent coeffs
    for (int i=0; i<201; i++)
    {
        centTable[i] = powf(2.0f,(float)(i-100)/1200.0f);
    }
        
    
    
    pitchRecords = [[NSArray alloc] initWithObjects:[[NSDictionary alloc] initWithObjectsAndKeys:@"A#", @"116.54", nil], [[NSDictionary alloc] initWithObjectsAndKeys:@"B", @"123.47", nil], [[NSDictionary alloc] initWithObjectsAndKeys:@"C", @"130.81", nil], [[NSDictionary alloc] initWithObjectsAndKeys:@" C#", @"138.59", nil], [[NSDictionary alloc] initWithObjectsAndKeys:@"D", @"146.83", nil], [[NSDictionary alloc] initWithObjectsAndKeys:@"D#", @"155.56", nil], [[NSDictionary alloc] initWithObjectsAndKeys:@"E", @"164.81", nil], nil];
    pitchFreqs =[[NSArray alloc] initWithObjects:@110.0,@116.54, @123.47, @130.81, @138.59,@146.83,@155.56,@164.81,@174.61, nil];
//pitchIndex = 4;
    NSString *key = [[[pitchRecords objectAtIndex:pitchIndex] allKeys] objectAtIndex:0];
    [pitchSymbolLbl setText:[[pitchRecords objectAtIndex:pitchIndex] objectForKey:key]];
    [pitchFreqLbl setText:[NSString stringWithFormat:@"%@Hz", key]];
    pitchFreq = [self calculatePitch:pitchFinetune];//[key floatValue];
    //pitchCoeff = 1.0f;
    pitchCoeff = 146.83f / pitchFreq;
 
    //UIImage *minImage = [UIImage imageNamed:@"volume leftbar.png"];
    //UIImage *maxImage = [UIImage imageNamed:@"volume rightbar.png"];
    UIImage *thumbImage = [UIImage imageNamed:@"sliderbutton.png"];
    
    //minImage = [minImage stretchableImageWithLeftCapWidth:10.0 topCapHeight:0.0];
    //maxImage = [maxImage stretchableImageWithLeftCapWidth:10.0 topCapHeight:0.0];
    
    //[pitchSlider setMinimumTrackImage:minImage forState:(UIControlStateNormal)];
    //[pitchSlider setMaximumTrackImage:maxImage forState:(UIControlStateNormal)];
    [pitchSlider setThumbImage:thumbImage forState:(UIControlStateNormal)];
    [pitchSlider setThumbImage:thumbImage forState:(UIControlStateHighlighted)];
    [pitchSlider setMinimumValue:0];//-1];//116.54f];
    [pitchSlider setMaximumValue:200];//1];//164.81f];
    [pitchSlider setValue:pitchFinetune];//0];//pitchFreq];
}

-(void) initBMPRecordsAndUI {
    //UIImage *minImage = [UIImage imageNamed:@"volume leftbar.png"];
    //UIImage *maxImage = [UIImage imageNamed:@"volume rightbar.png"];
    UIImage *thumbImage = [UIImage imageNamed:@"sliderbutton.png"];
    
    //minImage = [minImage stretchableImageWithLeftCapWidth:10.0 topCapHeight:0.0];
    //maxImage = [maxImage stretchableImageWithLeftCapWidth:10.0 topCapHeight:0.0];
    
    //[bmpSlider setMinimumTrackImage:minImage forState:(UIControlStateNormal)];
    //[bmpSlider setMaximumTrackImage:maxImage forState:(UIControlStateNormal)];
    [bmpSlider setThumbImage:thumbImage forState:(UIControlStateNormal)];
    [bmpSlider setThumbImage:thumbImage forState:(UIControlStateHighlighted)];
    [bmpSlider setMinimumValue:30];
    [bmpSlider setMaximumValue:240];
    //bmpValue = 75;
    [bmpSlider setValue:bmpValue ];//bmpSlider.minimumValue];
    [bmpValueLbl setText:[NSString stringWithFormat:@"%i", bmpValue]];
    [screenSaverBpmLabel setText:[NSString stringWithFormat:@"%i", bmpValue]];

}

-(void) initOtherUI {
    //UIImage *minImage = [UIImage imageNamed:@"volume leftbar.png"];
    //UIImage *maxImage = [UIImage imageNamed:@"volume rightbar.png"];
    UIImage *thumbImage = [UIImage imageNamed:@"sliderbutton.png"];
    
    //minImage = [minImage stretchableImageWithLeftCapWidth:10.0 topCapHeight:0.0];
    //maxImage = [maxImage stretchableImageWithLeftCapWidth:10.0 topCapHeight:0.0];
    
    //[slider3 setMinimumTrackImage:minImage forState:(UIControlStateNormal)];
    //[slider3 setMaximumTrackImage:maxImage forState:(UIControlStateNormal)];
    [slider3 setThumbImage:thumbImage forState:(UIControlStateNormal)];
    [slider3 setThumbImage:thumbImage forState:(UIControlStateHighlighted)];
    
    //lehraVolume = 0.5f;
    [slider3 setValue:lehraVolume];
    
    //tanpuraVolume = 0.5f;
    [slider4 setValue:tanpuraVolume];
    
    [self.metronomeSlider setValue:metronomeVolume];
    [self.metronomeSlider setThumbImage:thumbImage forState:(UIControlStateNormal)];
    [self.metronomeSlider setThumbImage:thumbImage forState:(UIControlStateHighlighted)];
    
    //[slider4 setMinimumTrackImage:minImage forState:(UIControlStateNormal)];
    //[slider4 setMaximumTrackImage:maxImage forState:(UIControlStateNormal)];
    [slider4 setThumbImage:thumbImage forState:(UIControlStateNormal)];
    [slider4 setThumbImage:thumbImage forState:(UIControlStateHighlighted)];
    
    //[slider5 setMinimumTrackImage:minImage forState:(UIControlStateNormal)];
    //[slider5 setMaximumTrackImage:maxImage forState:(UIControlStateNormal)];
    [slider5 setThumbImage:thumbImage forState:(UIControlStateNormal)];
    [slider5 setThumbImage:thumbImage forState:(UIControlStateHighlighted)];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
}

-(void) initInstrumentRecordsAndUI {
    /*upperCatRecord = [[NSArray alloc] initWithObjects:@"SARANGI",@"SANTOOR",  nil];
    middleCatRecord = [[NSArray alloc] initWithObjects:[[NSArray alloc] initWithObjects:@"TEEN TAAL",  @"ROOPAK TAAL", @"JHAP TAAL", @"EK TAAL",nil], [[NSArray alloc] initWithObjects:@"TEEN TAAL",  @"ROOPAK TAAL", @"JHAP TAAL",  @"EK TAAL",nil], nil];
    lowerCatRecord = [[NSArray alloc] initWithObjects:[[NSArray alloc] initWithObjects:[[NSArray alloc] initWithObjects:@"CHARUKESHI", @"DES", @"VACHASPATI", @"PURIYA DHANA", @"SHIVRANJANI", @"JANASAMMOHINI",nil], [[NSArray alloc] initWithObjects:@"BAIRAGI", @"BHAGESHREE", @"GARA", nil], [[NSArray alloc] initWithObjects:@"MALKAUNS", nil], [[NSArray alloc] initWithObjects:@"HEMANT", nil], nil], [[NSArray alloc] initWithObjects:[[NSArray alloc] initWithObjects:@"KIRWANI", @"DES", @"BHAIRAV", nil], [[NSArray alloc] initWithObjects:@"KIRWANI", @"DES", @"BHAIRAV", nil], [[NSArray alloc] initWithObjects:@"KIRWANI", @"DES", @"BHAIRAV", nil], [[NSArray alloc] initWithObjects:@"KIRWANI", @"DES", @"BHAIRAV", nil], nil], nil];*/
    
    //upperCatIndex = 0;
    //middleCatIndex = 0;
    //lowerCatIndex = 0;
    
    [self calculationForInstrumentRecord];
    [self updateInstrumentCatRelatedUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)checkForPitchBtns{
    int maxBpm = tempoMax[BPM(upperCatIndex,middleCatIndex,lowerCatIndex)];
    int minBpm = tempoMin[BPM(upperCatIndex,middleCatIndex,lowerCatIndex)];
                                 
    if (bmpValue == maxBpm){
        _plusBtn.enabled = false;
    }else{
        _plusBtn.enabled = true;
    }
    
    if (bmpValue == minBpm){
        _minusBtn.enabled = false;
    }else{
        _minusBtn.enabled = true;
    }
}



-(IBAction) plusBMP:(id)sender {
    lastEvent = screenSaverCounter;
    [tempoButtonTimer invalidate];
    tempoButtonTimer = nil;
    [self increaseBMP];
}

-(void) increaseBMP {
    lastEvent = screenSaverCounter;
  /* if (upperCatIndex == 0 && middleCatIndex == 0)
    {
        if (bmpValue<240) {
            bmpValue++;
        }
        else {
            bmpValue = 240;
        }
    } else {
    if (bmpValue<120) {
        bmpValue++;
    }
    else {
        bmpValue = 120;
    }
    }*/
    if (bmpValue < tempoMax[BPM(upperCatIndex,middleCatIndex,lowerCatIndex)])
        bmpValue++;
    else
        bmpValue = tempoMax[BPM(upperCatIndex,middleCatIndex,lowerCatIndex)];
    
    [self checkForPitchBtns];
    [bmpSlider setValue:bmpValue];
    [bmpValueLbl setText:[NSString stringWithFormat:@"%i", bmpValue]];
    [screenSaverBpmLabel setText:[NSString stringWithFormat:@"%i", bmpValue]];

#ifdef USE_TAAE
    [audioProcessor updateLehraData];
#endif

}

-(IBAction) minusBMP:(id)sender {
    lastEvent = screenSaverCounter;
    [tempoButtonTimer invalidate];
    tempoButtonTimer = nil;
    [self decreaseBMP];
}

-(void) decreaseBMP
{
    lastEvent = screenSaverCounter;
    if (bmpValue>tempoMin[BPM(upperCatIndex,middleCatIndex,lowerCatIndex)]) {
        bmpValue--;
    }
    else {
        bmpValue = tempoMin[BPM(upperCatIndex,middleCatIndex,lowerCatIndex)];
    }
    
    [self checkForPitchBtns];
    
    [bmpSlider setValue:bmpValue];
    [bmpValueLbl setText:[NSString stringWithFormat:@"%i", bmpValue]];
    [screenSaverBpmLabel setText:[NSString stringWithFormat:@"%i", bmpValue]];

#ifdef USE_TAAE
    [audioProcessor updateLehraData];
#endif

}

-(IBAction) bmpSliderValueChanged:(id)sender {
    lastEvent = screenSaverCounter;
    bmpValue = (int)((UISlider *)sender).value;
    [bmpSlider setValue:bmpValue];
    [bmpValueLbl setText:[NSString stringWithFormat:@"%i", bmpValue]];
    [screenSaverBpmLabel setText:[NSString stringWithFormat:@"%i", bmpValue]];
#ifdef USE_TAAE
    [audioProcessor updateLehraData];
#endif

    [self checkForPitchBtns];
}



-(void)checkForPrevAndNextPitchBtns{

    if (pitchIndex == pitchIndexMaxValue){
        _nextPitchBtn.enabled = false;
    }else{
        _nextPitchBtn.enabled = true;
    }
    
    if (pitchIndex == pitchIndexMinValue){
        _peviousPitchBtn.enabled = false;
    }else{
        _peviousPitchBtn.enabled = true;
    }
}

-(IBAction) nextPitch:(id)sender {
    lastEvent = screenSaverCounter;
    if (pitchIndex >= pitchIndexMaxValue) {
        pitchIndex = pitchIndexMaxValue;
    }
    else {
        pitchIndex++;
    }
    [self checkForPrevAndNextPitchBtns];
    [self updatePitchRelatedUI:NO];
}

-(IBAction) previousPitch:(id)sender {
    lastEvent = screenSaverCounter;
    if (pitchIndex <= pitchIndexMinValue) {
        pitchIndex = pitchIndexMinValue;
    }
    else {
        pitchIndex--;
    }
    [self checkForPrevAndNextPitchBtns];
    [self updatePitchRelatedUI:NO];
}


-(void) updatePitchRelatedUI: (BOOL) isSlide {
    if (isSlide) {
        [pitchFreqLbl setText:[NSString stringWithFormat:@"%0.2fHz", pitchFreq]];
        //[pitchSlider setValue:pitchFreq];
        
        /*for (int i=0; i<pitchRecords.count; i++) {
            NSString *key = [[[pitchRecords objectAtIndex:i] allKeys] objectAtIndex:0];
            if (pitchFreq >= [key floatValue]) {
                pitchIndex = i;
                [pitchSymbolLbl setText:[[pitchRecords objectAtIndex:i] valueForKey:key]];
            }
        }*/
    }
    else {
        NSString *key = [[[pitchRecords objectAtIndex:pitchIndex] allKeys] objectAtIndex:0];
        [pitchSymbolLbl setText:[[pitchRecords objectAtIndex:pitchIndex] objectForKey:key]];
        [pitchFreqLbl setText:[NSString stringWithFormat:@"%@Hz", key]];
        pitchFinetune = 100;

        pitchFreq = [key floatValue];
        [pitchSlider setValue:100];//0];//pitchFreq];
    }
    [centLabel setText:[NSString stringWithFormat:@"%d", (int)pitchSlider.value-100]];
    
    pitchCoeff = 146.83f / pitchFreq;
    
#ifdef USE_TAAE
    [audioProcessor updateLehraData];
#endif
}



-(IBAction) plusPitch:(id)sender {
    /*if (pitchFreq<164.81f) {
        pitchFreq += 0.08f;
    }
    else {
        pitchFreq = 164.81f;
    }*/
    double value =pitchSlider.value;
    if (value <= 199){//0.9f)
        value += 1;//0.1f;
    }
    else{
        value = 200;//1.0f;
    }
    
    pitchFreq = [self calculatePitch:value];
    [pitchSlider setValue:value];
    [self updatePitchRelatedUI:YES];
}

-(IBAction) minusPitch:(id)sender {
    /*if (pitchFreq>116.54f) {
        pitchFreq -= 0.08f;
    }
    else {
        pitchFreq = 116.54f;
    }*/
    double value =pitchSlider.value;
    if (value >= 1){//-0.9f)
        value -= 1;//0.1f;
    }
    else{
        value = 0;//-1.0f;
    }
   
    pitchFreq = [self calculatePitch:value];
    [pitchSlider setValue:value];
    
    [self updatePitchRelatedUI:YES];
}

-(float) calculatePitch : (float) value
{
    double f = [[pitchFreqs objectAtIndex:pitchIndex+1] floatValue];
    /*if (value < 0)
    {
        double f = [[pitchFreqs objectAtIndex:pitchIndex+1] floatValue];
        double f0 = [[pitchFreqs objectAtIndex:pitchIndex] floatValue];
        return f + (f-f0)*value;
    } else {
        double f = [[pitchFreqs objectAtIndex:pitchIndex+1] floatValue];
        double f0 = [[pitchFreqs objectAtIndex:pitchIndex+2] floatValue];
        return f + (f0-f)*value;
        
    }*/
    return f*centTable[(int)value];

}

-(IBAction) pitchSliderValueChanged:(id)sender {
    //pitchFreq = (int)((UISlider *)sender).value;
    //[pitchSlider setValue:pitchFreq];
    double value =((UISlider *)sender).value;
    pitchFreq = [self calculatePitch:value];
    pitchFinetune = value;
    [self updatePitchRelatedUI:YES];
}

-(IBAction) playPressed:(id)sender {
    lastEvent = screenSaverCounter;
    if (!isPlaying) {
        [playBtn setImage:[UIImage imageNamed:@"pause"] forState:UIControlStateNormal];
        isPlaying = YES;
        isPaused = NO;
        [self saveValuePreference:@"isPaused" value:@"No"];
        NSError* error = nil;
#ifdef USE_TAAE
        [audioProcessor startAudioEngine:error];
#endif
    }
    else {
        [playBtn setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
        isPlaying = NO;
        isPaused = YES;
        [self saveValuePreference:@"isPaused" value:@"Yes"];
#ifdef USE_TAAE
        [audioProcessor stopAudioEngine];
#endif
    }
}

-(IBAction) upperPressed:(id)sender {
    lastEvent = screenSaverCounter;
    /*
    if (upperCatIndex < upperCatCount-1) {
        upperCatIndex++;
    }
    else {
        upperCatIndex = 0;
    }
    [self calculationForInstrumentRecord];
    if (middleCatIndex >= middleCatCount)
    {
        middleCatIndex = 0;
    }
    
    if (lowerCatIndex >= lowerCatCount)
    {
        lowerCatIndex = 0;
    }
    [self updateInstrumentCatRelatedUI];*/
    lehraMenuTable.tag = LEHRA_INSTRUMENT_MENU;
    [self OpenLehraMenu];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:upperCatIndex inSection:0];
    [lehraMenuTable selectRowAtIndexPath:indexPath animated:YES scrollPosition:
     UITableViewScrollPositionMiddle];
}

-(IBAction) lowerPressed:(id)sender {
    lastEvent = screenSaverCounter;
   /* if (lowerCatIndex < lowerCatCount-1) {
        lowerCatIndex++;
    }
    else {
        lowerCatIndex = 0;
    }
    [self calculationForInstrumentRecord];
    [self updateInstrumentCatRelatedUI];*/
    lehraMenuTable.tag = LEHRA_RAAG_MENU;
    [self OpenLehraMenu];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:lowerCatIndex inSection:0];
    [lehraMenuTable selectRowAtIndexPath:indexPath animated:YES scrollPosition:
     UITableViewScrollPositionMiddle];
}

-(IBAction) middlePressed:(id)sender {
    lastEvent = screenSaverCounter;
   /* if (middleCatIndex < middleCatCount-1) {
        middleCatIndex++;
    }
    else {
        middleCatIndex = 0;
    }
    [self calculationForInstrumentRecord];
    if (lowerCatIndex >= lowerCatCount)
    {
        lowerCatIndex = 0;
    }
    [self updateInstrumentCatRelatedUI];*/
    lehraMenuTable.tag = LEHRA_TAAL_MENU;
    [self OpenLehraMenu];
    NSArray* sortedA = ((NSArray*)[middleCatSortRecord objectAtIndex:upperCatIndex]);

    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sortedA indexOfObject:[NSNumber numberWithInt:middleCatIndex]] inSection:0];
    [lehraMenuTable selectRowAtIndexPath:indexPath animated:YES scrollPosition:
     UITableViewScrollPositionMiddle];

}

-(void) calculationForInstrumentRecord {
    
    upperCatCount = (int)upperCatRecord.count;
    middleCatCount = (int)[[middleCatRecord objectAtIndex:upperCatIndex] count];
    if (middleCatIndex >= middleCatCount)
        middleCatIndex = 0;
    lowerCatCount = (int)[[[lowerCatRecord objectAtIndex:upperCatIndex] objectAtIndex:middleCatIndex] count];
    if (lowerCatIndex >= lowerCatCount)
        lowerCatIndex = 0;

    
    
    
    int maxValue = tempoMax[BPM(upperCatIndex,middleCatIndex,lowerCatIndex)];
    
    if (bmpSlider.maximumValue < maxValue)
    {
        [bmpSlider setMaximumValue:maxValue];
    } else if (bmpSlider.maximumValue > maxValue)
    {
        if (bmpSlider.value > maxValue){
            bmpSlider.value = maxValue;
            bmpValue = maxValue;
            [bmpValueLbl setText:[NSString stringWithFormat:@"%i", bmpValue]];
            [screenSaverBpmLabel setText:[NSString stringWithFormat:@"%i", bmpValue]];
        }
        [bmpSlider setMaximumValue:maxValue];
        
    }
    int minValue = tempoMin[BPM(upperCatIndex,middleCatIndex,lowerCatIndex)];
    
    if (bmpSlider.minimumValue > minValue)
    {
        [bmpSlider setMinimumValue:minValue];
    } else if (bmpSlider.minimumValue < minValue)
    {
        if (bmpSlider.value < minValue){
            bmpSlider.value = minValue;
            bmpValue = minValue;
            [bmpValueLbl setText:[NSString stringWithFormat:@"%i", bmpValue]];
            [screenSaverBpmLabel setText:[NSString stringWithFormat:@"%i", bmpValue]];
        }
        [bmpSlider setMinimumValue:minValue];
        
    }
    
    /*
    if (upperCatIndex == 0 && middleCatIndex == 0)
    {
        if (bmpSlider.maximumValue < 240)
        [bmpSlider setMaximumValue:240];

    } else if (bmpSlider.maximumValue > 120){
        if (bmpSlider.value > 120)
        {
            bmpSlider.value = 120;
            bmpValue = 120;
            [bmpValueLbl setText:[NSString stringWithFormat:@"%i", bmpValue]];

        }
        [bmpSlider setMaximumValue:120];

    }*/
}



-(void) updateInstrumentCatRelatedUI {
    [upperCatLbl setText:[upperCatRecord objectAtIndex:upperCatIndex]];
   
    [middleCatLbl setText:[[[middleCatRecord objectAtIndex:upperCatIndex] objectAtIndex:middleCatIndex] objectForKey:@"Name"] ];
    [lowerCatLbl setText:[[[[lowerCatRecord objectAtIndex:upperCatIndex] objectAtIndex:middleCatIndex] objectAtIndex:lowerCatIndex] objectForKey:@"Name"]];
#ifdef USE_TAAE
    [audioProcessor updateLehraData];
#endif

}

- (IBAction)lehraVolumeChanged:(id)sender {
    lastEvent = screenSaverCounter;
    lehraVolume =((UISlider *)sender).value;
#ifdef USE_TAAE
    [audioProcessor setLehraVolume:lehraVolume];
#endif

}

- (IBAction)tanpuraVolumeChanged:(id)sender {
    lastEvent = screenSaverCounter;
    tanpuraVolume =((UISlider *)sender).value;
#ifdef USE_TAAE
    [audioProcessor setTanpuraVolume:tanpuraVolume];
#endif
}

- (IBAction)aboutPressed:(id)sender {
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"AboutPressed"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)artistPressed:(id)sender {
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"ArtistPressed"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (IBAction)purchasePressed:(id)sender {
    if (_timer!= nil){
        [_timer invalidate];
    }
    if (_shortTimer!= nil){
        [_shortTimer invalidate];
        _timerStart = false;
    }

}

- (IBAction)menuPressed:(id)sender {
    //self.menuView.hidden = self.menuView.hidden == false;
    [self OpenMenu];
}


- (IBAction)plusButtonTouchUp:(id)sender {
    lastEvent = screenSaverCounter;
    [tempoButtonTimer invalidate];
    tempoButtonTimer = nil;
}

- (IBAction)plusButtonTouchDown:(id)sender {
    lastEvent = screenSaverCounter;
    tempoButtonTimerStart = [NSDate date];
    tempoButtonTimer = [NSTimer scheduledTimerWithTimeInterval:0.3f
                                                   target:self
                                                 selector:@selector(updatePlusTempoTimer)
                                                 userInfo:nil
                                                  repeats:YES];
}

- (IBAction)minusButtonTouchDown:(id)sender {
    lastEvent = screenSaverCounter;
    tempoButtonTimerStart = [NSDate date];
    tempoButtonTimer = [NSTimer scheduledTimerWithTimeInterval:0.3f
                                                        target:self
                                                      selector:@selector(updateMinusTempoTimer)
                                                      userInfo:nil
                                                       repeats:YES];
}

-(void) updatePlusTempoTimer
{
    NSTimeInterval elapsedTime = [[NSDate date] timeIntervalSinceDate:tempoButtonTimerStart];
    if (elapsedTime > 1.0)
    {
        [self increaseBMP];

    }
  
}

-(void) updateMinusTempoTimer
{
    NSTimeInterval elapsedTime = [[NSDate date] timeIntervalSinceDate:tempoButtonTimerStart];
    if (elapsedTime > 1.0)
    {
        [self decreaseBMP];
    }
    
}
- (IBAction)minusButtonTouchUp:(id)sender {
    lastEvent = screenSaverCounter;
    [tempoButtonTimer invalidate];
    tempoButtonTimer = nil;
}

- (IBAction)plusButtonDoubleTap:(id)sender {
    /*bmpValue = bmpValue*2;
    if (upperCatIndex == 0 && middleCatIndex == 0)
    {
        if (bmpValue<240) {
            //bmpValue++;
        }
        else {
            bmpValue = 240;
        }
    } else {
        if (bmpValue<120) {
            //bmpValue++;
        }
        else {
            bmpValue = 120;
        }
    }
    [bmpSlider setValue:bmpValue];
    [bmpValueLbl setText:[NSString stringWithFormat:@"%i", bmpValue]];*/

}

- (IBAction)minusButtonDoubleTap:(id)sender {
   /* bmpValue = bmpValue/2;
    if (upperCatIndex == 0 && middleCatIndex == 0)
    {
        if (bmpValue<240) {
            //bmpValue++;
        }
        else {
            bmpValue = 240;
        }
    } else {
        if (bmpValue<120) {
            //bmpValue++;
        }
        else {
            bmpValue = 120;
        }
    }
    [bmpSlider setValue:bmpValue];
    [bmpValueLbl setText:[NSString stringWithFormat:@"%i", bmpValue]];*/

}
- (void)bpmSliderTapped:(UIGestureRecognizer *)g {
    lastEvent = screenSaverCounter;
    UISlider* s = (UISlider*)g.view;
    if (s.highlighted)
        return; // tap on thumb, let slider deal with it
    CGPoint pt = [g locationInView: s];
    CGFloat percentage = pt.x / s.bounds.size.width;
    CGFloat delta = percentage * (s.maximumValue - s.minimumValue);
    CGFloat value = s.minimumValue + delta;
    if (value < s.value)
        [s setValue:s.value/2 animated:YES];
    else
        [s setValue:s.value*2 animated:YES];
     [s sendActionsForControlEvents:UIControlEventValueChanged];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (tableView.tag)
    {
        case LEHRA_INSTRUMENT_MENU:
            return upperCatRecord.count;
        case LEHRA_RAAG_MENU:
            return ((NSArray*)[[lowerCatRecord objectAtIndex:upperCatIndex] objectAtIndex:middleCatIndex]).count;//9;
        case LEHRA_TAAL_MENU:
            return ((NSArray*)[middleCatRecord objectAtIndex:upperCatIndex]).count;//_presetNames.count;
    }
    
    /*  if (isChordMenu)
     {
     return 9;
     }
     return 6;//[titles count];*/
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    static NSString *MyIdentifier = @"MyIdentifier";
    
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil){
        cell = [[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:MyIdentifier];
    }
    
    NSArray *a;
    
    if (tableView.tag == LEHRA_TAAL_MENU )
    {
        a = ((NSArray*)[middleCatRecord objectAtIndex:upperCatIndex]);
        NSArray* sortedA = ((NSArray*)[middleCatSortRecord objectAtIndex:upperCatIndex]);
        NSDictionary* d =[a objectAtIndex:[[sortedA objectAtIndex:indexPath.row] integerValue]];//[a objectAtIndex:indexPath.row];
        cell.textLabel.text = [d objectForKey:@"Name"];
        if ([[d objectForKey:@"Hidden"] boolValue])
        {
            cell.userInteractionEnabled = NO;
            cell.textLabel.enabled = NO;
        } else {
            cell.userInteractionEnabled = YES;
            cell.textLabel.enabled = YES;
        }
    } else {
    if (tableView.tag == LEHRA_INSTRUMENT_MENU)//isChordMenu)
    {
        a = upperCatRecord;
        cell.textLabel.text = [a objectAtIndex:indexPath.row];
        cell.userInteractionEnabled = YES;
        cell.textLabel.enabled = YES;
    } else if (tableView.tag == LEHRA_RAAG_MENU)
    {
        a = ((NSArray*)[[lowerCatRecord objectAtIndex:upperCatIndex] objectAtIndex:middleCatIndex]);
        NSDictionary* d =[a objectAtIndex:indexPath.row];
        cell.textLabel.text = [d objectForKey:@"Name"];
        if ([[d objectForKey:@"Hidden"] boolValue])
        {
            cell.userInteractionEnabled = NO;
            cell.textLabel.enabled = NO;
        }else {
            cell.userInteractionEnabled = YES;
            cell.textLabel.enabled = YES;
        }
        
    }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:14];
    cell.textLabel.textColor = [UIColor colorWithRed:0.1333 green:0.1333  blue:0.1333 alpha:1.0];
        
    
    //cell.textLabel.textColor = [UIColor darkGrayColor];
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.highlightedTextColor = [UIColor orangeColor];
    //cell.textLabel.text = [_presetNames objectAtIndex:indexPath.row];
    UIView *selectedView = [[UIView alloc]init];
    selectedView.backgroundColor = [UIColor clearColor];
    cell.selectedBackgroundView = selectedView;
    return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    lastEvent = screenSaverCounter;
   

        
        if (tableView.tag == LEHRA_INSTRUMENT_MENU) {
            upperCatIndex = (int)indexPath.row;
#ifdef LEHRA_FREE
            middleCatIndex = (int)[[middleCatFirstRecord objectAtIndex:upperCatIndex] integerValue];
            lowerCatIndex = (int)[[[lowerCatFirstRecord objectAtIndex:upperCatIndex] objectAtIndex:middleCatIndex] integerValue];
            
            
#endif
            [self calculationForInstrumentRecord];
            NSArray* sortedA = ((NSArray*)[middleCatSortRecord objectAtIndex:upperCatIndex]);
             middleCatIndex = [[sortedA objectAtIndex:0] intValue];
            
//            if (middleCatIndex >= middleCatCount)
//            {
//                middleCatIndex = 0;
//            }
            
            if (lowerCatIndex >= lowerCatCount)
            {
                lowerCatIndex = 0;
            }
            [self updateInstrumentCatRelatedUI];
        } else if (tableView.tag == LEHRA_RAAG_MENU) {
            lowerCatIndex = (int)indexPath.row;
            [self calculationForInstrumentRecord];
            [self updateInstrumentCatRelatedUI];
        } else if (tableView.tag == LEHRA_TAAL_MENU) {
           NSArray* sortedA = ((NSArray*)[middleCatSortRecord objectAtIndex:upperCatIndex]);
           middleCatIndex = (int)[[sortedA objectAtIndex:indexPath.row] integerValue];
            
#ifdef LEHRA_FREE
            lowerCatIndex = (int)[[[lowerCatFirstRecord objectAtIndex:upperCatIndex] objectAtIndex:middleCatIndex] integerValue];
            
            
#endif
            [self calculationForInstrumentRecord];
            if (lowerCatIndex >= lowerCatCount)
            {
                lowerCatIndex = 0;
            }
            [self updateInstrumentCatRelatedUI];
        }
    [self CloseLehraMenu];
    
}
- (IBAction)tapTempoPressed:(id)sender {
    @try {
        lastEvent = screenSaverCounter;
        if (isTapTempoOn) {
            //NSTimeInterval t =[[NSDate date] timeIntervalSince1970];
            UInt64 temp = (UInt64)([[NSDate date] timeIntervalSince1970]*1000.0);
            UInt64 interval = temp - lastTapTempoEvent;
            if (interval < maxTapTempoInterval && interval > minTapTempoInterval) {
                double sinterval = (double) interval / 1000;
                [tapTempoEvents addObject:[NSNumber numberWithDouble:sinterval]];//tapTempoEvents.add(Double.valueOf(sinterval));
                
                if (tapTempoEvents.count > 1) {
                    if (tapTempoEvents.count > 5)
                        [tapTempoEvents removeObjectAtIndex:0];
                    
                    double sum = 0;
                    for (NSNumber *d in tapTempoEvents) {
                        sum += [d doubleValue];
                    }
                    double ave = sum / (double) tapTempoEvents.count;
                    double bpm = 60.0 / ave;
                    bmpValue = bpm;
                    if (bmpValue > tempoMax[BPM(upperCatIndex,middleCatIndex,lowerCatIndex)])

                        bmpValue = tempoMax[BPM(upperCatIndex,middleCatIndex,lowerCatIndex)];

                    if (bmpValue<tempoMin[BPM(upperCatIndex,middleCatIndex,lowerCatIndex)]) {

                        bmpValue = tempoMin[BPM(upperCatIndex,middleCatIndex,lowerCatIndex)];
                    }

                    [bmpSlider setValue:bmpValue ];//bmpSlider.minimumValue];
                    [bmpValueLbl setText:[NSString stringWithFormat:@"%i", bmpValue]];
                    [screenSaverBpmLabel setText:[NSString stringWithFormat:@"%i", bmpValue]];
#ifdef USE_TAAE
                    [audioProcessor updateLehraData];
#endif
                    /*
                    AppMain.bpmValue = (int) bpm;
                    if (AppMain.bpmValue > AppMain.getMaxTempo())
                        AppMain.bpmValue = AppMain.getMaxTempo();
                    if (AppMain.bpmValue < AppMain.getMinTempo())
                        AppMain.bpmValue = AppMain.getMinTempo();
                    int progress = (int) (210.0f * (float) (AppMain.bpmValue - AppMain.getMinTempo()) / AppMain.getTempoValue());
                    //Log.d("BPM","INCREASEBPM progress:"+String.valueOf(progress) + " bpm:"+String.valueOf(AppMain.bpmValue));
                    tempoSlider.setProgress(progress);
                    tempoLabel.setText(String.valueOf(AppMain.bpmValue));
                    screenSaverTempoLbl.setText(String.valueOf(AppMain.bpmValue));
                    AppMain.UpdateParameters();*/
                    
                }
            } else {
                [tapTempoEvents removeAllObjects];
            }
            lastTapTempoEvent = temp;
        } else {
            lastTapTempoEvent = (long)([[NSDate date] timeIntervalSince1970]*1000);
            isTapTempoOn = true;
        }
    } @catch (NSException *e) {
        //reportError(e);
    }
}

- (IBAction)onScreenSaverTapped:(id)sender {
    lastEvent = screenSaverCounter;
    //self.screenSaverView.hidden = true;
    [UIView animateWithDuration:0.25
                     animations:^{
                         self.screenSaverView.alpha = 0;
                     }
                     completion:^(BOOL finished){
                         self.screenSaverView.hidden = true;
                     }];
}

-(void) screenSaverTimerEvent
{
    //View screenSaver = findViewById(R.id.screenSaver);
    if (screenSaverCounter-lastEvent > 1 && self.screenSaverView.hidden && isPlaying)
    {
        self.screenSaverView.alpha = 0;
        self.screenSaverView.hidden = false;
        [UIView animateWithDuration:0.65
                         animations:^{
                             self.screenSaverView.alpha = 1;
                         }
                         completion:^(BOOL finished){
                         }];
        

        
        //screenSaver.setAlpha(0.0f);
        /*screenSaver.setVisibility(View.VISIBLE);
        AlphaAnimation animation1 = new AlphaAnimation(0.0f, 1.0f);
        animation1.setDuration(1000);
        //animation1.setStartOffset(0);
        
        
        screenSaver.startAnimation(animation1);*/
        
    }
    screenSaverCounter++;
}
- (IBAction)metronomeVolumeChanged:(id)sender {
    lastEvent = screenSaverCounter;
    tanpuraVolume =((UISlider *)sender).value;
#ifdef USE_TAAE
    [audioProcessor setMetronomeVolume:tanpuraVolume];
#endif

}
/// Timer Function Set
-(void) inventoryDetails{
    
    inventoryDetailsStarted = true;
   // NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
   // NSString *upgraded_app = [prefs stringForKey:@"Upgraded"];
   // if (upgraded_app == nil) {
    //    [self registerTimer];
   // }

     [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
      [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void)paymentQueue:(SKPaymentQueue*)queue restoreCompletedTransactionsFailedWithError:(NSError*)error
{
    NSLog(@"error");
}

- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    if (inventoryDetailsStarted){
        inventoryDetailsStarted = FALSE;
        NSLog(@"received restored transactions: %lu", (unsigned long)queue.transactions.count);
        if (queue.transactions.count>0){
            for(SKPaymentTransaction *transaction in queue.transactions){
                if(transaction.transactionState == SKPaymentTransactionStateRestored){
                    //called when the user successfully restores a purchase
                    NSLog(@"Transaction state -> Restored");
                    
                    //if you have more than one in-app purchase product,
                    //you restore the correct product for the identifier.
                    //For example, you could use
                    //if(productID == kRemoveAdsProductIdentifier)
                    //to get the product identifier for the
                    //restored purchases, you can use
                    //
                    //NSString *productID = transaction.payment.productIdentifier;
                    
                    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                    [self paymentSucceeded];
                    break;
                }
            }
        }
        else{
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *upgraded_app = [prefs stringForKey:@"Upgraded"];
            if (upgraded_app == nil) {
                [self registerTimer];
            }
        }
    }
    
    
}

-(void)paymentQueue:(SKPaymentQueue *)queue
updatedTransactions:(NSArray *)transactions {
    for (SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchasing:
                NSLog(@"Purchasing");
                break;
            case SKPaymentTransactionStatePurchased:
                if ([transaction.payment.productIdentifier
                     isEqualToString:kProductID]) {
                    NSLog(@"Purchased ");
                    [self paymentSucceeded];
                    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                              @"Purchase is completed succesfully" message:nil delegate:
                                              self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                    [alertView show];
                    
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                if ([transaction.payment.productIdentifier
                     isEqualToString:kProductID]) {
                    NSLog(@"Restored ");
                    [self paymentSucceeded];
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                NSLog(@"Purchase failed ");
                break;
            default:
                break;
        }
    }
}

- (void)paymentSucceeded{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *upgraded_app_status = [prefs stringForKey:@"Upgraded"];
    if (upgraded_app_status == nil){
        [self saveValuePreference:@"Upgraded" value:@"true"];
        [self saveValuePreference:@"ShortTimer" value:@"Completed"];
    }
}


-(bool) isNetworkAvailable{
    SCNetworkReachabilityFlags flags;
    SCNetworkReachabilityRef address;
    address = SCNetworkReachabilityCreateWithName(NULL, "www.apple.com" );
    Boolean success = SCNetworkReachabilityGetFlags(address, &flags);
    CFRelease(address);
    
    bool canReach = success
    && !(flags & kSCNetworkReachabilityFlagsConnectionRequired)
    && (flags & kSCNetworkReachabilityFlagsReachable);
    
    return canReach;

}
-(void) updateUI{
    if (_timer != nil) {
        [_timer invalidate];
    }
    
    if ([[self getValuePreference:@"ShortTimer"]  isEqual: @"Running"]) {
        long long shortTimerStart = [[self getValuePreference:@"ShortTimerStart"] longLongValue];
        long long shortTimerEnd = [[self getValuePreference:@"ShortTimerEnd"] longLongValue];
        long long currentTime = (long long)([[NSDate date] timeIntervalSince1970] * 1000.0);
        long long diffInTime = currentTime - shortTimerStart;
        //2 min changed to 1 min
        if (diffInTime / _ONE_MINUTE >= 1) {
            [self saveValuePreference:@"ShortTimer" value:@"Completed"];
        } else {
            TWO_MINUTE_TIMER = shortTimerEnd - currentTime;
            _timerStart = false;
            [self shortTempTimer];
        }
    }
    
    if (![[self getValuePreference:@"ShortTimer"]  isEqual: @"Running"]) {
        
        [self saveValuePreference:@"Expired" value:@"true"];
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Your trial period has expired"
                              message:@"Click purchase to continue using Lehra Studio"
                              delegate:self
                              cancelButtonTitle:@"Cancel"
                              otherButtonTitles:nil];
        [alert addButtonWithTitle:@"Purchase"];
        [alert addButtonWithTitle:@"Restore"];
        alert.tag = 0;
        // ------ Pause the music------
        lastEvent = screenSaverCounter;
      if (isPlaying) {
      [playBtn setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
                   isPlaying = NO;
    #ifdef USE_TAAE
                [audioProcessor stopAudioEngine];
    #endif
    }


        [alert show];
    }

}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{

    if(alertView.tag == 0){
        NSLog(@"Button: %li, was pressed.", (long)buttonIndex);
        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
        
        if([title isEqualToString:@"Cancel"])
        {
            NSLog(@"Button 1 was selected.");
            _timerStart = true;
            [self saveValuePreference:@"ShortTimer" value:@"Running"];
            //---- Resume the music-----
//            lastEvent = screenSaverCounter;
//                if (!isPaused) {
//                    [playBtn setImage:[UIImage imageNamed:@"pause"] forState:UIControlStateNormal];
//                    isPlaying = YES;
//                    NSError* error = nil;
//            #ifdef USE_TAAE
//                    [audioProcessor startAudioEngine:error];
//            #endif
//                }
            //      -------- Starting 1 min timer --------
            [self shortTempTimer];
        }
        else if([title isEqualToString:@"Purchase"])
        {
            NSLog(@"Button 2 was selected.");
            NSString * storyboardName = @"Main2_iPad2";
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
            UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"UpgradeView"];
            [self presentViewController:vc animated:YES completion:nil];
        }
        else if([title isEqualToString:@"Restore"])
        {
            NSLog(@"Button 3 was selected.");
            NSString * storyboardName = @"Main2_iPad2";
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
            UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"UpgradeView"];
            [self presentViewController:vc animated:YES completion:nil];
        }
        
    }
    if (alertView.tag == 1){
        
        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
        
        if([title isEqualToString:@"Cancel"])
        {
            NSLog(@"Button 1 was selected.");
            _timerStart = YES;
            [self saveValuePreference:@"ShortTimer" value:@"Running"];
            //---- Resume the music-----
//            lastEvent = screenSaverCounter;
//            if (!isPaused) {
//                [playBtn setImage:[UIImage imageNamed:@"pause"] forState:UIControlStateNormal];
//                isPlaying = YES;
//                NSError* error = nil;
//            #ifdef USE_TAAE
//                [audioProcessor startAudioEngine:error];
//            #endif
//            }
            //      -------- Starting 1 min timer --------
            [self shortTempTimer];
        }
        else if([title isEqualToString:@"Purchase"])
        {
            NSString * storyboardName = @"Main2_iPad2";
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
            UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"UpgradeView"];
            [self presentViewController:vc animated:YES completion:nil];
            NSLog(@"Button 2 was selected.");
        }
        else if([title isEqualToString:@"Restore"])
        {
            NSString * storyboardName = @"Main2_iPad2";
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
            UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"UpgradeView"];
            [self presentViewController:vc animated:YES completion:nil];
            NSLog(@"Button 3 was selected.");
        }
    }
}

-(void) registerTimer{
    //timer changes 5 days turned to 3 days and 2 minute to 1 minute
    long long FIVE_DAYS = 3 * 24 * 60 * 60 * 1000;
    _FIVE_DAYS_FOR_TIMER = 3 * 24 * 60 * 60 * 1000;
    long ONE_DAY = 24 * 60 * 60 * 1000;
    
//        long long FIVE_DAYS = 2 * 60 * 60 * 1000;
//        _FIVE_DAYS_FOR_TIMER = 2 * 60 * 60 * 1000;
//        long ONE_DAY = 1 * 60 * 60 * 1000;

    
//    long long FIVE_DAYS = 2 * 60 * 1000;
//    _FIVE_DAYS_FOR_TIMER = 2 * 60 * 1000;
//    long ONE_DAY = 1 * 60 * 1000;
//
    
    
    long long FIVE_DAYS_DURATION = 0;
    long long FIVE_DAYS_DURATION_Local = 0;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *installDate = [prefs stringForKey:@"InstallDate"];
    
    NSString *FIVE_DAYS_DURATION_Shared = [prefs stringForKey:@"FiveDays"];
    NSString *upgraded_app = [prefs stringForKey:@"Upgraded"];
    
    
    if (installDate == nil) {
        // First run, so save the current date
        long long currentTime = (long long)([[NSDate date] timeIntervalSince1970] * 1000.0);
        FIVE_DAYS_DURATION = currentTime + FIVE_DAYS;
        
        [self saveValuePreference:@"InstallDate" value:[NSString stringWithFormat:@"%lld", currentTime]];
        [self saveValuePreference:@"FiveDays" value:[NSString stringWithFormat:@"%lld", FIVE_DAYS_DURATION]];
        
        long long seconds_left = FIVE_DAYS / 1000.0;
        FIVE_BACKUP_MINUTE_TIMER = _FIVE_DAYS_FOR_TIMER;
        FIVE_BACKUP_MINUTE_TIMER = FIVE_BACKUP_MINUTE_TIMER/1000.0;
        _timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(runScheduledTask:) userInfo:nil repeats:YES];
        [self ServerRegister:[NSString stringWithFormat:@"%lld", seconds_left] locally_expired:NO];
        
    } else {
        long long installTime = [installDate longLongValue];
        long long currentTime = (long long)([[NSDate date] timeIntervalSince1970] * 1000.0);
        long long FIVE_DAYS_Shared = 0;
        if (FIVE_DAYS_DURATION_Shared != nil) {
            FIVE_DAYS_Shared = [FIVE_DAYS_DURATION_Shared longLongValue];
        } else {
            long currentTime_Local = [[NSDate date] timeIntervalSince1970] * 1000;
            FIVE_DAYS_DURATION_Local = currentTime_Local + FIVE_DAYS;
            FIVE_DAYS_Shared = FIVE_DAYS_DURATION_Local;
        }
        long long diff_for_server = FIVE_DAYS_Shared - currentTime;
        long long diff_for_local = currentTime - installTime;
        long long seconds_left = diff_for_server / 1000.0;
        long long days_local_timer = diff_for_local / ONE_DAY;
        _FIVE_DAYS_FOR_TIMER = diff_for_server;
        if (_timer!= nil){
            [_timer invalidate];
        }
        
        FIVE_BACKUP_MINUTE_TIMER = _FIVE_DAYS_FOR_TIMER;
        FIVE_BACKUP_MINUTE_TIMER = FIVE_BACKUP_MINUTE_TIMER/1000.0;
        _timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(runScheduledTask:) userInfo:nil repeats:YES];
        
        
        if (upgraded_app == nil) {
            if (days_local_timer >= 3 || days_local_timer < 0) { // More than 5 days? changed to 3 days
                //EXPIRED
                [self ServerRegister:[NSString stringWithFormat:@"%lld", seconds_left] locally_expired:YES];
            } else {
                //NOT EXPIRED
                [self ServerRegister:[NSString stringWithFormat:@"%lld", seconds_left] locally_expired:NO];
            }
        } else {
            // no API call as app is already registered.
        }
        
    }

}
-(void) ServerRegister:(NSString *)seconds_left_
       locally_expired:(BOOL)locally_expired{
    int time_left;
    NSString *key = @"YKaB1sJnQCM17MDh";
    NSArray* components = [seconds_left_ componentsSeparatedByString:@"."];
    NSString *t = [components objectAtIndex:0];
    seconds_left_ = t;
    NSString *seconds_left = seconds_left_;
    NSString *uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    if ([self isNetworkAvailable]){
        
        NSArray *keys = [NSArray arrayWithObjects:@"package_name",@"seconds_left",@"device_id",nil];
        //        NSArray *objects = [NSArray arrayWithObjects:@"au.com.lehrastudio.tanpurastudio",seconds_left,@"ffffffff-97df-ad72-ffff-ffffdc16839e", nil];
        NSArray *objects = [NSArray arrayWithObjects:@"com.lehrastudio.lehrastudiopro",seconds_left,uniqueIdentifier, nil];
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
        
        NSError *err;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
        NSString *stringParams = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        NSString* encrypted = [AESTool encryptData:stringParams withKey:key];
        NSString* decrypted = [AESTool decryptData:encrypted withKey:key];
        
        NSData *data = [encrypted dataUsingEncoding:NSUTF8StringEncoding];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://www.lehrastudio.com/API/checkPro5days"]];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:data];
        
        NSURLResponse *response;
        NSError *error;
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSString *responseStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSLog(@"responseStr = %@", responseStr);
        if (responseStr!=nil){
            BOOL is_expired;
            NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:data
                                                                     options:kNilOptions
                                                                       error:&error];
            NSLog(@"response Server Side = %@", response);
            
            if ([response valueForKey:@"success"]) {
                int status = [[response valueForKey:@"success"] integerValue];
                if (status == 1) {
                    if ([response valueForKey:@"data"]) {
                        NSDictionary *jsonObject = [response valueForKey:@"data"];
                        if ([jsonObject valueForKey:@"is_expired"]) {
                            is_expired = [[jsonObject valueForKey:@"is_expired"] boolValue];
                            time_left = [[jsonObject valueForKey:@"time_left"] integerValue];
                            if (is_expired) {
                                if ([jsonObject valueForKey:@"is_pro"]) {
                                    BOOL is_pro =  [[jsonObject valueForKey:@"is_pro"] boolValue];
                                    if (is_pro) {
                                        // App is upgraded
                                        [self saveValuePreference:@"Upgraded" value:@"true"];
                                        [self saveValuePreference:@"ShortTimer" value:@"Completed"];
                                    } else {
                                        [self updateUI];
                                    }
                                }
                            }
                        }
                        
                    }
                    
                    
                } else {
                    //Error
                    NSLog(@"Error = ");
                }
            }
        }
    }
    else{
        if (locally_expired) {
            [self updateUI];
        }
    }
}
- (void) shortTempTimer{
    if ([[self getValuePreference:@"ShortTimer"]  isEqual: @"Running"]) {
        NSLog(@"Your 1 min timer has been started.");
        [self saveValuePreference:@"ShortTimer" value:@"Running"];
        
        if (_timerStart) {
            
            long long currentTime = (long long)([[NSDate date] timeIntervalSince1970] * 1000.0);
            long long endTime = currentTime + TWO_MINUTE_TIMER_;
            [self saveValuePreference:@"ShortTimerStart" value:[NSString stringWithFormat:@"%lld", currentTime]];
            [self saveValuePreference:@"ShortTimerEnd" value:[NSString stringWithFormat:@"%lld", endTime]];
            _timerStart = false;
        }
        
        TWO_BACKUP_MINUTE_TIMER = TWO_MINUTE_TIMER;
        TWO_BACKUP_MINUTE_TIMER = TWO_BACKUP_MINUTE_TIMER/1000.0;
        _shortTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(runScheduledTaskShort:) userInfo:nil repeats:YES];
    }
}


- (void)runScheduledTask: (NSTimer *) runningTimer {
    long long hours, minutes, seconds;
    FIVE_BACKUP_MINUTE_TIMER--;
    hours = FIVE_BACKUP_MINUTE_TIMER / 3600;
    minutes = (FIVE_BACKUP_MINUTE_TIMER % 3600) / 60;
    seconds = (FIVE_BACKUP_MINUTE_TIMER %3600) % 60;
    NSLog(@"_THREE_DAYS_FOR_TIMER %lld",FIVE_BACKUP_MINUTE_TIMER);
    if (FIVE_BACKUP_MINUTE_TIMER == 0 || FIVE_BACKUP_MINUTE_TIMER < 0) {
        [_timer invalidate];
        [self updateUI];
    }
}
- (void)runScheduledTaskShort: (NSTimer *) runningTimer {
    long long hours, minutes, seconds;
    TWO_BACKUP_MINUTE_TIMER--;
    hours = TWO_BACKUP_MINUTE_TIMER / 3600;
    minutes = (TWO_BACKUP_MINUTE_TIMER % 3600) / 60;
    seconds = (TWO_BACKUP_MINUTE_TIMER %3600) % 60;
    NSLog(@"%lld",TWO_BACKUP_MINUTE_TIMER);
    if (TWO_BACKUP_MINUTE_TIMER == 0 || TWO_BACKUP_MINUTE_TIMER < 0) {
        [_shortTimer invalidate];
        [self updateUIShortTimerInterval];
    }
    
}
-(void) updateUIShortTimerInterval{
    [self saveValuePreference:@"ShortTimer" value:@"Completed"];
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Your trial period has expired"
                          message:@"Click purchase to continue using Lehra Studio"
                          delegate:self
                          cancelButtonTitle:@"Cancel"
                          otherButtonTitles:nil];
    [alert addButtonWithTitle:@"Purchase"];
    [alert addButtonWithTitle:@"Restore"];
    alert.tag = 1;
    //---- Pause the music----
  lastEvent = screenSaverCounter;
    if (isPlaying) {
        [playBtn setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
        isPlaying = NO;
    #ifdef USE_TAAE
        [audioProcessor stopAudioEngine];
    #endif
    }

    [alert show];
}
    


- (void) saveValuePreference:(NSString *)key value:(NSString*)value {
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    if (standardUserDefaults) {
        [standardUserDefaults setObject:value forKey:key];
        [standardUserDefaults synchronize];
    }
}
- (NSString*) getValuePreference:(NSString *)key{
    NSString *valueForKey = @"";
    NSUserDefaults *data = [NSUserDefaults standardUserDefaults];
    NSString *myString = [data objectForKey:key];
    if(myString != nil){
        //object is there
        valueForKey = myString;
    }
    return valueForKey;
}

@end
