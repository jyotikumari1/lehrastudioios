//
//  AEHPFilter.h
//  eLehra
//
//  Created by Jukka Rauhala on 04/01/16.
//  Copyright (c) 2015 Jukka Rauhala. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AEFloatConverter.h"
#import "AEHPFilter.h"
#import "TimeStretcherPL.h"
#import "SamplingRateConverter.h"
#import "LehraAudioProcessor.h"
const int kScratchBufferLength = 8192;
#define CBUFFER_LENGTH 4096

@interface AEHPFilter () {
    float **_scratchBuffer;
    float *_buffer;
    //AudioProcessorData* data;
    //LehraAudioProcessor* processor;
    float pitch;
    float tsCoeff;
    HighShelvingFilter* hsf;
    float *_cbuffer;
    int cbuffer_read;
    int cbuffer_write;
}
@property (nonatomic, strong) AEFloatConverter *floatConverter;
@property (nonatomic, weak) AEAudioController *audioController;
@end
@implementation AEHPFilter

- (id)initWithAudioController:(AEAudioController *)audioController {
    if ( !(self = [super init]) ) return nil;
    _cbuffer = (float*) malloc(CBUFFER_LENGTH * sizeof(float));
    memset(_cbuffer,0,CBUFFER_LENGTH*sizeof(float));
    self.audioController = audioController;
    _clientFormat = audioController.audioDescription;
    self.floatConverter = [[AEFloatConverter alloc] initWithSourceFormat:_clientFormat];

   // self.limiter = [[AELimiter alloc] initWithNumberOfChannels:_clientFormat.mChannelsPerFrame sampleRate:_clientFormat.mSampleRate];
    
    _scratchBuffer = (float**)malloc(sizeof(float*) * _clientFormat.mChannelsPerFrame);
    assert(_scratchBuffer);
    for ( int i=0; i<_clientFormat.mChannelsPerFrame; i++ ) {
        _scratchBuffer[i] = (float*)malloc(sizeof(float) * kScratchBufferLength);
        assert(_scratchBuffer[i]);
    }
    
    hsf = new HighShelvingFilter();
    
    //_buffer = (float*)malloc(sizeof(float) * kScratchBufferLength);
    return self;
}

-(void)dealloc {
    for ( int i=0; i<_clientFormat.mChannelsPerFrame; i++ ) {
        free(_scratchBuffer[i]);
    }
    free(_scratchBuffer);
    //free(_buffer);
    free(_cbuffer);
    
    delete(hsf);
    //delete(srComponent);
    self.audioController = nil;
}

-(void)setClientFormat:(AudioStreamBasicDescription)clientFormat {
    
    AEFloatConverter *floatConverter = [[AEFloatConverter alloc] initWithSourceFormat:clientFormat];
    
    float **scratchBuffer = (float**)malloc(sizeof(float*) * clientFormat.mChannelsPerFrame);
    assert(scratchBuffer);
    for ( int i=0; i<clientFormat.mChannelsPerFrame; i++ ) {
        scratchBuffer[i] = (float*)malloc(sizeof(float) * kScratchBufferLength);
        assert(scratchBuffer[i]);
    }
    

    //AELimiter *limiter = [[AELimiter alloc] initWithNumberOfChannels:clientFormat.mChannelsPerFrame sampleRate:clientFormat.mSampleRate];
    float** oldScratchBuffer = _scratchBuffer;
    AudioStreamBasicDescription oldClientFormat = _clientFormat;
    
    [_audioController performSynchronousMessageExchangeWithBlock:^{
       // _limiter = limiter;
        _floatConverter = floatConverter;
        _scratchBuffer = scratchBuffer;
        _clientFormat = clientFormat;
    }];
    
    for ( int i=0; i<oldClientFormat.mChannelsPerFrame; i++ ) {
        free(oldScratchBuffer[i]);
    }
    free(oldScratchBuffer);
}

-(void) setParam: (float) _pitch : (float) _tsCoeff
{
    pitch = _pitch;
    tsCoeff = _tsCoeff;
}

static OSStatus filterCallback(__unsafe_unretained AEHPFilter *THIS,
                               __unsafe_unretained AEAudioController *audioController,
                               AEAudioControllerFilterProducer producer,
                               void *producerToken,
                               const AudioTimeStamp *time,
                               UInt32 frames,
                               AudioBufferList *audio) {
   
    OSStatus status = producer(producerToken, audio, &frames);
    if ( status != noErr ) return status;
    
    // Copy buffer into floating point scratch buffer
    AEFloatConverterToFloat(THIS->_floatConverter, audio, THIS->_scratchBuffer, frames);
    //memcpy(THIS->_buffer, audio->mBuffers[0].mData, tsBufferLen * sizeof(float));

    
    THIS->hsf->Process(THIS->_scratchBuffer[0], frames, 6000.0f, 15.0f);

    
    if ( frames > 0 ) {
            AEFloatConverterFromFloat(THIS->_floatConverter, THIS->_scratchBuffer, audio, frames);
    }
    return noErr;
}
-(AEAudioControllerFilterCallback)filterCallback {
    return filterCallback;
}
@end
