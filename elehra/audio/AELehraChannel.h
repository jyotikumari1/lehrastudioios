//
//  AELehraChannel.h
//  eLehra
//
//  Created by Jukka Rauhala on 04/01/16.
//  Copyright (c) 2016 Jukka Rauhala. All rights reserved.
//

#ifndef eLehra_AELehraChannel_h
#define eLehra_AELehraChannel_h
#import "TheAmazingAudioEngine.h"
#import "LehraAudioProcessor.h"
#ifdef __cplusplus
extern "C" {
#endif

typedef long (*AELehraEndPointerCallback) (__unsafe_unretained id    channel,
                                                    
                                                     UInt64                    timestamp
                                                     );

@interface AELehraChannel : NSObject  <AEAudioPlayable>
{
@public
    long position;
}
/*!
 * Initialise
 *
 * @param audioController The Audio Controller
 */
- (id)initWithAudioController:(AEAudioController*)audioController;
-(void) setParam: (LehraFile*) param : (float) maxBeats;
-(float) getBeats;
-(long) getPosition;

 long getEndPosition(id channel,UInt64 timestamp);
long getBufferLength(id channel,long position);

static long getStartPosition (id channel,long timestamp);

/*!
 * Track volume
 *
 * Range: 0.0 to 1.0
 */
@property (nonatomic, assign) float volume;

/*!
 * Track pan
 *
 * Range: -1.0 (left) to 1.0 (right)
 */
@property (nonatomic, assign) float pan;

/*
 * Whether channel is currently playing
 *
 * If this is NO, then the track will be silenced and no further render callbacks
 * will be performed until set to YES again.
 */
@property (nonatomic, assign) BOOL channelIsPlaying;

/*
 * Whether channel is muted
 *
 * If YES, track will be silenced, but render callbacks will continue to be performed.
 */
@property (nonatomic, assign) BOOL channelIsMuted;

/*
 * The audio format for this channel
 */
@property (nonatomic, assign) AudioStreamBasicDescription audioDescription;

@end
#ifdef __cplusplus
}
#endif
#endif
