//
//  AELehraChannel.mm
//  eLehra
//
//  Created by Jukka Rauhala on 04/01/16.
//  Copyright (c) 2016 Jukka Rauhala. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AEFloatConverter.h"
#import "AELehraChannel.h"
#include <iostream>

const int kScratchBufferLength = 8192;

const int timeStampLength = 16;

@interface AELehraChannel () {
    float *_buffer;
    float **_dbuffer;
    @public
    LehraFile* lFile;
    float beatCounter;
    float maxBeats;
//    long position;
    long positions[timeStampLength];
    UInt64 timestamps[timeStampLength];
    int timestampIndex;
    int readIndex;
    
}
@property (nonatomic, strong) AEFloatConverter *floatConverter;
@property (nonatomic, weak) AEAudioController *audioController;
@end
@implementation AELehraChannel



 long getEndPosition(id channel,UInt64 timestamp)
{
    AELehraChannel *THIS = (AELehraChannel *) channel;
    int index = THIS->timestampIndex-1;
    if (index < 0)
        index += timeStampLength;
    
    return THIS->positions[index];
 /*   long position = 0;
    if (THIS->readIndex != THIS->timestampIndex)
        position = THIS->positions[THIS->readIndex++];
    
    if (THIS->readIndex >= timeStampLength)
        THIS->readIndex = 0;
    
    return position;*/
    /*int index = -1;
    
    for (int i=0; i < timeStampLength; i++)
    {
        if (THIS->timestamps[i]==timestamp)
            index = i;
    }
    
    if (index > -1)
        return THIS->positions[index];
    
    return THIS->positions[THIS->timestampIndex];*/
    
}

long getBufferLength(id channel,long position)
{
    
    AELehraChannel *THIS = (AELehraChannel *) channel;
    int index = THIS->timestampIndex-1;
    if (index < 0)
        index += timeStampLength;
    int index2 = index-1;
    if (index2 < 0)
        index2 += timeStampLength;
    
    return THIS->positions[index]-THIS->positions[index2];

    
    /*AELehraChannel *THIS = (AELehraChannel *) channel;
    
     int index = -1;
     for (int i=0; i < timeStampLength; i++)
     {
     if (THIS->positions[i]==position)
         index = i;
     }
     
     if (index > -1)
     return THIS->timestamps[index];
     
     return THIS->timestamps[THIS->readIndex];*/
}

static long getStartPosition (id channel,long timestamp){
    AELehraChannel *THIS = (AELehraChannel *) channel;
    int index = -1;
    for (int i=0; i < timeStampLength; i++)
    {
        if (THIS->timestamps[i]==timestamp)
            index = i;
    }
    
    if (index == -1)
        return THIS->positions[THIS->timestampIndex];

    
    int index2 = index--;
    if (index2 < 0)
        index2 = timeStampLength-1;
    
    if (THIS->timestamps[index2] < THIS->timestamps[index])
        return THIS->positions[index2];
    
    return THIS->positions[index];
    
   
}


static OSStatus renderCallback(
                               id channel,
                               AEAudioController *audioController,
                               const AudioTimeStamp *time,
                               UInt32 frames,
                               AudioBufferList *audio
                               ) {
    //std::cout << "AELehraChannel time:"<<time->mHostTime << "\n";
    AELehraChannel *THIS = (AELehraChannel *) channel;
    if (!THIS->_channelIsPlaying)
        return noErr;
    
    if (THIS->lFile == nil)
        return noErr;
    
    LehraAudioProcessor::readLehraData(THIS->lFile, frames*2, THIS->_dbuffer[0], frames, 0, &THIS->beatCounter);
    THIS->position = ftell(THIS->lFile);
    
    THIS->positions[THIS->timestampIndex] = THIS->position;
    THIS->timestamps[THIS->timestampIndex++] = (UInt64)frames;//time->mHostTime;
    
    if (THIS->timestampIndex >= timeStampLength)
        THIS->timestampIndex = 0;
    
    
    //THIS->beatCounter += (float)frames * BEATS_PER_BYTE * data->ui_bmpValue;

   // if (THIS->beatCounter >= THIS->maxBeats)
   //     THIS->beatCounter -= THIS->maxBeats;
    if ( frames > 0 ) {
        // Convert back to buffer
        //memcpy(THIS->_dbuffer[0],THIS->_buffer, sizeof)
        AEFloatConverterFromFloat(THIS->_floatConverter, THIS->_dbuffer, audio, frames);
        //memcpy(audio->mBuffers[0].mData,THIS->_buffer,  frames * sizeof(float));
    }
    

    
    return noErr;
}

-(float) getBeats
{
    return beatCounter;//(int)floor(beatCounter);
}

-(long) getPosition
{
    return position;
}

- (id)initWithAudioController:(AEAudioController *)audioController {
    if ( !(self = [super init]) ) return nil;
    
    self.audioController = audioController;
    self.audioDescription = audioController.audioDescription;
    self.floatConverter = [[AEFloatConverter alloc] initWithSourceFormat:self.audioDescription];

    timestampIndex = 0;
    readIndex = 0;
    memset(positions,0,sizeof(long)*timeStampLength);
    memset(timestamps,0,sizeof(UInt64)*timeStampLength);
    //_buffer = (float*)malloc(sizeof(float) * kScratchBufferLength);
    beatCounter = 0;
    //_audioControllerRef = audioController;
    self.volume = 1.0;
    self.pan = 0.0;
    self.channelIsMuted = NO;
    self.channelIsPlaying = NO;
    _dbuffer = (float**)malloc(sizeof(float*) * self.audioDescription.mChannelsPerFrame);
    assert(_dbuffer);
    for ( int i=0; i<self.audioDescription.mChannelsPerFrame; i++ ) {
        _dbuffer[i] = (float*)malloc(sizeof(float) * kScratchBufferLength);
        assert(_dbuffer[i]);
    }
    return self;
}


- (BOOL)playing {
    return self.channelIsPlaying;
}
-(void)dealloc {
    for ( int i=0; i<self.audioDescription.mChannelsPerFrame; i++ ) {
        free(_dbuffer[i]);
    }
    free(_dbuffer);
    self.audioController = nil;
}

-(void)setClientFormat:(AudioStreamBasicDescription)clientFormat {
    self.audioDescription = clientFormat;
    self.floatConverter = [[AEFloatConverter alloc] initWithSourceFormat:self.audioDescription];

   /* AEFloatConverter *floatConverter = [[AEFloatConverter alloc] initWithSourceFormat:clientFormat];
    
    float **scratchBuffer = (float**)malloc(sizeof(float*) * clientFormat.mChannelsPerFrame);
    assert(scratchBuffer);
    for ( int i=0; i<clientFormat.mChannelsPerFrame; i++ ) {
        scratchBuffer[i] = (float*)malloc(sizeof(float) * kScratchBufferLength);
        assert(scratchBuffer[i]);
    }
    
    //AELimiter *limiter = [[AELimiter alloc] initWithNumberOfChannels:clientFormat.mChannelsPerFrame sampleRate:clientFormat.mSampleRate];
    float** oldScratchBuffer = _scratchBuffer;
    AudioStreamBasicDescription oldClientFormat = _clientFormat;
    
    [_audioController performSynchronousMessageExchangeWithBlock:^{
       // _limiter = limiter;
        _floatConverter = floatConverter;
        _scratchBuffer = scratchBuffer;
        _clientFormat = clientFormat;
    }];
    
    for ( int i=0; i<oldClientFormat.mChannelsPerFrame; i++ ) {
        free(oldScratchBuffer[i]);
    }
    free(oldScratchBuffer);*/
}

-(void) setParam: (LehraFile*) param : (float) _maxBeats
{
    lFile = param;
    maxBeats = _maxBeats;
}

- (AEAudioControllerRenderCallback)renderCallback {
    return renderCallback;
}

- (AELehraEndPointerCallback)getEndPointerCallback {
    return getEndPosition;
}

@end
