//
//  AELehraMetronome.h
//  elehra
//
//  Created by Jukka Rauhala on 22/03/16.
//  Copyright © 2016 AK Studios. All rights reserved.
//

#ifndef AELehraMetronome_h
#define AELehraMetronome_h
#import "TheAmazingAudioEngine.h"
#import "AudioProcessorData.h"
#import "AELehraChannel.h"

@interface AELehraMetronome : NSObject  <AEAudioPlayable>
/*!
 * Initialise
 *
 * @param audioController The Audio Controller
 */
- (id)initWithAudioController:(AEAudioController*)audioController  lehraChannel:(AELehraChannel*) lehraChannel;
-(void) setParam: (float) bpmOrig : (float) bpmCurrent ;
-(void) setCycleLength : (int) cycleLength;

@property (nonatomic, assign) AudioStreamBasicDescription clientFormat;

/*!
 * Track volume
 *
 * Range: 0.0 to 1.0
 */
@property (nonatomic, assign) float volume;

/*!
 * Track pan
 *
 * Range: -1.0 (left) to 1.0 (right)
 */
@property (nonatomic, assign) float pan;

/*
 * Whether channel is currently playing
 *
 * If this is NO, then the track will be silenced and no further render callbacks
 * will be performed until set to YES again.
 */
@property (nonatomic, assign) BOOL channelIsPlaying;

/*
 * Whether channel is muted
 *
 * If YES, track will be silenced, but render callbacks will continue to be performed.
 */
@property (nonatomic, assign) BOOL channelIsMuted;

/*
 * The audio format for this channel
 */
@property (nonatomic, assign) AudioStreamBasicDescription audioDescription;


@end


#endif /* AELehraMetronome_h */
