//
//  AELehraMetronome.m
//  elehra
//
//  Created by Jukka Rauhala on 22/03/16.
//  Copyright © 2016 AK Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AEFloatConverter.h"
#import "AELehraMetronome.h"
#import "AELehraChannel.h"
#import "LehraAudioProcessor.h"
//#include <stdlib.h>
//#include <string.h>
#include <iostream>
const int kScratchBufferLength = 8192;
#define CBUFFER_LENGTH 4096

#define METRONOME_POLYPHONY 16


@interface AELehraMetronome () {
    float **_scratchBuffer;
    float *_buffer;
    //AudioProcessorData* data;
    //LehraAudioProcessor* processor;
    float pitch;
    float tsCoeff;
    TimeStretcherPL* tsComponent;
    SamplingRateConverter* srComponent;
    float *_cbuffer;
    int cbuffer_read;
    int cbuffer_write;
    float maxval;
    
    float bpmOrig;
    float bpmCurrent;
    
    float *sound[2];
    long soundLength[2];
    //int soundIndex;
    
    UInt64 lastEndPosition;
    UInt64 lastLehraPosition;
    
    // arrays for metronome notes
    bool isSoundActive[METRONOME_POLYPHONY];
    long soundIndex[METRONOME_POLYPHONY];
    int soundVersion[METRONOME_POLYPHONY];
    
    int cycleLength;
    int metronomePatterns[10][16];
    int lastBeat;
    
}
@property (nonatomic, strong) AEFloatConverter *floatConverter;
@property (nonatomic, weak) AEAudioController *audioController;
@property (nonatomic, weak) AELehraChannel *lehraChannel;
@property (nonatomic, assign) bool isMetronomeSoundOn;
@end

@implementation AELehraMetronome

- (id)initWithAudioController:(AEAudioController *)audioController lehraChannel:(AELehraChannel*) lehraChannel{
    if ( !(self = [super init]) ) return nil;
    _cbuffer = (float*) malloc(CBUFFER_LENGTH * sizeof(float));
    memset(_cbuffer,0,CBUFFER_LENGTH*sizeof(float));
    self.audioController = audioController;
    self.lehraChannel = lehraChannel;
    _clientFormat = audioController.audioDescription;
    self.floatConverter = [[AEFloatConverter alloc] initWithSourceFormat:_clientFormat];
    
    // self.limiter = [[AELimiter alloc] initWithNumberOfChannels:_clientFormat.mChannelsPerFrame sampleRate:_clientFormat.mSampleRate];
    
    _scratchBuffer = (float**)malloc(sizeof(float*) * _clientFormat.mChannelsPerFrame);
    assert(_scratchBuffer);
    for ( int i=0; i<_clientFormat.mChannelsPerFrame; i++ ) {
        _scratchBuffer[i] = (float*)malloc(sizeof(float) * kScratchBufferLength);
        assert(_scratchBuffer[i]);
    }
    
    cycleLength = 6;
    // hsf = new HighShelvingFilter();
    //_buffer = (float*)malloc(sizeof(float) * kScratchBufferLength);
    lastEndPosition = 0;
    lastLehraPosition = -1;
    bpmOrig = 120;
    bpmCurrent = 120;
    lastBeat = -1;
    self.isMetronomeSoundOn = false;
    [self loadSound : 0 : @"MetronomeUp.wav"];
    [self loadSound : 1 : @"Metronome.wav"];
    
    for (int i=0; i < METRONOME_POLYPHONY; i++)
    {
        isSoundActive[i] = false;
        soundIndex[i] = 0;
        soundVersion[i] = 0;
    }
    
    // set metronome patterns
    int temp[16*10]={0,1,1,0,1,1, 0,0,0,0,0,0,0,0,0,0,
                    0,1,1,0,1,0, 1,0,0,0,0,0,0,0,0,0,
                    0,1,1,1,0,1, 1,1,0,0,0,0,0,0,0,0,
                    0,1,0,1,1,1, 0,1,1,0,0,0,0,0,0,0,
        
                    0,1,0,1,1,1, 1,0,1,1,0,0,0,0,0,0,
                    0,1,0,1,1,1, 0,1,0,1,1,0,0,0,0,0,
                    0,1,1,1,0,1, 1,1,0,1,0,1,0,0,0,0,
        
                    0,1,1,1,1,0, 1,1,1,1,0,1,1,1,0,0,
                    0,1,1,1,0,1, 1,1,1,1,1,0,1,1,1,0,
                    0,1,1,1,0,1, 1,1,1,1,1,1,0,1,1,1
    };
    for (int i=0; i < 10; i++)
        memcpy(&metronomePatterns[i],&temp[0] + i*16,sizeof(int)*16);

    return self;
}

-(void) setCycleLength : (int) _cycleLength
{
    cycleLength = _cycleLength;
    
    // reset lastLehraPosition
    lastLehraPosition = 20000000;
}

-(void) loadSound : (int) index : (NSString*) name{
  //  NSLog(@"%d",self.isMetronomeSoundOn);
    NSBundle *b = [NSBundle mainBundle];
    NSString *dir = [b bundlePath];
    NSString* filename = [dir stringByAppendingPathComponent:name ] ;//@"Metronome.wav"];
    byte bbuffer[70000];

    FILE* f = fopen([filename UTF8String],"rb");
    if (f != NULL) {
        fseek(f, 0L, SEEK_END);
        soundLength[index]  = (long)(ftell(f)-110) / (long)2;
        fseek(f, 110, SEEK_SET);
        sound[index] = (float*)malloc(sizeof(float)*soundLength[index]);
        float *soundp = sound[index];
        memset(soundp,0,sizeof(float)*soundLength[index]);
        long pos = 0;
        while (!feof(f))
        {
            long ret = (long)fread(&bbuffer[0],1, 1024,f);
        // convert byte to float
        for (long i = 0; i < ret / 2; i++)
        {
            //for (int k = 0; k < CHANNELS; k++)
            int k=0;
                 short shortSample = 0;
                long index = (i * 2+k);//*CHANNELS;
                shortSample = (short)(bbuffer[index] + (((int)bbuffer[index + 1]) << 8));
                soundp[pos+i] =  (float)shortSample * 0.00003052;
        }
            pos +=ret / 2;
        }
    }
   /* float temp[60000];
    memcpy(temp,sound,sizeof(float)*60000);
    int ii=0;
    ii = 1;*/
   
}

-(void)dealloc {
    for ( int i=0; i<_clientFormat.mChannelsPerFrame; i++ ) {
        free(_scratchBuffer[i]);
    }
    free(_scratchBuffer);
    //free(_buffer);
    free(_cbuffer);
    free(sound[0]);
    free(sound[1]);
    //delete(hsf);
    //delete(srComponent);
    self.audioController = nil;
}

-(void)setClientFormat:(AudioStreamBasicDescription)clientFormat {
    
    AEFloatConverter *floatConverter = [[AEFloatConverter alloc] initWithSourceFormat:clientFormat];
    
    float **scratchBuffer = (float**)malloc(sizeof(float*) * clientFormat.mChannelsPerFrame);
    assert(scratchBuffer);
    for ( int i=0; i<clientFormat.mChannelsPerFrame; i++ ) {
        scratchBuffer[i] = (float*)malloc(sizeof(float) * kScratchBufferLength);
        assert(scratchBuffer[i]);
    }
    
    
    //AELimiter *limiter = [[AELimiter alloc] initWithNumberOfChannels:clientFormat.mChannelsPerFrame sampleRate:clientFormat.mSampleRate];
    float** oldScratchBuffer = _scratchBuffer;
    AudioStreamBasicDescription oldClientFormat = _clientFormat;
    
    [_audioController performSynchronousMessageExchangeWithBlock:^{
        // _limiter = limiter;
        _floatConverter = floatConverter;
        _scratchBuffer = scratchBuffer;
        _clientFormat = clientFormat;
    }];
    
    for ( int i=0; i<oldClientFormat.mChannelsPerFrame; i++ ) {
        free(oldScratchBuffer[i]);
    }
    free(oldScratchBuffer);
}

-(void) setParam: (float) _bpmOrig : (float) _bpmCurrent
{
    bpmOrig = _bpmOrig;
    bpmCurrent = _bpmCurrent;
}

-(float) getLevel
{
    return 0;
}

-(void) reset
{
}

static OSStatus renderCallback(
                               id channel,
                               AEAudioController *audioController,
                               const AudioTimeStamp *time,
                               UInt32 frames,
                               AudioBufferList *audio
                               ) {
    //std::cout << "AELehraMetronome time:"<<time->mHostTime << "\n";

    //OSStatus status = producer(producerToken, audio, &frames);
    //if ( status != noErr ) return noErr;

    // Copy buffer into floating point scratch buffer
    //AEFloatConverterToFloat(THIS->_floatConverter, audio, THIS->_scratchBuffer, frames);
    AELehraMetronome *THIS = (AELehraMetronome *) channel;
    if (!THIS->_channelIsPlaying)
        return noErr;
    memset(THIS->_scratchBuffer[0],0,sizeof(float)*kScratchBufferLength);
    for (int k=0; k < METRONOME_POLYPHONY; k++)
    {
    if (THIS->isSoundActive[k])
    {
        long soundLen = THIS->soundLength[THIS->soundVersion[k]];
        float* soundp = THIS->sound[THIS->soundVersion[k]];
        
        int len =frames;
        
        if (len + THIS->soundIndex[k] > soundLen)
            len = (int)(soundLen - THIS->soundIndex[k]);
        
        for (int i=0; i < len; i++)
        {
            THIS->_scratchBuffer[0][i] = THIS->_scratchBuffer[0][i] + soundp[THIS->soundIndex[k]++];
        }
        //THIS->soundIndex[k] += len;
     //std::cout << "Metronome tone:"<<THIS->soundIndex[k] << "\n";
        if (THIS->soundIndex[k] >= soundLen)
            THIS->isSoundActive[k] = false; //THIS->_isMetronomeSoundOn = false;
        
        /*if ( frames > 0 ) {
            AEFloatConverterFromFloat(THIS->_floatConverter, THIS->_scratchBuffer, audio, frames);
        }*/
    }
    }
    long position =  getEndPosition(THIS->_lehraChannel, (UInt64)time->mHostTime);//THIS->_lehraChannel->position;
    long len = 2*frames * THIS->bpmCurrent /THIS->bpmOrig  ;//getBufferLength(THIS->_lehraChannel, position);

    bool metronomeEvent = false;
        if (position >= THIS->lastLehraPosition)
        {
            position = THIS->lastEndPosition + len;
        } else {
            THIS->lastLehraPosition = position;
            metronomeEvent = true;
        }
        THIS->lastEndPosition = position;

        
    float beats = ((float)position*THIS->bpmOrig/ (88200.0f*60.0f));
    float beatsDelta =((float)len*THIS->bpmOrig/ (88200.0f*60.0f)); //frames * THIS->bpmCurrent / (44100.0f * 60.0f);
    //std::cout << "Metronome beats:"<<beats << "/" << beats+beatsDelta << " pos:" << position << "/" << position+len << " len:"<< len << " delta:"<< beatsDelta << " bpm:" << THIS->bpmCurrent << "/" << THIS->bpmOrig <<"\n";
    float startBeats = beats - beatsDelta;
    float beatsFloor = floor(beats);
    int beat = (int)beatsFloor;
    while(beat >= THIS->cycleLength)
    {
        beat -= THIS->cycleLength;
    }
    
    if ((beatsFloor-floor(startBeats) >= 1.0f || metronomeEvent) && beat != THIS->lastBeat)
    {
        
        int index=0;
        while (index < METRONOME_POLYPHONY && THIS->isSoundActive[index]) {
            index++;
        }
        
        if (index < METRONOME_POLYPHONY) {
            THIS->lastBeat = beat;
            
            int mi = THIS->cycleLength-6;
            if (mi > 6)
                mi = mi-1;
            int mbeat = THIS->metronomePatterns[mi][beat];
            
            float* soundp = THIS->sound[mbeat];
            THIS->soundVersion[index] = mbeat;
            
            
        THIS->isSoundActive[index] = true;
        std::cout << "Metronome EVENT:"<<beatsFloor << " mi:" << mi << " mbeat:" << mbeat << " beat:"<< beat <<"\n";
        int frameIndex = (beatsFloor - startBeats) * frames / beatsDelta;
        THIS->soundIndex[index] = 0;
    for (int i=0; frameIndex+i < frames; i++)
    {
        THIS->_scratchBuffer[0][frameIndex+i] = THIS->_scratchBuffer[0][frameIndex+i] + soundp[THIS->soundIndex[index]++];
    }
        }
    

    }
    if ( frames > 0 ) {
        AEFloatConverterFromFloat(THIS->_floatConverter, THIS->_scratchBuffer, audio, frames);
    }
    //}
    return noErr;
}
- (AEAudioControllerRenderCallback)renderCallback {
    return renderCallback;
}
@end
