//
//  AELevelAnalyzer.h
//  eLehra
//
//  Created by Jukka Rauhala on 04/01/16.
//  Copyright (c) 2015 Jukka Rauhala. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AEFloatConverter.h"
#import "AELevelAnalyzer.h"
#import "TimeStretcherPL.h"
#import "SamplingRateConverter.h"
#import "LehraAudioProcessor.h"
const int kScratchBufferLength = 8192;
#define CBUFFER_LENGTH 4096
#define AUDIO_LEVEL_SMOOTHER    0.9

#define AUDIO_LEVEL_COUNTER_FRAME   512

@interface AELevelAnalyzer () {
    float **_scratchBuffer;
    float *_buffer;
    //AudioProcessorData* data;
    //LehraAudioProcessor* processor;
    float pitch;
    float tsCoeff;
    //HighShelvingFilter* hsf;
    float *_cbuffer;
    int cbuffer_read;
    int cbuffer_write;
    float audioLevel;
    float audioLevelMem;
    int audioLevelCounter;
    float frameLevelMem;
}
@property (nonatomic, strong) AEFloatConverter *floatConverter;
@property (nonatomic, weak) AEAudioController *audioController;
@end
@implementation AELevelAnalyzer

- (id)initWithAudioController:(AEAudioController *)audioController {
    if ( !(self = [super init]) ) return nil;
    _cbuffer = (float*) malloc(CBUFFER_LENGTH * sizeof(float));
    memset(_cbuffer,0,CBUFFER_LENGTH*sizeof(float));
    self.audioController = audioController;
    _clientFormat = audioController.audioDescription;
    self.floatConverter = [[AEFloatConverter alloc] initWithSourceFormat:_clientFormat];

   // self.limiter = [[AELimiter alloc] initWithNumberOfChannels:_clientFormat.mChannelsPerFrame sampleRate:_clientFormat.mSampleRate];
    
    _scratchBuffer = (float**)malloc(sizeof(float*) * _clientFormat.mChannelsPerFrame);
    assert(_scratchBuffer);
    for ( int i=0; i<_clientFormat.mChannelsPerFrame; i++ ) {
        _scratchBuffer[i] = (float*)malloc(sizeof(float) * kScratchBufferLength);
        assert(_scratchBuffer[i]);
    }
    
   // hsf = new HighShelvingFilter();
    audioLevel = 0;
    audioLevelCounter = 0;
    audioLevelMem = 0;
    frameLevelMem = 0;
    //_buffer = (float*)malloc(sizeof(float) * kScratchBufferLength);
    return self;
}

-(void)dealloc {
    for ( int i=0; i<_clientFormat.mChannelsPerFrame; i++ ) {
        free(_scratchBuffer[i]);
    }
    free(_scratchBuffer);
    //free(_buffer);
    free(_cbuffer);
    
    //delete(hsf);
    //delete(srComponent);
    self.audioController = nil;
}

-(void)setClientFormat:(AudioStreamBasicDescription)clientFormat {
    
    AEFloatConverter *floatConverter = [[AEFloatConverter alloc] initWithSourceFormat:clientFormat];
    
    float **scratchBuffer = (float**)malloc(sizeof(float*) * clientFormat.mChannelsPerFrame);
    assert(scratchBuffer);
    for ( int i=0; i<clientFormat.mChannelsPerFrame; i++ ) {
        scratchBuffer[i] = (float*)malloc(sizeof(float) * kScratchBufferLength);
        assert(scratchBuffer[i]);
    }
    

    //AELimiter *limiter = [[AELimiter alloc] initWithNumberOfChannels:clientFormat.mChannelsPerFrame sampleRate:clientFormat.mSampleRate];
    float** oldScratchBuffer = _scratchBuffer;
    AudioStreamBasicDescription oldClientFormat = _clientFormat;
    
    [_audioController performSynchronousMessageExchangeWithBlock:^{
       // _limiter = limiter;
        _floatConverter = floatConverter;
        _scratchBuffer = scratchBuffer;
        _clientFormat = clientFormat;
    }];
    
    for ( int i=0; i<oldClientFormat.mChannelsPerFrame; i++ ) {
        free(oldScratchBuffer[i]);
    }
    free(oldScratchBuffer);
}

-(void) setParam: (float) _pitch : (float) _tsCoeff
{
    pitch = _pitch;
    tsCoeff = _tsCoeff;
}

-(float) getLevel
{
    return audioLevel;
}

-(void) reset
{
    audioLevel = 0;
    audioLevelMem = 0;
    audioLevelCounter = 0;
}

static OSStatus filterCallback(__unsafe_unretained AELevelAnalyzer *THIS,
                               __unsafe_unretained AEAudioController *audioController,
                               AEAudioControllerFilterProducer producer,
                               void *producerToken,
                               const AudioTimeStamp *time,
                               UInt32 frames,
                               AudioBufferList *audio) {
   
    OSStatus status = producer(producerToken, audio, &frames);
    if ( status != noErr ) return status;
    
    // Copy buffer into floating point scratch buffer
    AEFloatConverterToFloat(THIS->_floatConverter, audio, THIS->_scratchBuffer, frames);
    //memcpy(THIS->_buffer, audio->mBuffers[0].mData, tsBufferLen * sizeof(float));
    float level = THIS->frameLevelMem;//0.0f;

    for (int i=0; i < frames; i++)
    {
        float sample = THIS->_scratchBuffer[0][i]*100.0f;//32.7680f;
        float tempx= sample*sample;//data->fbuffer[0][i]*data->fbuffer[0][i];
        level += tempx;
        
        THIS->audioLevelCounter++;
        if (THIS->audioLevelCounter >= AUDIO_LEVEL_COUNTER_FRAME)
        {
            THIS->audioLevelCounter = 0;
            level = sqrt(level) / (float)(frames);
            THIS->audioLevel = level * (1.0f-AUDIO_LEVEL_SMOOTHER) + THIS->audioLevelMem * AUDIO_LEVEL_SMOOTHER;
            THIS->audioLevelMem = THIS->audioLevel;
            level = 0;
        }
    }
    
    THIS->frameLevelMem = level;

    /*THIS->hsf->Process(THIS->_scratchBuffer[0], frames, 6000.0f, 15.0f);

    
    if ( frames > 0 ) {
            AEFloatConverterFromFloat(THIS->_floatConverter, THIS->_scratchBuffer, audio, frames);
    }*/
    return noErr;
}
-(AEAudioControllerFilterCallback)filterCallback {
    return filterCallback;
}
@end
