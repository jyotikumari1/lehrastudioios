//
//  AETimeStretcher.h
//  eLehra
//
//  Created by Jukka Rauhala on 04/01/16.
//  Copyright (c) 2015 Jukka Rauhala. All rights reserved.
//

#ifndef eLehra_AETimeStretcher_h
#define eLehra_AETimeStretcher_h
#import "TheAmazingAudioEngine.h"
#import "AudioProcessorData.h"

@interface AETimeStretcher : NSObject  <AEAudioFilter>
/*!
 * Initialise
 *
 * @param audioController The Audio Controller
 */
- (id)initWithAudioController:(AEAudioController*)audioController;
-(void) setParam: (float) pitch : (float) tsCoeff ;

@property (nonatomic, assign) AudioStreamBasicDescription clientFormat;


@end
#endif
