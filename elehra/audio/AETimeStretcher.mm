//
//  AETimeStretcher.h
//  eLehra
//
//  Created by Jukka Rauhala on 04/01/16.
//  Copyright (c) 2015 Jukka Rauhala. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AEFloatConverter.h"
#import "AETimeStretcher.h"
#import "TimeStretcherPL.h"
#import "SamplingRateConverter.h"
#import "LehraAudioProcessor.h"
#include <iostream>
const int kScratchBufferLength = 8192;
#define CBUFFER_LENGTH 4096

@interface AETimeStretcher () {
    float **_scratchBuffer;
    float *_buffer;
    //AudioProcessorData* data;
    //LehraAudioProcessor* processor;
    float pitch;
    float tsCoeff;
    TimeStretcherPL* tsComponent;
    SamplingRateConverter* srComponent;
    float *_cbuffer;
    int cbuffer_read;
    int cbuffer_write;
    float maxval;
}
@property (nonatomic, strong) AEFloatConverter *floatConverter;
@property (nonatomic, weak) AEAudioController *audioController;
@end
@implementation AETimeStretcher

- (id)initWithAudioController:(AEAudioController *)audioController {
    if ( !(self = [super init]) ) return nil;
    _cbuffer = (float*) malloc(CBUFFER_LENGTH * sizeof(float));
    memset(_cbuffer,0,CBUFFER_LENGTH*sizeof(float));
    self.audioController = audioController;
    _clientFormat = audioController.audioDescription;
    self.floatConverter = [[AEFloatConverter alloc] initWithSourceFormat:_clientFormat];

   // self.limiter = [[AELimiter alloc] initWithNumberOfChannels:_clientFormat.mChannelsPerFrame sampleRate:_clientFormat.mSampleRate];
    
    _scratchBuffer = (float**)malloc(sizeof(float*) * _clientFormat.mChannelsPerFrame);
    assert(_scratchBuffer);
    for ( int i=0; i<_clientFormat.mChannelsPerFrame; i++ ) {
        _scratchBuffer[i] = (float*)malloc(sizeof(float) * kScratchBufferLength);
        assert(_scratchBuffer[i]);
    }
    
    tsComponent = new TimeStretcherPL();
    srComponent = new SamplingRateConverter();
    maxval = 0.0f;
    cbuffer_read = 0;
    cbuffer_write = 0;
    
    _buffer = (float*)malloc(sizeof(float) * kScratchBufferLength);
    return self;
}

-(void)dealloc {
    for ( int i=0; i<_clientFormat.mChannelsPerFrame; i++ ) {
        free(_scratchBuffer[i]);
    }
    free(_scratchBuffer);
    free(_buffer);
    free(_cbuffer);
    
    delete(tsComponent);
    delete(srComponent);
    self.audioController = nil;
}

-(void)setClientFormat:(AudioStreamBasicDescription)clientFormat {
    
    AEFloatConverter *floatConverter = [[AEFloatConverter alloc] initWithSourceFormat:clientFormat];
    
    float **scratchBuffer = (float**)malloc(sizeof(float*) * clientFormat.mChannelsPerFrame);
    assert(scratchBuffer);
    for ( int i=0; i<clientFormat.mChannelsPerFrame; i++ ) {
        scratchBuffer[i] = (float*)malloc(sizeof(float) * kScratchBufferLength);
        assert(scratchBuffer[i]);
    }
    

    //AELimiter *limiter = [[AELimiter alloc] initWithNumberOfChannels:clientFormat.mChannelsPerFrame sampleRate:clientFormat.mSampleRate];
    float** oldScratchBuffer = _scratchBuffer;
    AudioStreamBasicDescription oldClientFormat = _clientFormat;
    
    [_audioController performSynchronousMessageExchangeWithBlock:^{
       // _limiter = limiter;
        _floatConverter = floatConverter;
        _scratchBuffer = scratchBuffer;
        _clientFormat = clientFormat;
    }];
    
    for ( int i=0; i<oldClientFormat.mChannelsPerFrame; i++ ) {
        free(oldScratchBuffer[i]);
    }
    free(oldScratchBuffer);
}

-(void) setParam: (float) _pitch : (float) _tsCoeff
{
    pitch = _pitch;
    tsCoeff = _tsCoeff;
}

static OSStatus filterCallback(__unsafe_unretained AETimeStretcher *THIS,
                               __unsafe_unretained AEAudioController *audioController,
                               AEAudioControllerFilterProducer producer,
                               void *producerToken,
                               const AudioTimeStamp *time,
                               UInt32 frames,
                               AudioBufferList *audio) {
    //std::cout << "AETimeStretcher time:"<<time->mHostTime << "\n";
    //float pitch=0;
    //float tsCoeff=0;
    //LehraAudioProcessor::calculateCoeffs(THIS->data, &pitch, &tsCoeff);
#define TARGET_SIZE 2048
    
   // int32_t availableBytes;
   // TPCircularBufferTail(&THIS->_cbuffer, &availableBytes);
   // UInt32 availableFrames = availableBytes/sizeof(float);
    int availableFrames = THIS->cbuffer_write-THIS->cbuffer_read;
    if (availableFrames < 0)
        availableFrames+=CBUFFER_LENGTH;
    
    if (availableFrames >= frames)
    {
        if ( frames > 0 ) {
            // Convert back to buffer
            //float *buffer = (float*)TPCircularBufferTail(&THIS->_cbuffer, &availableBytes);
            int framesCount = frames;
            if (CBUFFER_LENGTH-THIS->cbuffer_read < frames)
                framesCount = CBUFFER_LENGTH-THIS->cbuffer_read;
            
            memcpy(THIS->_scratchBuffer[0],THIS->_cbuffer+THIS->cbuffer_read,sizeof(float)*framesCount);
            THIS->cbuffer_read += framesCount;
            if (framesCount < frames)
            {
                int framesCount2 = frames - framesCount;
                memcpy(THIS->_scratchBuffer[0]+framesCount,THIS->_cbuffer,sizeof(float)*framesCount2);
                THIS->cbuffer_read = framesCount2;
                
            }
            //TPCircularBufferConsumeNoBarrier(&THIS->_cbuffer, frames * sizeof(float));
           AEFloatConverterFromFloat(THIS->_floatConverter, THIS->_scratchBuffer, audio, frames);
            //memcpy(audio->mBuffers[0].mData,THIS->_buffer,  frames * sizeof(float));
        }
        return noErr;
    }

    int neededFrames = TARGET_SIZE-availableFrames;
    int srcBufferLen = THIS->srComponent->GetInputFrameSize(THIS->pitch, TARGET_SIZE-availableFrames);
    UInt32 tsBufferLen = (UInt32)THIS->tsComponent->GetInputFrameSize( THIS->tsCoeff,  srcBufferLen);
    
    
    OSStatus status = producer(producerToken, audio, &tsBufferLen);
    if ( status != noErr ) return status;
    
    // Copy buffer into floating point scratch buffer
    AEFloatConverterToFloat(THIS->_floatConverter, audio, THIS->_scratchBuffer, tsBufferLen);
    //memcpy(THIS->_buffer, audio->mBuffers[0].mData, tsBufferLen * sizeof(float));

    

    
    //float temp[4096];
    //memcpy(temp,THIS->_scratchBuffer[0],sizeof(float)*tsBufferLen);
    
    THIS->tsComponent->Process(THIS->_scratchBuffer[0], tsBufferLen,  srcBufferLen,  THIS->tsCoeff);
    
    //float temp1[4096];
    //memcpy(temp1,THIS->_scratchBuffer[0],sizeof(float)*tsBufferLen);

    
    THIS->srComponent->Process(THIS->_scratchBuffer[0], srcBufferLen,neededFrames, THIS->pitch);
    //TPCircularBufferProduceBytes(&THIS->_cbuffer, THIS->_scratchBuffer[0], neededFrames * sizeof(float));
    
    for (int i=0; i < neededFrames; i++)
    {
        THIS->_scratchBuffer[0][i] = THIS->_scratchBuffer[0][i]*0.0001f;
        //if (THIS->_scratchBuffer[0][i] > THIS->maxval)
        //    THIS->maxval = THIS->_scratchBuffer[0][i];
    }
    

    //float temp2[4096];
    //memcpy(temp2,THIS->_scratchBuffer[0],sizeof(float)*tsBufferLen);
    
    int framesCount = neededFrames;
    if (CBUFFER_LENGTH-THIS->cbuffer_write < neededFrames)
        framesCount = CBUFFER_LENGTH-THIS->cbuffer_write;
    
    memcpy(THIS->_cbuffer+THIS->cbuffer_write,THIS->_scratchBuffer[0],sizeof(float)*framesCount);
    THIS->cbuffer_write += framesCount;
    if (framesCount < neededFrames)
    {
        int framesCount2 = neededFrames - framesCount;
        memcpy(THIS->_cbuffer,THIS->_scratchBuffer[0]+framesCount,sizeof(float)*framesCount2);
        THIS->cbuffer_write = framesCount2;
        
    }
    

    //THIS->BitCrush->Process(THIS->_scratchBuffer, THIS->_scratchBuffer, frames);
    //AELimiterEnqueue(THIS->_limiter, THIS->_scratchBuffer, frames, NULL);
   // AELimiterDequeue(THIS->_limiter, THIS->_scratchBuffer, &frames, NULL);
    
    if ( frames > 0 ) {
        if ( frames > 0 ) {
            // Convert back to buffer
            //float *buffer = (float*)TPCircularBufferTail(&THIS->_cbuffer, &availableBytes);
            int framesCount = frames;
            if (CBUFFER_LENGTH-THIS->cbuffer_read < frames)
                framesCount = CBUFFER_LENGTH-THIS->cbuffer_read;
            
            memcpy(THIS->_scratchBuffer[0],THIS->_cbuffer+THIS->cbuffer_read,sizeof(float)*framesCount);
            THIS->cbuffer_read += framesCount;
            if (framesCount < frames)
            {
                int framesCount2 = frames - framesCount;
                memcpy(THIS->_scratchBuffer[0]+framesCount,THIS->_cbuffer,sizeof(float)*framesCount2);
                THIS->cbuffer_read = framesCount2;
                
            }
            //TPCircularBufferConsumeNoBarrier(&THIS->_cbuffer, frames * sizeof(float));
            AEFloatConverterFromFloat(THIS->_floatConverter, THIS->_scratchBuffer, audio, frames);
            //memcpy(audio->mBuffers[0].mData,THIS->_buffer,  frames * sizeof(float));
        }
    }
    return noErr;
}
-(AEAudioControllerFilterCallback)filterCallback {
    return filterCallback;
}
@end
