//
//  AudioProcessor.h
//  eLehra
//
//  Created by Jukka Rauhala on 04/05/15.
//  Copyright (c) 2015 AK Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <AudioToolbox/AudioToolbox.h>

#include <AudioUnit/AudioUnit.h>
//#include "ViewController.h"
#include "TimeStretcherPL.h"
#include "SamplingRateConverter.h"
#include "AudioProcessorData.h"
#include "AudioProcessorData_iOS.h"
#include "LehraAudioProcessor.h"
//#define USE_OGG_DECODING 1
#define USE_TAAE
#ifdef USE_TAAE
#import "TheAmazingAudioEngine.h"
#import "AELehraChannel.h"
#import "AETimeStretcher.h"
#import "AEHPFilter.h"
#import "AELevelAnalyzer.h"
#import "AEAudioUnitFilePlayer.h"
#import "AEAudioFilePlayer.h"
#import "AELehraMetronome.h"
#endif

@interface AudioProcessor : NSObject
{
    @public
    struct AudioProcessorData *data;
    struct AudioProcessorData_iOS *dataWrapper;
    LehraAudioProcessor* processor;
}
-(bool) Initialize;// : (ViewController*) _parent;
-(void) LoadFiles;
-(void) DecodeFiles;
-(void) StartAudio;
-(void) StopAudio;
-(void) RestartAudio;

#ifdef USE_TAAE
@property (nonatomic, retain) AEAudioController* audioController;
@property (nonatomic, retain) AELehraChannel* lehraChannel;
@property (nonatomic, retain) AETimeStretcher* lehraTimeStretcher;
@property (nonatomic, retain) AEAudioFilePlayer* tanpuraChannel;
@property (nonatomic, retain) AETimeStretcher* tanpuraTimeStretcher;
@property (nonatomic, retain) AEHPFilter* hpf;
@property (nonatomic, retain) AELevelAnalyzer* levelAnalyzer;
@property (nonatomic, retain) AELehraMetronome* lehraMetronome;
//@property (nonatomic, retain) AETimeStretcher* tanpuraTimeStretcher;

@property (nonatomic, assign) bool isPlaying;

-(bool) startAudioEngine : (NSError*) error;

-(void) stopAudioEngine;
-(void) updateLehraData;
-(void) setLehraVolume :(float) volume;
-(void) setTanpuraVolume :(float) volume;
-(void) setMetronomeVolume :(float) volume;
-(void) getMetrics : (int*) beat : (float*) volume;

#endif
@end
