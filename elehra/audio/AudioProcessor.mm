//
//  AudioProcessor.m
//  eLehra
//
//  Created by Jukka Rauhala on 04/05/15.
//  Copyright (c) 2015 AK Studios. All rights reserved.
//

#import "AudioProcessor.h"
#import "AppDelegate.h"
#include "ViewController.h"
#import <AVFoundation/AVAudioSession.h>
#include <iostream>
#ifdef USE_OGG_DECODING
#include "OggVorbisDecoder.h"
#endif




#define kOutputBus 0
#define kInputBus 1

AudioComponentInstance audioUnit;
AURenderCallbackStruct callbackStruct;
//ViewController* parentInstance;
//TimeStretcher* tsComponentL;
//SamplingRateConverter* srcComponentL;
//TimeStretcher* tsComponentR;
//SamplingRateConverter* srcComponentR;
//FILE* inputFile;
short temp[4096];

float phase = 0.0;

@implementation AudioProcessor


-(id) init
{
    self = [super init];
    if (self)
    {
        
        processor = new LehraAudioProcessor();
        data = processor->getAudioProcessorData();
        dataWrapper = new AudioProcessorData_iOS;
        dataWrapper->data = data;
        dataWrapper->processor = processor;
    }
    return self;
}



OSStatus playbackCallback(void *inRefCon,
                          AudioUnitRenderActionFlags *ioActionFlags,
                          const AudioTimeStamp *inTimeStamp,
                          UInt32 inBusNumber,
                          UInt32 inNumberFrames,
                          AudioBufferList *ioData) {
    //const int numChannels = ioData->mBuffers[0].mNumberChannels;
    short* buffer = (short*) ioData->mBuffers[0].mData;
    
    
    AudioProcessorData_iOS* dataWrapper = (AudioProcessorData_iOS*)inRefCon;
    LehraAudioProcessor* processor = dataWrapper->processor;
    AudioProcessorData* data = dataWrapper->data;
    ViewController* ui =((ViewController*)dataWrapper->parentInstance);
    // for some reason simulator buffersize is half
#if TARGET_IPHONE_SIMULATOR
    int bufferLen = data->bufferSize/2;
#else
    int bufferLen = data->bufferSize;
    
#endif
    data->ui_bmpValue = ui.bmpValue;
    data->ui_isPlaying = ui.isPlaying;
    data->ui_pitchCoeff = ui.pitchCoeff;
    data->ui_lowerCatIndex = ui.lowerCatIndex;
    data->ui_middleCatIndex = ui.middleCatIndex;
    data->ui_upperCatIndex = ui.upperCatIndex;
    data->ui_tanpuraVolume = ui.tanpuraVolume;
    data->ui_lehraVolume = ui.lehraVolume;
#if 1
    processor->processAudio(buffer, bufferLen,0);
#else
    if (ui.isPlaying != data->isPlayingInternal)
    {
        if (ui.isPlaying)
        {
            data->fadeIn = true;
            data->isPlayingInternal = true;
        } else {
            data->fadeOut = true;
        }
    }
    
    if (!data->isPlayingInternal)//ui.isPlaying)
    {
        data->audioLevel = 0.0f;
        memset(buffer, 0, sizeof(short)*bufferLen*CHANNELS);
        return noErr;
    }
    
    
    /*float **fbuffer;
    fbuffer = new float*[2];
    fbuffer[0] = new float[20000];
    fbuffer[1] = new float[20000];*/
    byte bbuffer[20000];
    byte tbuffer[20000];
    
    float ShortMaxInv = 1.0f / (float)32767;
    //float ShortMinInv = -1.0f / (float)-32767;
   // int Channels = 2;
    
    // read bpm and pitch values
    float pitch = ui.pitchCoeff;
    int bpm = ui.bmpValue;
    
    
    bool fileChangeRequired = false;
    int sarangi_bpms[] = {40,50,60,75,90,120,150,180};
    float bpmOld = 0.0f;
    if (data->currentInstrument == 0) {
        bpmOld = (float)sarangi_bpms[data->currentBpm];
    } else {
        bpmOld = 40.0f+data->currentBpm*20.0f;
    }

    if (ui.lowerCatIndex != data->currentRaag)
    {
        data->currentRaag = ui.lowerCatIndex;
        fileChangeRequired = true;
    }
    if (ui.middleCatIndex != data->currentTaal)
    {
        data->currentTaal = ui.middleCatIndex;
        fileChangeRequired = true;
    }
    if (ui.upperCatIndex != data->currentInstrument)
    {
        data->currentInstrument = ui.upperCatIndex;
        fileChangeRequired = true;
    }

    int bpmIndex = 0;
    float tanpuraPitch = pitch * 1.18919841518334f;
    float tanpuraPitch2 = 2.0f*pitch * 1.18919841518334f; // sampling rate 22.5kHz

    if (data->currentInstrument == 0)
    {
        if(bpm >=165 && data->currentTaal<2){
            bpmIndex = 7;
        } else if (bpm >= 135 && data->currentTaal<2)
        {
            bpmIndex = 6;
        } else if (bpm >= 105 && data->currentTaal <2)
        {
            bpmIndex = 5;
        }
    else if (bpm >= 83){
        bpmIndex = 4;
    } else if (bpm >= 67){
        bpmIndex = 3;
    } else if (bpm >= 55){
        bpmIndex = 2;
    } else if (bpm >= 45)
    {
        bpmIndex = 1;
    }
        pitch = pitch * 0.94388068f; // compensate for the sarangi's tuning
        
        
    } else {
        if (bpm >= 110 && (data->currentTaal == 1 ||data->currentTaal == 3)){
            bpmIndex = 4;
        } else if (bpm >= 90){
            bpmIndex = 3;
        } else if (bpm >= 70){
            bpmIndex = 2;
        } else if (bpm >= 50)
        {
            bpmIndex = 1;
        }
        
    }

    float bpmOrig = 0;
    if (data->currentInstrument == 0) {
        bpmOrig = (float)sarangi_bpms[bpmIndex];
    } else {
        bpmOrig = 40.0f+bpmIndex*20.0f;
    }
    
    if (bpmIndex != data->currentBpm || fileChangeRequired)
    {
        NSLog(@"bpmIndex:%d", bpmIndex);
        long oldposition = ftell(data->inputFile) - 44;
        data->inputFile = data->allFiles[getFileIndex(data->currentInstrument, data->currentTaal, data->currentRaag, bpmIndex)];
        fseek(data->inputFile,44,SEEK_SET);
        long position = (long)((float)oldposition * bpmOld*0.5 / bpmOrig) * 2;//(long)(data->beatCounter *60.0f*44100.0f / bpmOrig);
        std::cout << "Old pos:" << oldposition << " New pos:" << position << "\n";
        //position = position * 2 * CHANNELS;
        fseek(data->inputFile, position, SEEK_CUR);
        data->currentBpm = bpmIndex;
    }
    

    
    // determine how much new data we need
    int srcBufferLen = data->srcComponentL->GetInputFrameSize(pitch, bufferLen);
    //float bpmOrig = 20.0f+bpmIndex*20.0f;
    float bpmCoeff = (float)bpm / bpmOrig;
    float tsCoeff = pitch * bpmCoeff;
    
    
    
    
    int tsBufferLen = data->tsComponentL->GetInputFrameSize(tsCoeff, srcBufferLen);
    int srcBufferLen2 = data->srcComponentX->GetInputFrameSize(tanpuraPitch2, bufferLen);
#if TANPURA_BPM_CONTROL
    int tsBufferLen2 = data->tsComponentX->GetInputFrameSize(tanpuraPitch, srcBufferLen2);
//    int tsBufferLen2 = data->tsComponentX->GetInputFrameSize(tanpuraPitch, srcBufferLen2);
#else
    int tsBufferLen2 = tsBufferLen;
#endif
    
    if (feof(data->inputFile))
    {
        fseek(data->inputFile, 44, SEEK_SET);
        data->beatCounter = 0;
        NSLog(@"Input file end.");
    }
    if (feof(data->tanpuraFile))
    {
        fseek(data->tanpuraFile, 44, SEEK_SET);
        //memset(&tbuffer[0],0,sizeof(byte)*tsBufferLen*4);
    }
    // read data from file
    int len = tsBufferLen*2;//*CHANNELS;
    int ret = (int)fread(&bbuffer[0],1, len,data->inputFile);
    if (ret < len)
    {
        long temp = ftell(data->inputFile);
        fseek(data->inputFile, 44, SEEK_SET);
        data->beatCounter = 0;
        fread(&bbuffer[0]+ret,1, len-ret,data->inputFile);
        NSLog(@"Input file end 2.");
        
    }
    int len2 = tsBufferLen2*2;//*CHANNELS;
    ret = (int)fread(&tbuffer[0],1, len2,data->tanpuraFile);
    if (ret < len2)
    {
        //memset(&tbuffer[0],0,sizeof(byte)*tsBufferLen*4);
        fseek(data->tanpuraFile, 44, SEEK_SET);
        fread(&tbuffer[0]+ret,1, len2-ret,data->tanpuraFile);
       // int ret2 = (int)fread(&tbuffer[0],1, len2,data->tanpuraFile);
        
    }
    // convert byte to float
    for (int i = 0; i < tsBufferLen; i++)
    {
        //for (int k = 0; k < CHANNELS; k++)
        int k=0;
        {
            short shortSample = 0;
            int index = (i * 2+k);//*CHANNELS;
            shortSample = (short)(bbuffer[index] + (((int)bbuffer[index + 1]) << 8));
            //if (shortSample > 0)
            {
                //fbuffer[k][i] = (double)shortSample * ShortMaxInv;
#if TANPURA_BPM_CONTROL
                data->fbuffer[k][i] =  (float)shortSample * ShortMaxInv;
                
#else
                data->fbuffer[k][i] =  (float)shortSample * ShortMaxInv * ui.lehraVolume;
                shortSample = (short)(tbuffer[index] + (((int)tbuffer[index + 1]) << 8));
                data->fbuffer[k][i] +=  (float)shortSample * ShortMaxInv * ui.tanpuraVolume;
               
#endif
                
                //data->fbuffer[k][i] =  sinf(2.0f*PI*440.0f*phase+k*PI);//(float)shortSample * ShortMaxInv;
            }
            /*else
            {
                //fbuffer[k][i] = (double)shortSample * ShortMinInv;
                data->fbuffer[k][i] =  (float)shortSample * ShortMinInv;
               //data->fbuffer[k][i] =  sinf(2.0f*PI*440.0f*phase+k*PI);//(float)shortSample * ShortMaxInv;
            }*/
            //phase += 1.0f/44100.0f;
        }
    }
    
#if TANPURA_BPM_CONTROL
    for (int i = 0; i < tsBufferLen2; i++)
    {
        //for (int k = 0; k < CHANNELS; k++)
        int k=0;
        {
            short shortSample = 0;
            int index = (i * 2+k);//*CHANNELS;
            shortSample = (short)(tbuffer[index] + (((int)tbuffer[index + 1]) << 8));
                data->fbuffer[k+2][i] =  (float)shortSample * ShortMaxInv;
                //data->fbuffer[k][i] =  sinf(2.0f*PI*440.0f*phase+k*PI);//(float)shortSample * ShortMaxInv;

            //phase += 1.0f/44100.0f;
        }
    }
#endif
    

    
    // timestretch
    data->tsComponentL->Process(data->fbuffer[0], tsBufferLen, srcBufferLen, tsCoeff);
#if TANPURA_BPM_CONTROL
    data->tsComponentX->Process(data->fbuffer[2], tsBufferLen2, srcBufferLen2, tanpuraPitch);
#endif
    //data->tsComponentR->Process(data->fbuffer[1], tsBufferLen, srcBufferLen, tsCoeff);
    
    // sampling rate conversion
    data->srcComponentL->Process(data->fbuffer[0], srcBufferLen,bufferLen, pitch);
    //data->srcComponentR->Process(data->fbuffer[1], srcBufferLen,bufferLen, pitch);
#if TANPURA_BPM_CONTROL
    data->srcComponentX->Process(data->fbuffer[2], srcBufferLen2,bufferLen, tanpuraPitch2);
//    data->srcComponentX->Process(data->fbuffer[2], srcBufferLen2,bufferLen, tanpuraPitch);
#endif
    
    /*
    for (int i = 0; i < bufferLen; i++)
    {
        for (int k = 0; k < CHANNELS; k++)
        {
#if TANPURA_BPM_CONTROL
            data->fbuffer[k][i] =  data->fbuffer[k][i] + data->fbuffer[k+2][i];
#else
            data->fbuffer[k][i] =  data->fbuffer[k][i];
            
#endif
        }
    }*/
    
    data->hsfL->Process(data->fbuffer[0], bufferLen, 6000.0f, 15.0f);
    data->hsfX->Process(data->fbuffer[2], bufferLen, 6000.0f, 15.0f);
    
    // float to short
    float level = 0.0f;
    float tempx = 0.0f;
    
    for(int i=0; i < bufferLen; i++)
    {
#if TANPURA_BPM_CONTROL
        float sample =(data->fbuffer[0][i]*ui.lehraVolume+data->fbuffer[2][i]*ui.tanpuraVolume)* GAIN* data->fadeGain;
        buffer[i*CHANNELS ] = (signed short)(sample);
        if(data->fadeOut)
        {
            data->fadeGain -= 0.0001f;
            if (data->fadeGain < 0.0f)
            {
                data->fadeGain = 0.0f;
                data->fadeOut = false;
                data->isPlayingInternal = false;
            }
        } else if (data->fadeIn)
        {
            data->fadeGain += 0.0001f;
            if (data->fadeGain > 1.0f)
            {
                data->fadeGain = 1.0f;
                data->fadeIn = false;
            }
            
        }
#else
        float sample =(data->fbuffer[0][i])* GAIN * data->fadeGain;
        if(data->fadeOut)
        {
            data->fadeGain -= 0.001f;
            if (data->fadeGain < 0.0f)
            {
                data->fadeGain = 0.0f;
                data->fadeOut = false;
                data->isPlayingInternal = false;
            }
        } else if (data->fadeIn)
        {
            data->fadeGain += 0.001f;
            if (data->fadeGain > 1.0f)
            {
                data->fadeGain = 1.0f;
                data->fadeIn = false;
            }
            
        }
        buffer[i*CHANNELS ] = (signed short)(sample);
#endif
        tempx= sample*sample;//data->fbuffer[0][i]*data->fbuffer[0][i];
        level += tempx;
        
        /*if (sample > 32768.0f || sample < -32768.0f)
        {
            memcpy(&temp[0], buffer,2048*sizeof(short));
            float temp2[4096];
            memcpy(&temp2[0],data->fbuffer[0],sizeof(float)*bufferLen*CHANNELS);
            std::cout << "OVERFLOW: " << sample / 32768.0f << "\n";
        }*/
        
        for (int k = 1; k < CHANNELS; k++)
        {
            buffer[i*CHANNELS+k] = sample;
           
        }
    //buffer[i*Channels + k] = (signed short)(doubleBuffer[k][i] * 32768.0);
    }
    level = sqrt(level) / (float)(bufferLen*CHANNELS);
    data->audioLevel = level * (1.0f-AUDIO_LEVEL_SMOOTHER) + data->audioLevelMem * AUDIO_LEVEL_SMOOTHER;
    data->audioLevelMem = data->audioLevel;
    //memcpy(&temp[0], buffer,bufferLen*sizeof(short));
    
    data->beatCounter += (float)bufferLen * BEATS_PER_BYTE * bpm;
    
    int max_beats[] = {16,7,10,12};

    /*if (data->currentInstrument==0)
    {
        while (data->beatCounter > 12)
        {
            data->beatCounter -= 12.0f;
        }
        
    } else {*/
    while (data->beatCounter > max_beats[data->currentTaal])
    {
        data->beatCounter -= (float) max_beats[data->currentTaal];
    }
    //}
    
    /*
    if (data->currentInstrument == 0 && data->currentTaal == 1) // if sarangi roopak taal
    {
        while (data->beatCounter > 7)
        {
            data->beatCounter -= 7.0f;
        }
        
    } else if (data->currentInstrument == 1 && data->currentTaal == 3 && data->beatCounter > 12) {
        data->beatCounter -= 12.0f;
    }else if (data->beatCounter > 16)
    {
        data->beatCounter -= 16.0f;
    }*/
#endif
    return noErr;

}

static OSStatus recordingCallback(void *inRefCon,
                                  AudioUnitRenderActionFlags *ioActionFlags,
                                  const AudioTimeStamp *inTimeStamp,
                                  UInt32 inBusNumber,
                                  UInt32 inNumberFrames,
                                  AudioBufferList *ioData) {
    return noErr;

}

void MyAudioSessionInterruptionCallback (void    *icata,UInt32  iState)
{
    if (iState == kAudioSessionBeginInterruption) {
        AudioOutputUnitStop (audioUnit);
    } else if (iState == kAudioSessionEndInterruption) {
        AudioSessionSetActive (true);
        OSStatus status = AudioOutputUnitStart(audioUnit);
        checkError(status);
    }
}
void checkError(OSStatus result)
{
    int k = 0;
    switch (result)
    {
        case kAudioUnitErr_InvalidProperty:
            k = 1;
            break;
        case kAudioUnitErr_InvalidParameter:
            k = 2;
            break;
        case kAudioUnitErr_InvalidElement:
            k = 3;
            break;
        case kAudioUnitErr_NoConnection:
            k = 4;
            break;
        case kAudioUnitErr_FailedInitialization:
            k = 5;
            break;
        case kAudioUnitErr_TooManyFramesToProcess:
            k = 6;
            break;
        case kAudioUnitErr_InvalidFile:
            k = 7;
            break;
        case kAudioUnitErr_FormatNotSupported:
            k = 8;
            break;
        case kAudioUnitErr_Uninitialized:
            k = 9;
            break;
        case kAudioUnitErr_InvalidScope:
            k = 10;
            break;
        case kAudioUnitErr_PropertyNotWritable:
            k = 11;
            break;
        case kAudioUnitErr_CannotDoInCurrentContext:
            k = 12;
            break;
        case kAudioUnitErr_InvalidPropertyValue:
            k = 13;
            break;
        case kAudioUnitErr_PropertyNotInUse:
            k = 14;
            break;
        case kAudioUnitErr_Initialized:
            k = 15;
            break;
        case kAudioUnitErr_InvalidOfflineRender:
            k = 16;
            break;
        case kAudioUnitErr_Unauthorized:
            k = 17;
            break;
        case kAudioSessionNotActiveError:
            k = 18;
            break;
        case kAudioSessionNotInitialized:
            k = 19;
            break;
        case kAudioSessionAlreadyInitialized:
            k = 20;
            break;
        case kAudioSessionInitializationError:
            k = 21;
            break;
        case kAudioSessionUnsupportedPropertyError:
            k = 22;
            break;
        case kAudioSessionBadPropertySizeError:
            k = 23;
            break;
        case kAudioServicesNoHardwareError:
            k = 24;
            break;
        case kAudioSessionNoCategorySet:
            k = 25;
            break;
        case kAudioSessionIncompatibleCategory:
            k = 26;
            break;
        case kAudioSessionUnspecifiedError:
            k = 27;
            break;
        default:
            k = 0;
            break;
    }
    if (result != noErr)
    {
        //int i=0;
        NSLog(@"ERROR: %i", k);
    }
    
    
}

-(bool) Initialize //: (ViewController*) _parentx;
{
   // self.parent = _parentx;
   // data->parentInstance = self.parent;
    data->tsComponentL = new TimeStretcherPL();
    data->tsComponentR = new TimeStretcherPL();
    data->srcComponentL = new SamplingRateConverter();
    data->srcComponentR = new SamplingRateConverter();
    data->hsfL = new HighShelvingFilter();
    data->hsfL->CalculateNewCoeffs(6000.0f, 15.0f);
    data->hsfX = new HighShelvingFilter();
    data->hsfX->CalculateNewCoeffs(6000.0f, 15.0f);
    data->fbuffer = new float*[4];
    data->fbuffer[0] = new float[20000];
    data->fbuffer[1] = new float[20000];
    data->fbuffer[2] = new float[20000];
    data->tsComponentX = new TimeStretcherPL();
    data->srcComponentX = new SamplingRateConverter();
    data->fbuffer[3] = new float[20000];
    data->audioLevelMem = 0.0f;
    data->audioLevel = 0.0f;
    data->tsComponentL->Initialize();
    data->tsComponentR->Initialize();
    data->tsComponentX->Initialize();
    
#ifdef USE_OGG_DECODING

    NSString* bundleID = [[NSBundle mainBundle] bundleIdentifier];
    NSFileManager*fm = [NSFileManager defaultManager];
    NSURL*    dirPath = nil;
    
    // Find the application support directory in the home directory.
    NSArray* appSupportDir = [fm URLsForDirectory:NSApplicationSupportDirectory
                                        inDomains:NSUserDomainMask];
    if ([appSupportDir count] > 0)
    {
        // Append the bundle ID to the URL for the
        // Application Support directory
        dirPath = [[appSupportDir objectAtIndex:0] URLByAppendingPathComponent:bundleID];
        
        // If the directory does not exist, this method creates it.
        // This method is only available in OS X v10.7 and iOS 5.0 or later.
        NSError*    theError = nil;
        if (![fm createDirectoryAtURL:dirPath withIntermediateDirectories:YES
                           attributes:nil error:&theError])
        {
            // Handle the error.
        }
    }
    
    // Check if we have already decoded media files
    return [fm fileExistsAtPath:[[dirPath path] stringByAppendingPathComponent:@"Sarangi_Roopak Taal_Gara_90.wav"]];
#else
    return true;
#endif
    
    
}

-(void) DecodeFiles
{
#ifdef USE_OGG_DECODING

    NSString* bundleID = [[NSBundle mainBundle] bundleIdentifier];
    NSFileManager*fm = [NSFileManager defaultManager];
    NSURL*    dirPath = nil;
    
    // Find the application support directory in the home directory.
    NSArray* appSupportDir = [fm URLsForDirectory:NSApplicationSupportDirectory
                                        inDomains:NSUserDomainMask];
    dirPath = [[appSupportDir objectAtIndex:0] URLByAppendingPathComponent:bundleID];

    NSString* destDir = [dirPath path];
    
    OggVorbisDecoder* decoder = new OggVorbisDecoder();
    NSBundle *b = [NSBundle mainBundle];
    NSString *dir = [b bundlePath];
#if TANPURA_BPM_CONTROL
    NSString* filename = [dir stringByAppendingPathComponent:@"tanpura_06_01.wav"];
    //NSString* filename = [dir stringByAppendingPathComponent:@"tanpura_06.wav"];
    
#else
    NSString* filename = [dir stringByAppendingPathComponent:@"tanpura_04.wav"];
#endif
    data->tanpuraFile = fopen([filename UTF8String],"rb");
    fseek(data->tanpuraFile, 44, SEEK_CUR);
    NSArray *instrumentA = [NSArray arrayWithObjects:@"Santoor",@"Sarangi", nil];
    
    double buffer[4096];
    byte byteBuffer[9192];
    
    for (int instrument=0; instrument < 2; instrument++)
    {
        NSArray *taalA = nil;
        NSArray *raagA = nil;
        NSArray *bpmA = nil;
    if (instrument == 0)
    {
   taalA = [NSArray arrayWithObjects:@"Teen Taal",@"Roopak Taal", @"Jhap Taal", @"Ek Taal", nil];
    raagA = [NSArray arrayWithObjects:@"Kiwani", @"Des", @"Bhairav", nil];
    bpmA = [NSArray arrayWithObjects:@"40", @"60", @"80", @"100",nil];
    } else {
        taalA = [NSArray arrayWithObjects:@"Teen Taal", @"Roopak Taal", nil];
        raagA = [NSArray arrayWithObjects:@"Charukeshi",@"Des",@"Vachaspati", nil];
        bpmA = [NSArray arrayWithObjects:@"40", @"50", @"60", @"75",@"90",nil];
        
    }
    for (int taal=0; taal < taalA.count; taal++)
    {
        if (instrument == 1 && taal == 1)
        {
            raagA = [NSArray arrayWithObjects:@"Bairagi", @"Bhageshree", @"Gara", nil];
            
        }
        for (int raag = 0; raag < raagA.count; raag++)
        {
            for (int bpm = 0; bpm < bpmA.count; bpm++)
            {
                FILE* fin = fopen([[dir stringByAppendingPathComponent: [NSString stringWithFormat:@"%@_%@_%@_%@.ogg",[instrumentA objectAtIndex:instrument],[taalA objectAtIndex:taal],[raagA objectAtIndex:raag],[bpmA objectAtIndex:bpm]]] UTF8String],"rb");
                NSString *foutName =[destDir stringByAppendingPathComponent: [NSString stringWithFormat:@"%@_%@_%@_%@.wav",[instrumentA objectAtIndex:instrument],[taalA objectAtIndex:taal],[raagA objectAtIndex:raag],[bpmA objectAtIndex:bpm]]];
                FILE* fout = fopen([foutName UTF8String],"wb");
                
                memset(&byteBuffer[0],0,sizeof(byte)*9192);
                fwrite(byteBuffer,1,44,fout); // compensate for wav header
                
                decoder->Initialize(fin, 44100);
                while(!decoder->IsEof())
                {
                    int samples = decoder->Decode(buffer, 4096, fin);
                    for (int k=0; k < samples; k++)
                    {
                        float fSample = (float)buffer[k];//buffer[0][k];
                        short sSample = (short)(fSample >= 0.0 ? fSample * 	32767 : fSample * 	-32767 * -1);
                        int index =  k * 2;
                            byteBuffer[index] = (byte)sSample;
                            byteBuffer[index + 1] = (byte)(sSample >> 8);
                        
                    }
                    fwrite(byteBuffer, 1,samples*2,fout);

                }
                fclose(fin);
                fclose(fout);
                [AudioProcessor addSkipBackupAttributeToItemAtURL:foutName];
            }
        }
    }
    }
#endif
}

+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSString *)path
{
    //assert([[NSFileManager defaultManager] fileExistsAtPath: path]);
    if ([[NSFileManager defaultManager] fileExistsAtPath: path]){
        NSError *error = nil;
        BOOL success = [[NSURL fileURLWithPath:path] setResourceValue: [NSNumber numberWithBool: YES]
                                                               forKey: NSURLIsExcludedFromBackupKey error: &error];
        if(!success){
            NSLog(@"Error excluding %@ from backup %@", path, error);
        }
        
        return success;
    }
    return false;
}

-(void) LoadFiles
{
    NSBundle *b = [NSBundle mainBundle];
    NSString *dir = [b bundlePath];
#if TANPURA_BPM_CONTROL
    NSString* filename = [dir stringByAppendingPathComponent:@"tanpura_06_02.wav"];
    //NSString* filename = [dir stringByAppendingPathComponent:@"tanpura_06.wav"];
    
#else
    NSString* filename = [dir stringByAppendingPathComponent:@"tanpura_04.wav"];
#endif
    data->tanpuraFile = fopen([filename UTF8String],"rb");
    fseek(data->tanpuraFile, 44, SEEK_CUR);
    
#ifdef USE_OGG_DECODING

    NSString* bundleID = [[NSBundle mainBundle] bundleIdentifier];
    NSFileManager*fm = [NSFileManager defaultManager];
    NSURL*    dirPath = nil;
    
    // Find the application support directory in the home directory.
    NSArray* appSupportDir = [fm URLsForDirectory:NSApplicationSupportDirectory
                                        inDomains:NSUserDomainMask];
    dirPath = [[appSupportDir objectAtIndex:0] URLByAppendingPathComponent:bundleID];
    
    NSString* destDir = [dirPath path];
#else
    NSString* destDir = dir;
#endif
    
    // Load all files
    AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSArray* instruments = appDelegate.lehraData.allKeys;
    
    for (int i=0; i < instruments.count; i++)
    {
        NSDictionary* instrumentData = [appDelegate.lehraData objectForKey:[instruments objectAtIndex:i]];
        NSDictionary* tData = [instrumentData objectForKey:@"Taals"];
        NSArray* taals = tData.allKeys;
        for (int t=0; t < taals.count; t++)
        {
            NSDictionary* taalData = [tData objectForKey:[taals objectAtIndex:t]];
            NSDictionary* rData = [taalData objectForKey:@"Raags"];
            NSArray* bpms = [taalData objectForKey:@"Tempos"];
            NSArray* raags = rData.allKeys;
            NSString* beats = [taalData objectForKey:@"Beats"];
            NSString* cycles = [taalData objectForKey:@"Cycles"];
            int icycles = 1;
            if (cycles != nil)
                icycles = (int)[cycles integerValue];


            for (int r=0; r < raags.count; r++)
            {
                NSDictionary* raagData = [rData objectForKey:[raags objectAtIndex:r]];
                NSArray* rbpms = [raagData objectForKey:@"Tempos"];

                if (rbpms == nil ){
                    rbpms = bpms;
                }

                NSString* fileNameFormat = [raagData objectForKey:@"FileName"];
                [ self loadRaag:dir :fileNameFormat :rbpms:i:t:r:(int)[beats integerValue]:icycles];
            }
            
        }
        
        
    }
    
    
    // test that all files are available
    /*for (int i=0; i < instruments.count; i++)
    {
        NSDictionary* instrumentData = [appDelegate.lehraData objectForKey:[instruments objectAtIndex:i]];
        NSDictionary* tData = [instrumentData objectForKey:@"Taals"];
        NSArray* taals = tData.allKeys;
        for (int t=0; t < taals.count; t++)
        {
            NSDictionary* taalData = [tData objectForKey:[taals objectAtIndex:t]];
            NSDictionary* rData = [taalData objectForKey:@"Raags"];
            NSArray* bpms = [taalData objectForKey:@"Tempos"];
            NSArray* raags = rData.allKeys;
            NSString* beats = [taalData objectForKey:@"Beats"];
            
            for (int r=0; r < raags.count; r++)
            {
                NSDictionary* raagData = [rData objectForKey:[raags objectAtIndex:r]];
                NSArray* rbpms = [raagData objectForKey:@"Tempos"];
                
                if (rbpms == nil ){
                    rbpms = bpms;
                }
                
                NSString* fileNameFormat = [raagData objectForKey:@"FileName"];
                //[ self loadRaag:dir :fileNameFormat :rbpms:i:t:r:(int)[beats integerValue]];
                for (int b=0; b < rbpms.count; b++)
                {
                    LehraFile* temp = processor->getLehraFile(i , t , r , b);
                    if (temp == nil)
                    {
                        int ii=0;
                        ii=1;
                    }
                }
            }
            
        }
     
        
    }*/

    
    // Load Santoor files
    /*
    NSArray *taalA = [NSArray arrayWithObjects:@"Teen Taal",@"Roopak Taal", @"Jhap Taal", @"Ek Taal", nil];
    NSArray *raagA = [NSArray arrayWithObjects:@"Kiwani", @"Des", @"Bhairav", nil];
    NSArray *bpmA = [NSArray arrayWithObjects:@"40", @"60", @"80", @"100",@"120",nil];
    

    
    for (int raag = 0; raag < 3; raag++)
    {
        for (int bpm = 0; bpm < 4; bpm++)
        {
            [self openFile:destDir :[NSString stringWithFormat:@"Santoor_%@_%@_%@.wav",[taalA objectAtIndex:0],[raagA objectAtIndex:raag],[bpmA objectAtIndex:bpm]] :processor->getFileIndex(1, 0, raag, bpm)];
            
        }
    }

    
    for (int raag = 0; raag < 3; raag++)
    {
        for (int bpm = 0; bpm < 4; bpm++)
        {
            [self openFile:destDir :[NSString stringWithFormat:@"Santoor_%@_%@_%@.wav",[taalA objectAtIndex:2],[raagA objectAtIndex:raag],[bpmA objectAtIndex:bpm]] :processor->getFileIndex(1, 2, raag, bpm)];
            
        }
    }
    
    for (int raag = 0; raag < 3; raag++)
    {
        for (int bpm = 2; bpm < 5; bpm++)
        {
            [self openFile:destDir :[NSString stringWithFormat:@"Santoor_%@_%@_%@.wav",[taalA objectAtIndex:1],[raagA objectAtIndex:raag],[bpmA objectAtIndex:bpm]] :processor->getFileIndex(1, 1, raag, bpm)];
            
        }
    }
    
    for (int raag = 0; raag < 3; raag++)
    {
        for (int bpm = 2; bpm < 5; bpm++)
        {
            [self openFile:destDir :[NSString stringWithFormat:@"Santoor_%@_%@_%@.wav",[taalA objectAtIndex:3],[raagA objectAtIndex:raag],[bpmA objectAtIndex:bpm]] :processor->getFileIndex(1, 3, raag, bpm)];
            
        }
    }
    
    // Load Sarangi files
    
    // Teen taal
    NSArray *taalA2 = [NSArray arrayWithObjects:@"Teen Taal", @"Roopak Taal", @"Jhap Taal", @"Ek Taal", nil];
    NSArray *raagA2 = [NSArray arrayWithObjects:@"Charukeshi",@"Des",@"Vachaspati",@"Puriya Dhana", @"Shivranjani", @"Janasammohini", nil];
    NSArray *bpmA2 = [NSArray arrayWithObjects:@"40", @"50", @"60", @"75",@"90",@"120",@"150", @"180",@"240",@"320",nil];
    
    
    for (int taal=0; taal < 1; taal++)
    {
        for (int raag = 0; raag < 6; raag++)
        {
            for (int bpm = 0; bpm < 8; bpm++)
            {
                [self openFile:dir :[NSString stringWithFormat:@"Sarangi_%@_%@_%@.wav",[taalA2 objectAtIndex:taal],[raagA2 objectAtIndex:raag],[bpmA2 objectAtIndex:bpm]] :processor->getFileIndex(0, taal, raag, bpm)];
                
            }
        }
    }
    
    // Ek & Jhap taal
   // NSArray *taalA2 = [NSArray arrayWithObjects:@"Teen Taal", @"Roopak Taal", @"Jhap Taal", @"Ek Taal", nil];
    NSArray *raagA4 = [NSArray arrayWithObjects:@"Malkauns",@"Hemant", nil];
   // NSArray *bpmA2 = [NSArray arrayWithObjects:@"40", @"50", @"60", @"75",@"90",nil];
    
    for (int taal=2; taal < 4; taal++)
    {
        //for (int raag = 0; raag < 3; raag++)
        {
            for (int bpm = 0; bpm < 5; bpm++)
            {
                [self openFile:dir :[NSString stringWithFormat:@"Sarangi_%@_%@_%@.wav",[taalA2 objectAtIndex:taal],[raagA4 objectAtIndex:taal-2],[bpmA2 objectAtIndex:bpm]] :processor->getFileIndex(0, taal, 0, bpm)];
                
            }
        }
    }
    
    // Roopak taal
   // NSArray *taalA2 = [NSArray arrayWithObjects:@"Teen Taal", @"Roopak Taal", @"Jhap Taal", @"Ek Taal", nil];
    NSArray *raagA3 = [NSArray arrayWithObjects:@"Bairagi", @"Bhageshree", @"Gara", nil];
    
    for (int taal=1; taal < 2; taal++)
    {
        for (int raag = 0; raag < 3; raag++)
        {
            for (int bpm = 5; bpm < 8; bpm++)
            {
                [self openFile:dir :[NSString stringWithFormat:@"Sarangi_%@_%@_%@.wav",[taalA2 objectAtIndex:taal],[raagA3 objectAtIndex:raag],[bpmA2 objectAtIndex:bpm]] :processor->getFileIndex(0, taal, raag, bpm)];
                
            }
        }
    }
    
    [ self loadFileGroup:dir :@"Sitar_%@_%@_%@.wav" :@[@"Teen Taal"] :@[@"Gorakh Kalyan",@"Bibhas"] :@[@"40", @"50", @"60", @"75",@"90",@"120",@"150", @"180",@"240",@"320"] :2:0];
    [ self loadFileGroup:dir :@"Sitar_%@_%@_%@.wav" :@[@"Ek Taal"] :@[@"Bageshree",@"Ahiri"] :@[ @"60", @"75",@"90",@"120",@"150", @"180"] :2:1];
    [ self loadFileGroup:dir :@"Sitar_%@_%@_%@.wav" :@[@"Jhap Taal"] :@[@"Hansa Dhwani",@"Desh"] :@[ @"40", @"50",@"60", @"75",@"90",@"120"] :2:2];

    [ self loadFileGroup:dir :@"Harmonium_%@_%@_%@.wav" :@[@"Teen Taal"] :@[@"Jog Kouns",@"Ahir Bhairav",@"Tilang"] :@[ @"40", @"50",@"60", @"75",@"90",@"120",@"150", @"180",@"240",@"320"] :3:0];
    [ self loadFileGroup:dir :@"Harmonium_%@_%@_%@.wav" :@[@"Ek Taal"] :@[@"Dhani",@"Sohini"] :@[ @"60", @"75",@"90",@"120",@"150", @"180"] :3:1];
    [ self loadFileGroup:dir :@"Harmonium_%@_%@_%@.wav" :@[@"Jhap Taal"] :@[@"Kirwani",@"Madhu Banti"] :@[ @"40",@"50",@"60", @"75",@"90",@"120"] :3:2];
    [ self loadFileGroup:dir :@"Harmonium_%@_%@_%@.wav" :@[@"Pancham Sawari"] :@[@"Yaman"] :@[ @"50",@"60", @"75",@"90",@"120",@"150"] :3:3];
    [ self loadFileGroup:dir :@"Harmonium_%@_%@_%@.wav" :@[@"Dadra"] :@[@"Pilu"] :@[ @"90",@"120",@"120",@"150", @"180",@"240"] :3:4];
    [ self loadFileGroup:dir :@"Harmonium_%@_%@_%@.wav" :@[@"Keharwa"] :@[@"Khamaj"] :@[ @"90",@"120",@"120",@"150", @"180",@"240",@"320"] :3:5];
    [ self loadFileGroup:dir :@"Harmonium_%@_%@_%@.wav" :@[@"Char Taal Ki Sawari"] :@[@"Kalavati"] :@[  @"75",@"90",@"120",@"150", @"180"] :3:6];
    */
    
    /*[self openFile:dir :@"santoor_teen taal_bhairav_40.wav" :getFileIndex(2, 1, 3, 1)];
    [self openFile:dir :@"santoor_teen taal_bhairav_60.wav" :getFileIndex(2, 1, 3, 2) ];
    [self openFile:dir :@"santoor_teen taal_bhairav_80.wav" :getFileIndex(2, 1, 3, 3)];
    [self openFile:dir :@"santoor_teen taal_bhairav_100.wav" :getFileIndex(2, 1, 3, 4) ];*/
    data->currentBpm = 0;
    data->currentFileIndex = processor->getFileIndex(0, 0, 0, 0);
    data->currentInstrument = 0;
    data->currentRaag = 0;
    data->currentTaal = 0;
    data->inputFile =data->allFiles[data->currentFileIndex];
    NSLog(@"CURRENT FILE:%d",data->currentFileIndex);

    int i= 0;
    i = 1;
    data->beatCounter = 0;
    data->isPlayingInternal = false;
    data->fadeIn = false;
    data->fadeOut = false;
    data->fadeGain = 0.0f;
    data->ui_bmpValue = 90;
    data->ui_pitchCoeff = 1.0f;
    //processor->initialize();

}


-(void) loadRaag : (NSString*) dir : (NSString*) fileString : (NSArray*) bpms : (int) instrument :(int) taal :(int) raag :(int) beats :(int) cycles
{
    int* ibpms = new int[bpms.count];
            for (int bpm = 0; bpm < bpms.count; bpm++)
            {
                ibpms[bpm] = (int)[[bpms objectAtIndex:bpm] integerValue];
                //[self openFile:dir :[NSString stringWithFormat:fileString,[bpms objectAtIndex:bpm]] :processor->getFileIndex(instrument, taal, raag, bpm)];
                
            }
    NSString* destFileName = [fileString substringToIndex:fileString.length-7];

    processor->addLehraPath([[[dir stringByAppendingPathComponent:destFileName] stringByAppendingString:@".wav"] UTF8String], instrument,  taal,  raag, ibpms, (int)bpms.count,  beats,cycles);

}

-(void) loadFileGroup : (NSString*) dir : (NSString*) fileString : (NSArray*) taals : (NSArray*) raags : (NSArray*) bpms : (int) instrument :(int) startTaal
{
    for (int taal=0; taal < taals.count; taal++)
    {
        for (int raag = 0; raag < raags.count; raag++)
        {
            for (int bpm = 0; bpm < bpms.count; bpm++)
            {
                [self openFile:dir :[NSString stringWithFormat:fileString,[taals objectAtIndex:taal],[raags objectAtIndex:raag],[bpms objectAtIndex:bpm]] :processor->getFileIndex(instrument, startTaal+taal, raag, bpm)];
                
            }
        }
    }
}
static int counter = 0;

-(void) openFile : (NSString*) dir : (NSString*) filename : (int) index
{
    FILE* f = fopen([[dir stringByAppendingPathComponent:filename] UTF8String],"rb");
    fseek(f, 44, SEEK_CUR);
    //data->allFiles[index] = f;
    NSLog(@"FILE:%d %@ %d",index,filename,++counter);
}



#ifdef USE_TAAE
-(bool) startAudioEngine : (NSError*) error
{
    BOOL result = [_audioController start:&error];
    return result;
}

-(void) stopAudioEngine
{
    [self.audioController stop];
    if(self.levelAnalyzer)
       [self.levelAnalyzer reset];
}

-(void) setLehraVolume :(float) volume
{
    if(self.lehraChannel) {
        self.lehraChannel.volume = volume;
    }
    
}

-(void) setTanpuraVolume :(float) volume
{
    if(self.tanpuraChannel) {
        self.tanpuraChannel.volume = volume;
    }
    
}


-(void) setMetronomeVolume :(float) volume
{
    if(self.lehraMetronome) {
        self.lehraMetronome.volume = volume;
    }
    
}

-(void) updateLehraData
{
    float pitch=0;
    float tsCoeff=0;
    ViewController* ui =((ViewController*)dataWrapper->parentInstance);
    data->ui_bmpValue = ui.bmpValue;
    data->ui_isPlaying = ui.isPlaying;
    data->ui_pitchCoeff = ui.pitchCoeff;
    data->ui_lowerCatIndex = ui.lowerCatIndex;
    data->ui_middleCatIndex = ui.middleCatIndex;
    data->ui_upperCatIndex = ui.upperCatIndex;
    data->ui_tanpuraVolume = ui.tanpuraVolume;
    data->ui_lehraVolume = ui.lehraVolume;
    processor->calculateCoeffs(data, &pitch, &tsCoeff);
    bool fileUpdated = processor->updateLehraFile(data);
    if (fileUpdated)
    {
        [self.lehraChannel setParam:data->inputFile :data->beatsPerFile[data->currentInstrument*MAX_TAAL+data->currentTaal]];
    }
    [self.lehraTimeStretcher setParam:pitch :tsCoeff ];
    [self.lehraMetronome setParam:data->bpmOrig :(float)data->ui_bmpValue ];
    float tanpuraPitch = data->ui_pitchCoeff * 1.18919841518334f;
    float tanpuraPitch2 = 2.0f*tanpuraPitch; // sampling rate 22.05kHz
    [self.tanpuraTimeStretcher setParam:tanpuraPitch2 :tanpuraPitch];
    [self.lehraMetronome setCycleLength:data->beatsPerFile[data->currentInstrument*MAX_TAAL+data->currentTaal]];
}


-(void) getMetrics : (int*) beat : (float*) volume
{
    long position = [self.lehraChannel getPosition];
    
    float beats = floor((float)position*data->bpmOrig/ (88200.0f*60.0f));
    //NSLog(@"GET metrics beats:%f position:%ld",beats,position);
    if (beats >= data->beatsPerFile[data->currentInstrument*MAX_TAAL+data->currentTaal])
        beats -= data->beatsPerFile[data->currentInstrument*MAX_TAAL+data->currentTaal];
    if (beats >= data->beatsPerFile[data->currentInstrument*MAX_TAAL+data->currentTaal])
        beats -= data->beatsPerFile[data->currentInstrument*MAX_TAAL+data->currentTaal];
    if (beats >= data->beatsPerFile[data->currentInstrument*MAX_TAAL+data->currentTaal])
        beats -= data->beatsPerFile[data->currentInstrument*MAX_TAAL+data->currentTaal];
    
    *beat = (int)beats;//[self.lehraChannel getBeats];
    *volume = [self.levelAnalyzer getLevel]*5.0f;
}

#endif

-(void) StartAudio
{
    
    
#ifdef USE_TAAE
    AudioStreamBasicDescription audioDescription;
    memset(&audioDescription, 0, sizeof(audioDescription));
    audioDescription.mFormatID          = kAudioFormatLinearPCM;
    audioDescription.mFormatFlags       = kAudioFormatFlagIsSignedInteger | kAudioFormatFlagIsPacked | kAudioFormatFlagsNativeEndian | kAudioFormatFlagIsNonInterleaved;
    audioDescription.mChannelsPerFrame  = 1;
    audioDescription.mBytesPerPacket    = sizeof(SInt16);
    audioDescription.mFramesPerPacket   = 1;
    audioDescription.mBytesPerFrame     = sizeof(SInt16);
    audioDescription.mBitsPerChannel    = 8 * sizeof(SInt16);
    audioDescription.mSampleRate        = 44100.0;
    self.audioController = [[AEAudioController alloc]
                            initWithAudioDescription:audioDescription
                            inputEnabled:false];
    self.audioController.preferredBufferDuration = 0.046;//.0025f;
    
    self.lehraChannel = [[AELehraChannel alloc] initWithAudioController:self.audioController];
    self.lehraTimeStretcher = [[AETimeStretcher alloc] initWithAudioController:self.audioController];
    self.tanpuraTimeStretcher = [[AETimeStretcher alloc] initWithAudioController:self.audioController];
    self.hpf = [[AEHPFilter alloc] initWithAudioController:self.audioController];
    self.levelAnalyzer = [[AELevelAnalyzer alloc] initWithAudioController:self.audioController];
    self.lehraMetronome = [[AELehraMetronome alloc] initWithAudioController:self.audioController lehraChannel:self.lehraChannel];
    [self.lehraChannel setParam:data->inputFile :data->beatsPerFile[data->currentInstrument*MAX_TAAL+data->currentTaal]];
    NSError *error;
    
    AEChannelGroupRef maingroup = [self.audioController createChannelGroup];
    AEChannelGroupRef group = [self.audioController createChannelGroupWithinChannelGroup:maingroup];

  //BOOL result = [_audioController start:&error];
    [self.audioController addChannels:@[self.lehraChannel] toChannelGroup:group];
    
    NSBundle *b = [NSBundle mainBundle];
    NSString *dir = [b bundlePath];
    NSString* filename = [dir stringByAppendingPathComponent:@"tanpura_06_02.wav"];
    NSURL *fileURL = [NSURL URLWithString:filename];
   // NSError *error = nil;
    
    


    
    //AEAudioFilePlayer *player;
    self.tanpuraChannel = [AEAudioFilePlayer audioFilePlayerWithURL:fileURL audioController:self.audioController error:&error ];//audioUnitFilePlayerWithController:self.audioController error:&error];
    if (self.tanpuraChannel) {
        [self.audioController addChannels:@[self.tanpuraChannel] toChannelGroup:group];
        //[player setUrl:fileURL];
        self.tanpuraChannel.loop = YES;
        [self.tanpuraChannel setCurrentTime:0];
        
    }
    _isPlaying = true;
    self.lehraChannel.channelIsPlaying = true;
    
    
    [self updateLehraData];
    [self.audioController addFilter:self.lehraTimeStretcher toChannel:self.lehraChannel];
    [self.audioController addFilter:self.tanpuraTimeStretcher toChannel:self.tanpuraChannel];
    [self.audioController addFilter:self.hpf toChannelGroup:group];
    [self.audioController addFilter:self.levelAnalyzer toChannelGroup:group];
    [self.audioController addChannels:@[self.lehraMetronome] toChannelGroup:maingroup];
    self.lehraMetronome.channelIsPlaying = true;
    self.lehraMetronome.volume = 1.0f;
    //[self.lehraChannel setPlaying:true];
    //[player setPlaying:true];
   // [self.lehraChannel setCurrentTime:0];
#else
    
    OSStatus status;
    int SampleRate = 44100;
    //int Channels = 2;
    int BufferLength = 512*4;
    NSError *err = nil;
    UInt32 flag = 1;
    
#define USE_RECORDING 0
#define USE_AVAUDIOSESSION 1
    

    
    AudioComponentDescription desc;
    desc.componentType = kAudioUnitType_Output;
    desc.componentSubType = kAudioUnitSubType_RemoteIO;
    desc.componentFlags = 0;
    desc.componentFlagsMask = 0;
    desc.componentManufacturer = kAudioUnitManufacturer_Apple;
    Float32 BufferLengthF = (Float32)BufferLength / (Float32)SampleRate;

    
#ifdef USE_AVAUDIOSESSION
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayback withOptions:AVAudioSessionCategoryOptionAllowBluetooth error:&err];
    [session setPreferredIOBufferDuration:BufferLengthF error:&err];
    float audioBufferSize = session.preferredIOBufferDuration;
    [session setActive:YES error:&err];
#else
    status = AudioSessionInitialize(NULL, NULL, MyAudioSessionInterruptionCallback, NULL);

#ifdef USE_RECORDING
    UInt32 audioCategory = kAudioSessionCategory_PlayAndRecord;//kAudioSessionCategory_PlayAndRecord;//
#else
    UInt32 audioCategory = kAudioSessionCategory_MediaPlayback;//kAudioSessionCategory_PlayAndRecord;//kAudioSessionCategory_MediaPlayback;//kAudioSessionCategory_PlayAndRecord;
#endif
    status = AudioSessionSetProperty(kAudioSessionProperty_AudioCategory, sizeof(audioCategory), &audioCategory);
    checkError(status);
    
    
    status = AudioSessionSetProperty(kAudioSessionProperty_PreferredHardwareIOBufferDuration,
                                     sizeof(Float32), &BufferLengthF);
    checkError(status);
    /*
    UInt32 newRoute = kAudioSessionOverrideAudioRoute_Speaker;
    status = AudioSessionSetProperty(kAudioSessionProperty_OverrideAudioRoute, sizeof(newRoute), &newRoute);
    checkError(status);*/
    
    UInt32 doChangeDefaultRoute = 1;
    AudioSessionSetProperty (kAudioSessionProperty_OverrideCategoryDefaultToSpeaker, sizeof (doChangeDefaultRoute), &doChangeDefaultRoute);
    
    
    
    status = AudioSessionSetActive(true);
    
    Float32 audioBufferSize;
    UInt32 size = sizeof (audioBufferSize);
    status = AudioSessionGetProperty(kAudioSessionProperty_CurrentHardwareIOBufferDuration, &size, &audioBufferSize);
    checkError(status);
#endif
    
    data->bufferSize = (int)round(audioBufferSize*44100.0f);
    
    AudioComponent inputComponent = AudioComponentFindNext(NULL, &desc);
    
    
    status = AudioComponentInstanceNew(inputComponent, &audioUnit);
    checkError(status);
    
    
    status = AudioUnitSetProperty(audioUnit,
                                  kAudioOutputUnitProperty_EnableIO,
                                  kAudioUnitScope_Output,
                                  kOutputBus,
                                  &flag,
                                  sizeof(flag));
    
    checkError(status);
    
#ifdef USE_RECORDING
    
    status = AudioUnitSetProperty(audioUnit,
                                  kAudioOutputUnitProperty_EnableIO, // use io
                                  kAudioUnitScope_Input, // scope to input
                                  kInputBus, // select input bus (1)
                                  &flag, // set flag
                                  sizeof(flag));
    checkError(status);
#endif
    
    AudioStreamBasicDescription audioFormat={0};
    
    audioFormat.mSampleRate			= (double)SampleRate;
    audioFormat.mFormatID			= kAudioFormatLinearPCM;
    audioFormat.mFormatFlags		= kAudioFormatFlagIsSignedInteger | kAudioFormatFlagIsPacked;
    audioFormat.mFramesPerPacket	= 1;
    audioFormat.mChannelsPerFrame	= CHANNELS;
    audioFormat.mBitsPerChannel		= 16;
    audioFormat.mBytesPerPacket		= 2*CHANNELS;
    audioFormat.mBytesPerFrame		= 2*CHANNELS;
    
    
    
#ifdef USE_RECORDING
    
    status = AudioUnitSetProperty(audioUnit,
                                  kAudioUnitProperty_StreamFormat,
                                  kAudioUnitScope_Input,
                                  kOutputBus,
                                  &audioFormat,
                                  sizeof(audioFormat));
    checkError(status);
#endif
    
    status = AudioUnitSetProperty(audioUnit,
                                  kAudioUnitProperty_StreamFormat,
                                  kAudioUnitScope_Output,
                                  kInputBus,
                                  &audioFormat,
                                  sizeof(audioFormat));
    checkError(status);
    
#ifdef USE_RECORDING
    
    // set recording callback
    callbackStruct.inputProc = recordingCallback; // recordingCallback pointer
    callbackStruct.inputProcRefCon = (__bridge void*)self;
    
    // set input callback to recording callback on the input bus
    status = AudioUnitSetProperty(audioUnit,
                                  kAudioOutputUnitProperty_SetInputCallback,
                                  kAudioUnitScope_Global,
                                  kInputBus,
                                  &callbackStruct,
                                  sizeof(callbackStruct));
    checkError(status);
#endif
    
    callbackStruct.inputProc = playbackCallback;
    callbackStruct.inputProcRefCon = (void*)dataWrapper;//self;//_delegate;
    status = AudioUnitSetProperty(audioUnit,
                                  kAudioUnitProperty_SetRenderCallback,
                                  kAudioUnitScope_Global,
                                  kOutputBus,
                                  &callbackStruct,
                                  sizeof(callbackStruct));
    
    /* add callback for device route change */
 /*   AudioSessionAddPropertyListener (
                                     kAudioSessionProperty_AudioRouteChange,
                                     audioRouteChangeListenerCallback,
                                     (__bridge void *)(self));*/
    
    checkError(status);
    status = AudioUnitInitialize(audioUnit);
    checkError(status);
    status = AudioOutputUnitStart(audioUnit);
    checkError(status);
    NSLog(@"AppDelegate::InitAudioEngine end");
#endif
}

void audioRouteChangeListenerCallback (
                                       void                      *inUserData,
                                       AudioSessionPropertyID    inPropertyID,
                                       UInt32                    inPropertyValueSize,
                                       const void                *inPropertyValue) 
{
    // Code here
    if (inPropertyID != kAudioSessionProperty_AudioRouteChange) return;
    
    //audioViewController *controller = (audioViewController *) inUserData;
    
    CFDictionaryRef routeChangeDictionary = ( CFDictionaryRef )inPropertyValue;
    
    CFNumberRef routeChangeReasonRef =
    (CFNumberRef)CFDictionaryGetValue (
                          routeChangeDictionary,
                          CFSTR (kAudioSession_AudioRouteChangeKey_Reason));
    
    SInt32 routeChangeReason;
    
    CFNumberGetValue (
                      routeChangeReasonRef,
                      kCFNumberSInt32Type,
                      &routeChangeReason);
    
    CFStringRef oldRouteRef =
    (CFStringRef)CFDictionaryGetValue (
                          routeChangeDictionary,
                          CFSTR (kAudioSession_AudioRouteChangeKey_OldRoute));
    
    NSString *oldRouteString = (__bridge NSString *)oldRouteRef;
    OSStatus status;

    if (routeChangeReason == kAudioSessionRouteChangeReason_NewDeviceAvailable)
    {
        if ([oldRouteString isEqualToString:@"Speaker"])
        {
           // [controller.audioPlayer play];
        }
    }
    
    if (routeChangeReason ==
        kAudioSessionRouteChangeReason_OldDeviceUnavailable)
    {
        UInt32 newRoute = kAudioSessionOverrideAudioRoute_Speaker;
        status = AudioSessionSetProperty(kAudioSessionProperty_OverrideAudioRoute, sizeof(newRoute), &newRoute);
        checkError(status);

       /* if ((controller.audioPlayer.playing == YES) &&
            (([oldRouteString isEqualToString:@"Headphone"]) ||
             ([oldRouteString isEqualToString:@"LineOut"])))
        {
            //[controller.audioPlayer pause];
        }*/
    }
}

-(void) StopAudio
{
    OSStatus status = AudioOutputUnitStop(audioUnit);
    checkError(status);
   
}
/*
-(void) StartAudioNew
{
AVAudioSession* session = [AVAudioSession sharedInstance]; //error handling BOOL success;
    NSError* error; //set the audioSession category. //Needs to be Record or PlayAndRecord to use audioRouteOverride:
    bool success = [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
    if (!success)
        NSLog(@"AVAudioSession error setting category:%@",error); //set the audioSession
    
    success = [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error]; if (!success) NSLog(@"AVAudioSession error overrideOutputAudioPort:%@”,error);
    //activate the audio session
    success = [session setActive:YES error:&error];
    if (!success) NSLog(@"AVAudioSession error activating: %@",error); else NSLog(@"audioSession active");
AVAudioSessionRouteChangeNotification [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSessionRouteChange:) name:AVAudioSessionRouteChangeNotification object:nil];
}

-(void)didSessionRouteChange:(NSNotification *)notification {
    NSDictionary *interuptionDict = notification.userInfo;
    NSInteger routeChangeReason = [[interuptionDict valueForKey:AVAudioSessionRouteChangeReasonKey] integerValue];
    switch (routeChangeReason) {
        case AVAudioSessionRouteChangeReasonCategoryChange: { // Set speaker as default route
            NSError* error; [[AVAudioSession sharedInstance] overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error];
        } break; default: break;
    }
}*/

@end
