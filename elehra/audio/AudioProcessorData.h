//
//  AudioProcessorData.h
//  eLehra
//
//  Created by Jukka Rauhala on 12/05/15.
//  Copyright (c) 2015 AK Studios. All rights reserved.
//

#ifndef eLehra_AudioProcessorData_h
#define eLehra_AudioProcessorData_h
#include "TimeStretcherPL.h"
#include "SamplingRateConverter.h"
#include "HighShelvingFilter.h"
#include "LehraFile.h"

#define MAX_BPM 10
#define MAX_TAAL 10
#define MAX_RAAG 9
#define MAX_INSTRUMENT 4

#define BPM(i,t,r) i*MAX_RAAG*MAX_TAAL+t*MAX_RAAG+r



struct AudioProcessorData {
    float **fbuffer;
    LehraFile* inputFile;
    //NSObject* parentInstance;
    TimeStretcherPL* tsComponentL;
    SamplingRateConverter* srcComponentL;
    TimeStretcherPL* tsComponentR;
    SamplingRateConverter* srcComponentR;
    TimeStretcherPL* tsComponentX;
    SamplingRateConverter* srcComponentX;
    int bufferSize;
    //LehraFile lehraFiles[MAX_BPM*MAX_TAAL*MAX_RAAG*MAX_INSTRUMENT];
    LehraFile* allFiles[MAX_BPM*MAX_TAAL*MAX_RAAG*MAX_INSTRUMENT];
    int** fileIndex;
    int currentInstrument; // 0=sarangi, 1=santoor
    int currentTaal; // 0=teen taal, 1=roopak taal, 2=jhap taal,  3=ek taal, for sarangi 0=teen taal, 1=roopak taal
    int currentRaag; // 0=kirwani, 1=des, 2=bhairav
    int currentBpm; // 0=40, 1=60, 2=80, 3=100
    int bpmIndex;
    float bpmOrig;
    float bpmOld;
    int currentFileIndex;
    float beatCounter;
    FILE* tanpuraFile;
    float audioLevel;
    float audioLevelMem;
    int audioLevelCounter;
    float frameLevelMem;
    HighShelvingFilter* hsfL;
    HighShelvingFilter* hsfX;
    bool fadeIn;
    bool fadeOut;
    float fadeGain;
    bool isPlayingInternal;
    float lehraVolume;
    float tanpuraVolume;
    int TunerSemitone;
    int FineTunerValue;
    
    // settings for different instrument/taal/raags
    float tuningCoeffs[MAX_INSTRUMENT];
    int beatsPerFile[MAX_INSTRUMENT*MAX_TAAL];
    int cyclesPerFile[MAX_INSTRUMENT*MAX_TAAL];
    int bpmValues[MAX_INSTRUMENT*MAX_TAAL*MAX_RAAG][10];
    int bpmEdges[MAX_INSTRUMENT*MAX_TAAL*MAX_RAAG][10];
    int maxBpmValues[MAX_INSTRUMENT*MAX_TAAL*MAX_RAAG];
    
    // ui values
    bool ui_isPlaying;
    float ui_pitchCoeff;
    int ui_bmpValue;
    int ui_lowerCatIndex;
    int ui_middleCatIndex;
    int ui_upperCatIndex;
    float ui_tanpuraVolume;
    float ui_lehraVolume;
    
    int errorFlag;
};
#endif
