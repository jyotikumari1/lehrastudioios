//
//  AudioProcessorData_iOS.h
//  eLehra
//
//  Created by Jukka Rauhala on 20/08/15.
//  Copyright (c) 2015 AK Studios. All rights reserved.
//

#ifndef eLehra_AudioProcessorData_iOS_h
#define eLehra_AudioProcessorData_iOS_h
#include "LehraAudioProcessor.h"
#include "AudioProcessorData.h"

struct AudioProcessorData_iOS {
    AudioProcessorData* data;
    NSObject* parentInstance;
    LehraAudioProcessor* processor;

};
#endif
