//
//  HighShelvingFilter.cpp
//  eLehra
//
//  Created by Jukka Rauhala on 03/06/15.
//  Copyright (c) 2015 AK Studios. All rights reserved.
//

#include "HighShelvingFilter.h"
#include <math.h>
#define PI 3.141592653589793f
#define FS 44100.0f

HighShelvingFilter::HighShelvingFilter()
{
    Initialize();
}

HighShelvingFilter::~HighShelvingFilter()
{
    
}

int HighShelvingFilter::Process(float* buffer, int inBufferLen, float _fc, float _G)
{
    if (fc != _fc || G != _G)
        CalculateNewCoeffs(_fc, _G);
    
    float temp = 0.0f;
    for (int i=0; i < inBufferLen; i++)
    {
        temp = buffer[i] * b0 + zb1 * b1 + zb2 * b2 - za1 * a1 - za2 * a2;
        zb2 = zb1;
        zb1 = buffer[i];
        za2 = za1;
        za1 = temp;
        buffer[i] = temp;
    }
    return inBufferLen;
}

void HighShelvingFilter::Initialize(){
    b0 = 1.0f;
    b1 = 0.0f;
    b2 = 0.0f;
    a1 = 0.0f;
    a2 = 0.0f;
    za1 = 0.0f;
    za2 = 0.0f;
    zb1 = 0.0f;
    zb2 = 0.0f;
}

void HighShelvingFilter::CalculateNewCoeffs(float _fc, float _G)
{
    
    float denom = 0.0f;
    float root2 = sqrtf(2.0f);
    
    if (_fc != fc)
    {
        fc = _fc;
        K = tanf((PI*fc)/FS);
        denom = 1.0f / (1.0f + root2*K + K*K);
        a1 =  (2*(K*K - 1) ) * denom;
        a2 =  (1 - root2*K + K*K) * denom;
    } else {
        denom = 1.0f / (1 + root2*K + K*K);
        
    }
    G = _G;
    float KK = K*K;
    float V0 = powf(10.0f,G/20);
    float sqrtV0 = sqrtf(V0);
    float rootsqrtV0K = root2*sqrtV0*K;

    
    b0 = (V0 + rootsqrtV0K + KK) * denom;
    b1 =  (2*(KK - V0) ) * denom;
    b2 = (V0 - rootsqrtV0K + KK) * denom;
  
}
