//
//  HighShelvingFilter.h
//  eLehra
//
//  Created by Jukka Rauhala on 03/06/15.
//  Copyright (c) 2015 AK Studios. All rights reserved.
//

#ifndef __eLehra__HighShelvingFilter__
#define __eLehra__HighShelvingFilter__

#include <stdio.h>
class HighShelvingFilter
{
    
public:
    HighShelvingFilter();
    ~HighShelvingFilter();
    
    int Process(float* buffer, int inBufferLen, float fc, float G);
    void Initialize();
    void CalculateNewCoeffs(float fc, float G);
private:
    float fc;
    float G;
    float K;
    float a1,a2,b0,b1,b2;
    
    float zb1,zb2,za1,za2;
   
};
#endif /* defined(__eLehra__HighShelvingFilter__) */
