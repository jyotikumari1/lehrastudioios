//
//  LehraAudioProcessor.c
//  eLehra
//
//  Created by Jukka Rauhala on 20/08/15.
//  Copyright (c) 2015 AK Studios. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include "LehraAudioProcessor.h"
#include "math.h"
#ifdef USE_ANDROID
#include <android/log.h>
#endif

#define PI 3.141592653589793
#define BEATS_PER_BYTE  3.77929E-07
#define BYTES_PER_BEAT  5292000
#define GAIN    5.5f//1.0f
#define AUDIO_LEVEL_SMOOTHER    0.9
typedef unsigned char byte;

#define AUDIO_LEVEL_COUNTER_FRAME   512


#define MAX_BPM_TAAL    MAX_BPM*MAX_TAAL
#define MAX_BPM_TAAL_RAAG   MAX_BPM*MAX_TAAL*MAX_RAAG

LehraAudioProcessor::LehraAudioProcessor()
{
    data = new AudioProcessorData;
    
}

LehraAudioProcessor::~LehraAudioProcessor()
{
    delete data;
}

void LehraAudioProcessor::initialize()
{
    data->audioLevelCounter = 0;
    data->frameLevelMem = 0;
    data->currentBpm = 1;
    data->currentFileIndex = getFileIndex(1, 0, 2, 3);
    data->currentInstrument = 1;
    data->currentRaag = 2;
    data->currentTaal = 0;
    //data->inputFile =data->allFiles[data->currentFileIndex];
    int i= 0;
    i = 1;
    data->beatCounter = 0;
    data->isPlayingInternal = false;
    data->fadeIn = false;
    data->fadeOut = false;
    data->fadeGain = 0.0f;
    data->ui_isPlaying = true;
    data->ui_bmpValue = 90;
    data->ui_lehraVolume = 0.5f;
    data->ui_tanpuraVolume = 0.5f;
    data->ui_lowerCatIndex = 0;
    data->ui_middleCatIndex = 0;
    data->ui_upperCatIndex = 0;
    data->ui_pitchCoeff = 1.0f;
    data->errorFlag = 0;
    
    data->tsComponentL = new TimeStretcherPL();
    data->tsComponentR = new TimeStretcherPL();
    data->srcComponentL = new SamplingRateConverter();
    data->srcComponentR = new SamplingRateConverter();
    data->hsfL = new HighShelvingFilter();
    data->hsfL->CalculateNewCoeffs(6000.0f, 15.0f);
    data->hsfX = new HighShelvingFilter();
    data->hsfX->CalculateNewCoeffs(6000.0f, 15.0f);
    data->fbuffer = new float*[4];
    data->fbuffer[0] = new float[20000];
    data->fbuffer[1] = new float[20000];
    data->fbuffer[2] = new float[20000];
    data->tsComponentX = new TimeStretcherPL();
    data->srcComponentX = new SamplingRateConverter();
    data->fbuffer[3] = new float[20000];
    data->audioLevelMem = 0.0f;
    data->audioLevel = 0.0f;
    data->tsComponentL->Initialize();
    data->tsComponentR->Initialize();
    data->tsComponentX->Initialize();

}

int LehraAudioProcessor::getFileIndex(int instrument, int taal, int raag, int bpm)
{
    return (instrument)*MAX_BPM_TAAL_RAAG + (raag)*MAX_BPM_TAAL + (taal)*MAX_BPM + bpm;
}

LehraFile* LehraAudioProcessor::getLehraFile(AudioProcessorData* data,int instrument, int taal, int raag, int bpm)
{
    data->currentFileIndex = (instrument)*MAX_BPM_TAAL_RAAG + (raag)*MAX_BPM_TAAL + (taal)*MAX_BPM + bpm;
    return data->allFiles[data->currentFileIndex];
}

AudioProcessorData* LehraAudioProcessor::getAudioProcessorData()
{
    return data;
}

void LehraAudioProcessor::addLehraPath(const char* filepath, int instrument, int taal, int raag, int *bpms, int numOfBpms,int beats,int cycles)
{
    FILE* f = fopen(filepath,"rb");
    if (f== NULL){
        data->errorFlag = 2;
#ifdef USE_ANDROID
        __android_log_print(ANDROID_LOG_INFO, "Tag", "File not found %s\n",filepath);//: %ld, %ld\n",ftell(data->inputFile) - 44,ftell(data->tanpuraFile) - 44);
#endif
    } else {
        fseek(f, 46, SEEK_CUR);
        
        long startPosition = 46;
        for (int i=0; i< numOfBpms; i++)
        {
            int bpm = bpms[i];
            int index = getFileIndex(instrument, taal, raag, i);
            float samplesPerBeat = 44100.0f* 60.0f /(float)bpm;
            long expectedLen = (long)(2.0f* samplesPerBeat *  (float)beats *(float)cycles);
            //std::cout << "File:" << filepath << " length:" << expectedLen << "\n";
            LehraFile* lf = initLehraFile(f, startPosition, expectedLen);
            startPosition += expectedLen;
            data->allFiles[index] = lf;
        }
    }
}

int LehraAudioProcessor::getErrorFlag()
{
    return data->errorFlag;
}

void LehraAudioProcessor::addTanpuraPath(const char* filepath)
{
    FILE* f = fopen(filepath,"rb");
    if (f== NULL){
        data->errorFlag = 1;
#ifdef USE_ANDROID
        __android_log_print(ANDROID_LOG_INFO, "Tag", "File not found %s\n",filepath);//: %ld, %ld\n",ftell(data->inputFile) - 44,ftell(data->tanpuraFile) - 44);
#endif
    } else {
        fseek(f, 44, SEEK_CUR);
        data->tanpuraFile = f;
    }
    
}

float LehraAudioProcessor::getAudioLevel()
{
    return data->audioLevel;
}

float LehraAudioProcessor::getBeatCounter(){
    return data->beatCounter;
}


void LehraAudioProcessor::calculateCoeffs(AudioProcessorData* data, float* pitchOut, float* tsCoeffOut)
{
    float pitch = data->ui_pitchCoeff;
    int bpm = data->ui_bmpValue;
    
    
    bool fileChangeRequired = false;
    //int sarangi_bpms[] = {40,50,60,75,90,120,150,180};
    float bpmOld = 0.0f;
    /*if (data->currentInstrument == 0) {
     bpmOld = (float)sarangi_bpms[data->currentBpm];
     } else {
     bpmOld = 40.0f+data->currentBpm*20.0f;
     }*/
    bpmOld = (float)data->bpmValues[BPM(data->currentInstrument,data->currentTaal,data->currentRaag)][data->currentBpm];
    
   /* if (data->ui_lowerCatIndex != data->currentRaag)
    {
        data->currentRaag = data->ui_lowerCatIndex;
        fileChangeRequired = true;
    }
    if (data->ui_middleCatIndex != data->currentTaal)
    {
        data->currentTaal = data->ui_middleCatIndex;
        fileChangeRequired = true;
    }
    if (data->ui_upperCatIndex != data->currentInstrument)
    {
        data->currentInstrument = data->ui_upperCatIndex;
        fileChangeRequired = true;
    }*/
    
    int bpmIndex = 0;
    float tanpuraPitch = pitch * 1.18919841518334f;
    float tanpuraPitch2 = 2.0f*pitch * 1.18919841518334f; // sampling rate 22.05kHz
    pitch = pitch * data->tuningCoeffs[data->ui_upperCatIndex];
    
    while (bpmIndex < data->maxBpmValues[BPM(data->ui_upperCatIndex,data->ui_middleCatIndex,data->ui_lowerCatIndex)]-1 && bpm >=  data->bpmEdges[BPM(data->ui_upperCatIndex,data->ui_middleCatIndex,data->ui_lowerCatIndex)][bpmIndex])
    {
        bpmIndex++;
    }
    
    
    
    float bpmOrig = 0;
    
    bpmOrig = (float)data->bpmValues[BPM(data->ui_upperCatIndex,data->ui_middleCatIndex,data->ui_lowerCatIndex)][bpmIndex];
#ifdef USE_ANDROID
    __android_log_print(ANDROID_LOG_INFO, "Tag", "CprocessAudio 2 %f %d\n",pitch,bpm);//: %ld, %ld\n",ftell(data->inputFile) - 44,ftell(data->tanpuraFile) - 44);
#endif
    
    data->bpmIndex = bpmIndex;
    
    float bpmCoeff = (float)bpm / bpmOrig;
    float tsCoeff = pitch * bpmCoeff;
    *pitchOut = pitch;
    *tsCoeffOut = tsCoeff;
    data->bpmOld = bpmOld;
    data->bpmOrig = bpmOrig;
    
}

bool LehraAudioProcessor::updateLehraFile(AudioProcessorData* data) {
#ifdef USE_ANDROID
    __android_log_print(ANDROID_LOG_INFO, "Tag", "UpdateLehraFile 1\n");
#endif
    bool fileChangeRequired = false;
    if (data->ui_lowerCatIndex != data->currentRaag)
    {
        data->currentRaag = data->ui_lowerCatIndex;
        fileChangeRequired = true;
    }
    if (data->ui_middleCatIndex != data->currentTaal)
    {
        data->currentTaal = data->ui_middleCatIndex;
        fileChangeRequired = true;
    }
    if (data->ui_upperCatIndex != data->currentInstrument)
    {
        data->currentInstrument = data->ui_upperCatIndex;
        fileChangeRequired = true;
    }
#ifdef USE_ANDROID
    __android_log_print(ANDROID_LOG_INFO, "Tag", "UpdateLehraFile 2\n");
#endif
    if (data->bpmIndex != data->currentBpm || fileChangeRequired)
    {
        fileChangeRequired = true;
        //NSLog(@"bpmIndex:%d", bpmIndex);
#ifdef USE_ANDROID
        __android_log_print(ANDROID_LOG_INFO, "Tag", "UpdateLehraFile 3 %p\n",data->inputFile);
#endif
        long oldposition = 0;
        try {
        if (data->inputFile != NULL && data->inputFile == data->allFiles[data->currentFileIndex])
            oldposition = ftell(data->inputFile);// - 44;
        } catch(...) {
#ifdef USE_ANDROID
            __android_log_print(ANDROID_LOG_INFO, "Tag", "UpdateLehraFile 3 %p ftell exception\n",data->inputFile);
#endif
        }
#ifdef USE_ANDROID
        __android_log_print(ANDROID_LOG_INFO, "Tag", "UpdateLehraFile 3b %p\n",data->inputFile);
#endif
        data->inputFile = getLehraFile(data,data->currentInstrument, data->currentTaal, data->currentRaag, data->bpmIndex);//data->allFiles[getFileIndex(data->currentInstrument, data->currentTaal, data->currentRaag, bpmIndex)];
#ifdef USE_ANDROID
        __android_log_print(ANDROID_LOG_INFO, "Tag", "UpdateLehraFile 4\n");
#endif
        if(data->inputFile == NULL) {
            data->errorFlag = 3;
#ifdef USE_ANDROID
            __android_log_print(ANDROID_LOG_INFO, "Tag", "INPUT FILE NULL\n");
#endif
            return true;
        }
#ifdef USE_ANDROID
        __android_log_print(ANDROID_LOG_INFO, "Tag", "UpdateLehraFile 5\n");
#endif
        fseek(data->inputFile,0,SEEK_SET);//44,SEEK_SET);
        long position = (long)((float)oldposition * data->bpmOld*0.5 / data->bpmOrig) * 2;//(long)(data->beatCounter *60.0f*44100.0f / bpmOrig);
        //std::cout << "Old pos:" << oldposition << " New pos:" << position << "\n";
        //position = position * 2 * CHANNELS;
#ifdef USE_ANDROID
        __android_log_print(ANDROID_LOG_INFO, "Tag", "UpdateLehraFile 6\n");
#endif
        fseek(data->inputFile, position, SEEK_CUR);
        data->currentBpm = data->bpmIndex;
    }
    
#ifdef USE_ANDROID
    __android_log_print(ANDROID_LOG_INFO, "Tag", "UpdateLehraFile 7\n");
#endif
    if(data->inputFile == NULL) {
        data->errorFlag = 4;
#ifdef USE_ANDROID
        __android_log_print(ANDROID_LOG_INFO, "Tag", "INPUT FILE NULL 2\n");
#endif
        return fileChangeRequired;
    }
    return fileChangeRequired;

}

void LehraAudioProcessor::readLehraData(LehraFile* inputFile, int neededBytes, float *buffer, int bufferLen,int start,float *beatCounter)
{
    byte bbuffer[20000];



    if (feofLf(inputFile))
    {
        fseek(inputFile, 0,SEEK_SET);//44, SEEK_SET);
        *beatCounter = 0;
        //NSLog(@"Input file end.");
    }
    /*if(data->tanpuraFile == NULL) {
        data->errorFlag = 5;
#ifdef USE_ANDROID
        __android_log_print(ANDROID_LOG_INFO, "Tag", "INPUT FILE NULL 3\n");
#endif
        return;
    }
    if (feof(data->tanpuraFile))
    {
        fseek(data->tanpuraFile, 44, SEEK_SET);
        //memset(&tbuffer[0],0,sizeof(byte)*tsBufferLen*4);
    }*/
    // read data from file
    int len = neededBytes;//tsBufferLen*2;//*CHANNELS;
    int ret = (int)fread(&bbuffer[0],1, len,inputFile);
    if (ret < len)
    {
        //long temp = ftell(inputFile);
        fseek(inputFile, 0,SEEK_SET);//44, SEEK_SET);
        *beatCounter = 0;
        fread(&bbuffer[0]+ret,1, len-ret,inputFile);
        // NSLog(@"Input file end 2.");
        
    }
    // convert byte to float
    for (int i = 0; i < neededBytes/2; i++)
    {
        //for (int k = 0; k < CHANNELS; k++)
        int k=0;
        {
            short shortSample = 0;
            int index = (i * 2+k);//*CHANNELS;
            shortSample = (short)(bbuffer[index] + (((int)bbuffer[index + 1]) << 8));
            //if (shortSample > 0)
            {
                //fbuffer[k][i] = (double)shortSample * ShortMaxInv;
#if TANPURA_BPM_CONTROL
                buffer[i] =  (float)shortSample * 0.00003052;
                
#else
                buffer[i] =  (float)shortSample * ShortMaxInv * data->ui_lehraVolume;
                shortSample = (short)(tbuffer[index] + (((int)tbuffer[index + 1]) << 8));
                buffer[i] +=  (float)shortSample * ShortMaxInv * data->ui_tanpuraVolume;
                
#endif
                
                //data->fbuffer[k][i] =  sinf(2.0f*PI*440.0f*phase+k*PI);//(float)shortSample * ShortMaxInv;
            }
            /*else
             {
             //fbuffer[k][i] = (double)shortSample * ShortMinInv;
             data->fbuffer[k][i] =  (float)shortSample * ShortMinInv;
             //data->fbuffer[k][i] =  sinf(2.0f*PI*440.0f*phase+k*PI);//(float)shortSample * ShortMaxInv;
             }*/
            //phase += 1.0f/44100.0f;
        }
    }

}

void LehraAudioProcessor::processAudio(short* buffer, int bufferLen,int start)
/*{
    
    processAudio(&fbuffer[0], bufferLen);
    
    for (int i=0; i < bufferLen; i++)
    {
        buffer[i] = (signed short)(fbuffer[i]*10000.0f);
    }
}

void LehraAudioProcessor::processAudio(float* buffer, int bufferLen)*/
{
#define LEHRA_PITCH_COEFF 1.0f // 44100.0f/sample_rate
    
#ifdef USE_ANDROID
    //__android_log_print(ANDROID_LOG_INFO, "Tag", "CprocessAudio 1\n");
#endif

    if (data->ui_isPlaying != data->isPlayingInternal)
    {
        if (data->ui_isPlaying)
        {
            data->fadeIn = true;
            data->isPlayingInternal = true;
        } else {
            data->fadeOut = true;
        }
    }
    
    if (!data->isPlayingInternal)//ui.isPlaying)
    {
        data->audioLevel = 0.0f;
        memset(buffer+start, 0, sizeof(short)*bufferLen*CHANNELS);
        return;
    }
    
     byte tbuffer[20000];
    /*float **fbuffer;
     fbuffer = new float*[2];
     fbuffer[0] = new float[20000];
     fbuffer[1] = new float[20000];*/
    /*byte bbuffer[20000];

    
    float ShortMaxInv = 1.0f / (float)32767;
    //float ShortMinInv = -1.0f / (float)-32767;
    // int Channels = 2;
    
    // read bpm and pitch values
    float pitch = data->ui_pitchCoeff;
    int bpm = data->ui_bmpValue;
    
    
    bool fileChangeRequired = false;
    //int sarangi_bpms[] = {40,50,60,75,90,120,150,180};
    float bpmOld = 0.0f;

    bpmOld = (float)data->bpmValues[BPM(data->currentInstrument,data->currentTaal,data->currentRaag)][data->currentBpm];
    
    if (data->ui_lowerCatIndex != data->currentRaag)
    {
        data->currentRaag = data->ui_lowerCatIndex;
        fileChangeRequired = true;
    }
    if (data->ui_middleCatIndex != data->currentTaal)
    {
        data->currentTaal = data->ui_middleCatIndex;
        fileChangeRequired = true;
    }
    if (data->ui_upperCatIndex != data->currentInstrument)
    {
        data->currentInstrument = data->ui_upperCatIndex;
        fileChangeRequired = true;
    }
    
    int bpmIndex = 0;
    float tanpuraPitch = pitch * 1.18919841518334f;
    float tanpuraPitch2 = 2.0f*pitch * 1.18919841518334f; // sampling rate 22.05kHz
    pitch = pitch * data->tuningCoeffs[data->currentInstrument];
    
    while (bpmIndex < data->maxBpmValues[BPM(data->currentInstrument,data->currentTaal,data->currentRaag)]-1 && bpm >=  data->bpmValues[BPM(data->currentInstrument,data->currentTaal,data->currentRaag)][bpmIndex])
    {
        bpmIndex++;
    }
    
    /*
    if (data->currentInstrument == 0)
    {
        if(bpm >=165 && data->currentTaal<2){
            bpmIndex = 7;
        } else if (bpm >= 135 && data->currentTaal<2)
        {
            bpmIndex = 6;
        } else if (bpm >= 105 && data->currentTaal <2)
        {
            bpmIndex = 5;
        }
        else if (bpm >= 83){
            bpmIndex = 4;
        } else if (bpm >= 67){
            bpmIndex = 3;
        } else if (bpm >= 55){
            bpmIndex = 2;
        } else if (bpm >= 45)
        {
            bpmIndex = 1;
        }
        pitch = pitch * 0.94388068f; // compensate for the sarangi's tuning
        
        
    } else {
        if (bpm >= 110 && (data->currentTaal == 1 ||data->currentTaal == 3)){
            bpmIndex = 4;
        } else if (bpm >= 90){
            bpmIndex = 3;
        } else if (bpm >= 70){
            bpmIndex = 2;
        } else if (bpm >= 50)
        {
            bpmIndex = 1;
        }
        
    }
    
    float bpmOrig = 0;

    bpmOrig = (float)data->bpmValues[BPM(data->currentInstrument,data->currentTaal,data->currentRaag)][bpmIndex];
#ifdef USE_ANDROID
    __android_log_print(ANDROID_LOG_INFO, "Tag", "CprocessAudio 2 %f %d\n",pitch,bpm);//: %ld, %ld\n",ftell(data->inputFile) - 44,ftell(data->tanpuraFile) - 44);
#endif

    if (bpmIndex != data->currentBpm || fileChangeRequired)
    {
        //NSLog(@"bpmIndex:%d", bpmIndex);
        long oldposition = ftell(data->inputFile);// - 44;
        data->inputFile = getLehraFile(data->currentInstrument, data->currentTaal, data->currentRaag, bpmIndex);//data->allFiles[getFileIndex(data->currentInstrument, data->currentTaal, data->currentRaag, bpmIndex)];
        if(data->inputFile == NULL) {
            data->errorFlag = 3;
            #ifdef USE_ANDROID
             __android_log_print(ANDROID_LOG_INFO, "Tag", "INPUT FILE NULL\n");
#endif
            return;
        }
        fseek(data->inputFile,0,SEEK_SET);//44,SEEK_SET);
        long position = (long)((float)oldposition * bpmOld*0.5 / bpmOrig) * 2;//(long)(data->beatCounter *60.0f*44100.0f / bpmOrig);
        //std::cout << "Old pos:" << oldposition << " New pos:" << position << "\n";
        //position = position * 2 * CHANNELS;
        fseek(data->inputFile, position, SEEK_CUR);
        data->currentBpm = bpmIndex;
    }
    */
    float pitch = 0;
    float tsCoeff = 0;
    calculateCoeffs(data, &pitch, &tsCoeff);
    //updateLehraFile(data);
    float tanpuraPitch = data->ui_pitchCoeff * 1.18919841518334f;
    float tanpuraPitch2 = 2.0f*tanpuraPitch; // sampling rate 22.05kHz

    
    // determine how much new data we need

    int srcBufferLen = data->srcComponentL->GetInputFrameSize(pitch*LEHRA_PITCH_COEFF, bufferLen);

    //float bpmOrig = 20.0f+bpmIndex*20.0f;
    /*float bpmCoeff = (float)bpm / bpmOrig;
    float tsCoeff = pitch * bpmCoeff;
    */
    
#ifdef USE_ANDROID
    __android_log_print(ANDROID_LOG_INFO, "Tag", "CprocessAudio 3\n");
#endif


    int tsBufferLen = data->tsComponentL->GetInputFrameSize(tsCoeff, srcBufferLen);

    int srcBufferLen2 = data->srcComponentX->GetInputFrameSize(tanpuraPitch2, bufferLen);
#if TANPURA_BPM_CONTROL
    int tsBufferLen2 = data->tsComponentX->GetInputFrameSize(tanpuraPitch, srcBufferLen2);
    //    int tsBufferLen2 = data->tsComponentX->GetInputFrameSize(tanpuraPitch, srcBufferLen2);
#else
    int tsBufferLen2 = tsBufferLen;
#endif
#ifdef USE_ANDROID
    __android_log_print(ANDROID_LOG_INFO, "Tag", "CprocessAudio 4\n");
#endif
    updateLehraFile(data);
    if(data->inputFile == NULL) {
        data->errorFlag = 4;
#ifdef USE_ANDROID
        __android_log_print(ANDROID_LOG_INFO, "Tag", "INPUT FILE NULL 2\n");
#endif
        return;
    }

    readLehraData(data->inputFile, tsBufferLen*2, data->fbuffer[0], bufferLen, start,&data->beatCounter);
  /*  if (feof(data->inputFile))
    {
        fseek(data->inputFile, 0,SEEK_SET);//44, SEEK_SET);
        data->beatCounter = 0;
        //NSLog(@"Input file end.");
    }*/
    if(data->tanpuraFile == NULL) {
        data->errorFlag = 5;
#ifdef USE_ANDROID
        __android_log_print(ANDROID_LOG_INFO, "Tag", "INPUT FILE NULL 3\n");
#endif
        return;
    }
    if (feof(data->tanpuraFile))
    {
        fseek(data->tanpuraFile, 44, SEEK_SET);
        //memset(&tbuffer[0],0,sizeof(byte)*tsBufferLen*4);
    }
    // read data from file
    /*int len = tsBufferLen*2;//*CHANNELS;
    int ret = (int)fread(&bbuffer[0],1, len,data->inputFile);
    if (ret < len)
    {
        long temp = ftell(data->inputFile);
        fseek(data->inputFile, 0,SEEK_SET);//44, SEEK_SET);
        data->beatCounter = 0;
        fread(&bbuffer[0]+ret,1, len-ret,data->inputFile);
       // NSLog(@"Input file end 2.");
     
    }*/
#ifdef USE_ANDROID
   // __android_log_print(ANDROID_LOG_INFO, "Tag", "CprocessAudio 5: %ld %d %d %f %d %f %d\n",ftell(data->inputFile) ,ret,len,tsCoeff,srcBufferLen,pitch,bufferLen);
#endif
    int len2 = tsBufferLen2*2;//*CHANNELS;
    int ret = (int)fread(&tbuffer[0],1, len2,data->tanpuraFile);
    if (ret < len2)
    {
        //memset(&tbuffer[0],0,sizeof(byte)*tsBufferLen*4);
        fseek(data->tanpuraFile, 44, SEEK_SET);
        fread(&tbuffer[0]+ret,1, len2-ret,data->tanpuraFile);
        // int ret2 = (int)fread(&tbuffer[0],1, len2,data->tanpuraFile);
        
    }
    

    // convert byte to float
   /* for (int i = 0; i < tsBufferLen; i++)
    {
        //for (int k = 0; k < CHANNELS; k++)
        int k=0;
        {
            short shortSample = 0;
            int index = (i * 2+k);//*CHANNELS;
            shortSample = (short)(bbuffer[index] + (((int)bbuffer[index + 1]) << 8));
            //if (shortSample > 0)
            {
                //fbuffer[k][i] = (double)shortSample * ShortMaxInv;
#if TANPURA_BPM_CONTROL
                data->fbuffer[k][i] =  (float)shortSample * ShortMaxInv;
                
#else
                data->fbuffer[k][i] =  (float)shortSample * ShortMaxInv * data->ui_lehraVolume;
                shortSample = (short)(tbuffer[index] + (((int)tbuffer[index + 1]) << 8));
                data->fbuffer[k][i] +=  (float)shortSample * ShortMaxInv * data->ui_tanpuraVolume;
                
#endif
                
                //data->fbuffer[k][i] =  sinf(2.0f*PI*440.0f*phase+k*PI);//(float)shortSample * ShortMaxInv;
            }

            //phase += 1.0f/44100.0f;
        }
    }*/
#ifdef USE_ANDROID
    __android_log_print(ANDROID_LOG_INFO, "Tag", "CprocessAudio 6\n");
#endif
#if TANPURA_BPM_CONTROL
    for (int i = 0; i < tsBufferLen2; i++)
    {
        //for (int k = 0; k < CHANNELS; k++)
        int k=0;
        {
            short shortSample = 0;
            int index = (i * 2+k);//*CHANNELS;
            shortSample = (short)(tbuffer[index] + (((int)tbuffer[index + 1]) << 8));
            data->fbuffer[k+2][i] =  (float)shortSample * 0.00003052;
            //data->fbuffer[k][i] =  sinf(2.0f*PI*440.0f*phase+k*PI);//(float)shortSample * ShortMaxInv;
            
            //phase += 1.0f/44100.0f;
        }
    }
#endif
    
#ifdef USE_ANDROID
    __android_log_print(ANDROID_LOG_INFO, "Tag", "CprocessAudio 7\n");
#endif
    
    // timestretch
    data->tsComponentL->Process(data->fbuffer[0], tsBufferLen, srcBufferLen, tsCoeff);
#if TANPURA_BPM_CONTROL
    data->tsComponentX->Process(data->fbuffer[2], tsBufferLen2, srcBufferLen2, tanpuraPitch);
#endif
    //data->tsComponentR->Process(data->fbuffer[1], tsBufferLen, srcBufferLen, tsCoeff);
    
    // sampling rate conversion

    data->srcComponentL->Process(data->fbuffer[0], srcBufferLen,bufferLen, pitch*LEHRA_PITCH_COEFF);

    //data->srcComponentR->Process(data->fbuffer[1], srcBufferLen,bufferLen, pitch);
#if TANPURA_BPM_CONTROL
    data->srcComponentX->Process(data->fbuffer[2], srcBufferLen2,bufferLen, tanpuraPitch2);
    //    data->srcComponentX->Process(data->fbuffer[2], srcBufferLen2,bufferLen, tanpuraPitch);
#endif
#ifdef USE_ANDROID
    __android_log_print(ANDROID_LOG_INFO, "Tag", "CprocessAudio 8\n");
#endif
    /*
     for (int i = 0; i < bufferLen; i++)
     {
     for (int k = 0; k < CHANNELS; k++)
     {
     #if TANPURA_BPM_CONTROL
     data->fbuffer[k][i] =  data->fbuffer[k][i] + data->fbuffer[k+2][i];
     #else
     data->fbuffer[k][i] =  data->fbuffer[k][i];
     
     #endif
     }
     }*/
    
    data->hsfL->Process(data->fbuffer[0], bufferLen, 6000.0f, 15.0f);
    data->hsfX->Process(data->fbuffer[2], bufferLen, 6000.0f, 15.0f);
    
#ifdef USE_ANDROID
    __android_log_print(ANDROID_LOG_INFO, "Tag", "CprocessAudio 9\n");
#endif
    
    // float to short
    float level = data->frameLevelMem;//0.0f;
    float tempx = 0.0f;
    
    for(int i=0; i < bufferLen; i++)
    {
#if TANPURA_BPM_CONTROL
        float sample =(data->fbuffer[0][i]*data->ui_lehraVolume+data->fbuffer[2][i]*data->ui_tanpuraVolume)* GAIN* data->fadeGain;
        buffer[i*CHANNELS +start] = (signed short)(sample);
        if(data->fadeOut)
        {
            data->fadeGain -= 0.0001f;
            if (data->fadeGain < 0.0f)
            {
                data->fadeGain = 0.0f;
                data->fadeOut = false;
                data->isPlayingInternal = false;
            }
        } else if (data->fadeIn)
        {
            data->fadeGain += 0.0001f;
            if (data->fadeGain > 1.0f)
            {
                data->fadeGain = 1.0f;
                data->fadeIn = false;
            }
            
        }
#else
        float sample =(data->fbuffer[0][i])* GAIN * data->fadeGain;
        if(data->fadeOut)
        {
            data->fadeGain -= 0.001f;
            if (data->fadeGain < 0.0f)
            {
                data->fadeGain = 0.0f;
                data->fadeOut = false;
                data->isPlayingInternal = false;
            }
        } else if (data->fadeIn)
        {
            data->fadeGain += 0.001f;
            if (data->fadeGain > 1.0f)
            {
                data->fadeGain = 1.0f;
                data->fadeIn = false;
            }
            
        }
        buffer[i*CHANNELS ] = (signed short)(sample);
#endif
        tempx= sample*sample;//data->fbuffer[0][i]*data->fbuffer[0][i];
        level += tempx;
        
        data->audioLevelCounter++;
        if (data->audioLevelCounter >= AUDIO_LEVEL_COUNTER_FRAME)
        {
            data->audioLevelCounter = 0;
            level = sqrt(level) / (float)(bufferLen*CHANNELS);
            data->audioLevel = level * (1.0f-AUDIO_LEVEL_SMOOTHER) + data->audioLevelMem * AUDIO_LEVEL_SMOOTHER;
            data->audioLevelMem = data->audioLevel;
            level = 0;
        }
        /*if (sample > 32768.0f || sample < -32768.0f)
         {
         memcpy(&temp[0], buffer,2048*sizeof(short));
         float temp2[4096];
         memcpy(&temp2[0],data->fbuffer[0],sizeof(float)*bufferLen*CHANNELS);
         std::cout << "OVERFLOW: " << sample / 32768.0f << "\n";
         }*/
        
        for (int k = 1; k < CHANNELS; k++)
        {
            buffer[i*CHANNELS+k] = sample;
            
        }
        //buffer[i*Channels + k] = (signed short)(doubleBuffer[k][i] * 32768.0);
    }
    //memcpy(&temp[0], buffer,bufferLen*sizeof(short));
    data->frameLevelMem = level;
    data->beatCounter += (float)bufferLen * BEATS_PER_BYTE * data->ui_bmpValue;
    
    //int max_beats[] = {16,7,10,12};
    
    /*if (data->currentInstrument==0)
     {
     while (data->beatCounter > 12)
     {
     data->beatCounter -= 12.0f;
     }
     
     } else {*/
    while (data->beatCounter > data->beatsPerFile[data->currentInstrument*MAX_TAAL+data->currentTaal])//max_beats[data->currentTaal])
    {
        data->beatCounter -= (float) data->beatsPerFile[data->currentInstrument*MAX_TAAL+data->currentTaal];
    }
    //}
    
    /*
     if (data->currentInstrument == 0 && data->currentTaal == 1) // if sarangi roopak taal
     {
     while (data->beatCounter > 7)
     {
     data->beatCounter -= 7.0f;
     }
     
     } else if (data->currentInstrument == 1 && data->currentTaal == 3 && data->beatCounter > 12) {
     data->beatCounter -= 12.0f;
     }else if (data->beatCounter > 16)
     {
     data->beatCounter -= 16.0f;
     }*/
#ifdef USE_ANDROID
    __android_log_print(ANDROID_LOG_INFO, "Tag", "CprocessAudio 10\n");
#endif

}
