//
//  LehraAudioProcessor.h
//  eLehra
//
//  Created by Jukka Rauhala on 20/08/15.
//  Copyright (c) 2015 AK Studios. All rights reserved.
//

#ifndef eLehra_LehraAudioProcessor_h
#define eLehra_LehraAudioProcessor_h
#include "AudioProcessorData.h"
#define CHANNELS 1
#define TANPURA_BPM_CONTROL 1 // if tanpura is bpm dependent or not
class LehraAudioProcessor
{
    
public:
    LehraAudioProcessor();
    ~LehraAudioProcessor();
    void processAudio(short* buffer, int bufferLen,int start);
    //void processAudio(float* buffer, int bufferLen);
    int getFileIndex(int instrument, int taal, int raag, int bpm);
    AudioProcessorData* getAudioProcessorData();
    void addTanpuraPath(const char* filepath);
    void addLehraPath(const char* filepath, int instrument, int taal, int raag, int *bpms, int numOfBpms,int beats,int cycles);
    float getAudioLevel();
    float getBeatCounter();
    void initialize();
    int getErrorFlag();
    static LehraFile* getLehraFile(AudioProcessorData* data,int instrument, int taal, int raag, int bpm);
    static void calculateCoeffs(AudioProcessorData* data, float* pitch, float* tsCoeff);
    static void readLehraData(LehraFile* inputFile, int neededBytes, float *buffer, int bufferLen,int start,float *beatCounter);
    static bool updateLehraFile(AudioProcessorData* data);
private:
    AudioProcessorData *data;
    float fbuffer[4096];

};
#endif
