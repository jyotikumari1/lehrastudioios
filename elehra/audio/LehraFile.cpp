//
//  LehraFile.cpp
//  elehra
//
//  Created by Jukka Rauhala on 30/12/15.
//  Copyright © 2015 AK Studios. All rights reserved.
//

#include "LehraFile.h"

LehraFile* initLehraFile(FILE* f, long startPosition_, long length_) {
    LehraFile* lf = new LehraFile;
    
    lf->iFile = f;
    lf->startPosition = startPosition_;
    lf->length = length_;
    lf->currentPosition = lf->startPosition;
    lf->endPosition = lf->startPosition+lf->length;
    
    return lf;
}

bool feofLf(LehraFile* file)
{
    if(feof(file->iFile) || file->currentPosition >= file->endPosition)
        return true;
    
    return false;
}

unsigned long fread(byte* buffer,int i,int len,LehraFile* file)
{
    long left = file->endPosition-file->currentPosition;
    long llen = (long)len;
    if(llen > left)
    {
        file->currentPosition += left;
        return fread(buffer,i,left,file->iFile);
    }
    file->currentPosition += len;
    
    return fread(buffer,i,len,file->iFile);
}

long ftell(LehraFile* file) {
    return file->currentPosition-file->startPosition;
}

int fseek(LehraFile* file,long position, int flag)
{
    if (flag == SEEK_SET){
        fseek(file->iFile,file->startPosition+position,flag);
        file->currentPosition = file->startPosition+position;
    } else if (flag == SEEK_CUR) {
        fseek(file->iFile,position,flag);
        file->currentPosition += position;
    }
    return 0;
}