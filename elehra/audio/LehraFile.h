//
//  LehraFile.hpp
//  elehra
//
//  Created by Jukka Rauhala on 30/12/15.
//  Copyright © 2015 AK Studios. All rights reserved.
//

#ifndef LehraFile_hpp
#define LehraFile_hpp

#include <stdio.h>
struct LehraFile {
    FILE* iFile;
    long startPosition;
    long length;
    long endPosition;
    long currentPosition;
};
typedef unsigned char byte;

LehraFile* initLehraFile(FILE* f, long startPosition_, long length_);

bool feofLf(LehraFile* file);
unsigned long fread(byte* buffer,int i,int len,LehraFile* file);
long ftell(LehraFile* file);
int fseek(LehraFile* file,long position, int flag);

#endif /* LehraFile_hpp */
