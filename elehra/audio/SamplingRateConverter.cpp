//
//  SamplingRateConverter.cpp
//  eLehra
//
//  Created by Jukka Rauhala on 06/05/15.
//  Copyright (c) 2015 AK Studios. All rights reserved.
//

#include "SamplingRateConverter.h"
#include <math.h>
#include <string.h>
#ifdef USE_ANDROID
#include <android/log.h>
#endif

SamplingRateConverter::SamplingRateConverter()
{
    ibuffer_read = 0;
    ibuffer_write = 0;
    bypass = true;
    mem = 0;
    ibuffer = new float[VSRC_BUFFER_LENGTH];
    Reset();
}

int SamplingRateConverter::GetInputFrameSize(float pitchCoeff, int bufferLen)
{
    float targetSampleIndex = ceil(((float)bufferLen)/pitchCoeff);
    float samplesAvailable = (float)ibuffer_write - sampleIndex;
    if (samplesAvailable < 0)
        samplesAvailable += VSRC_BUFFER_LENGTH;
    
    int samplesNeeded =(int)(ceil(targetSampleIndex - samplesAvailable+7));
    if (samplesNeeded < 0)
    {
#ifdef USE_ANDROID
        __android_log_print(ANDROID_LOG_INFO, "SRC", "SamplesNeeded < 0: %f %d %d %f\n",pitchCoeff,bufferLen,ibuffer_write,sampleIndex);
#endif

        samplesNeeded = 0;
    }
    return samplesNeeded;
}

int SamplingRateConverter::Process(float* buffer, int inBufferLen, int outBufferLen,float pitchCoeff)
{
    ratio = pitchCoeff;
    
    for (int k=0; k < inBufferLen; k++)
    {
        ibuffer[ibuffer_write++] = buffer[k];
        if (ibuffer_write >= VSRC_BUFFER_LENGTH)
            ibuffer_write = 0;
    }
    
    //int rbuflen =(int)((double)outBufferLen * ratio);
    double invRatio = 1.0 / ratio;
    const long offset = 2;
    double rem = 0.0;
    double qbuffer[6];
    
    for (int i = 0; i < outBufferLen; i++)
    {
        
        
        
        int index = (int)floor(sampleIndex);
        rem = sampleIndex - (double)index;
        if (index >= VSRC_BUFFER_LENGTH)
            index = 0;
        sampleIndex = (double)index + rem;
        
        for (int k=0; k < 6; k++)
        {
            if (index >= VSRC_BUFFER_LENGTH)
                index = 0;
            qbuffer[k] = ibuffer[index++];
        }
        
        float z = rem - 0.5;
        float even1 = qbuffer[offset+1]+qbuffer[offset+0], odd1 = qbuffer[offset+1]-qbuffer[offset+0];
        float even2 = qbuffer[offset+2]+qbuffer[offset-1], odd2 = qbuffer[offset+2]-qbuffer[offset-1];
        float even3 = qbuffer[offset+3]+qbuffer[offset-2], odd3 = qbuffer[offset+3]-qbuffer[offset-2];
        
        float c0 = even1*0.42685983409379380 + even2*0.07238123511170030
        + even3*0.00075893079450573;
        float c1 = odd1*0.35831772348893259 + odd2*0.20451644554758297
        + odd3*0.00562658797241955;
        float c2 = even1*-0.217009177221292431 + even2*0.20051376594086157
        + even3*0.01649541128040211;
        float c3 = odd1*-0.25112715343740988 + odd2*0.04223025992200458
        + odd3*0.02488727472995134;
        float c4 = even1*0.04166946673533273 + even2*-0.06250420114356986
        + even3*0.02083473440841799;
        float c5 = odd1*0.08349799235675044 + odd2*-0.04174912841630993
        + odd3*0.00834987866042734;
        
       buffer[i]  =((((c5*z+c4)*z+c3)*z+c2)*z+c1)*z+c0;
        
        //if ((buffer[0][i]-mem)*(buffer[0][i]-mem) > 0.01)
        //{
        //    std::cout << "i:" << i << "\n";
        //}
        mem = buffer[i];
        

        
        sampleIndex += invRatio;
        
    }
    

    return outBufferLen;

}

void SamplingRateConverter::Reset()
{
    memset(ibuffer, 0, VSRC_BUFFER_LENGTH * sizeof(float));
    ibuffer_read = 0;
    ibuffer_write = 6;
    sampleIndex = 0.0;
    ratio = 1.0;

}