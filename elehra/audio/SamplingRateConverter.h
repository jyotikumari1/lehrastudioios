//
//  SamplingRateConverter.h
//  eLehra
//
//  Created by Jukka Rauhala on 06/05/15.
//  Copyright (c) 2015 AK Studios. All rights reserved.
//

#ifndef __eLehra__SamplingRateConverter__
#define __eLehra__SamplingRateConverter__

#include <stdio.h>

#define VSRC_BUFFER_LENGTH   30000//8192*3

class SamplingRateConverter
{
    
public:
    SamplingRateConverter();
    ~SamplingRateConverter(){}
    
    int GetInputFrameSize(float pitchCoeff, int bufferLen);
    
    int Process(float* buffer, int inBufferLen, int outBufferLen,float pitchCoeff);
    void Reset();
    
private:
    float* ibuffer;
    int ibuffer_read;// = 0;
    int ibuffer_write;// = 0;
    bool bypass;// = true;
    float ratio;
    float sampleIndex;
    
    float mem;// = 0;

    
};
#endif /* defined(__eLehra__SamplingRateConverter__) */
