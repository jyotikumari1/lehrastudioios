//
//  TimeStretch.cpp
//  eLehra
//
//  Created by Jukka Rauhala on 06/05/15.
//  Copyright (c) 2015 AK Studios. All rights reserved.
//

#include "TimeStretcher.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <string.h>
#include <iostream>

#define PI 3.141592653589793
#define ONE_PER_2PI   0.1591549430919
#define ONE_PER_TS_FRAME_LEN_066    0.000322265625
#define PI_PER_2    1.5707963267949
#define ONE_PER_TS_FRAME_LEN 0.00048828
//#define angle(x) (atan2(x.i/x.r))
#define absri(x) (sqrtf(x.i*x.i + x.r*x.r))


double TimeStretcher::angle(kiss_fft_cpx value)
{
    double yx = value.i / value.r;
    
    
    double anglex = 0.0;
    if (value.r > 0)
    {
        return atanf(yx);//anglex = atan(yx);//return atan(yx);
    } else if (value.r == 0)
    {
        if ( value.i < 0)
            return -0.5*PI;
        else if (value.i > 0)
            return 0.5*PI;
        else
            return 0.0;
    } else {
        if (value.i < 0)
            return atanf(yx) - PI;
        else
            return atanf(yx) + PI;
    }
    
    /*    double anglex = 0.0;
     if (value.r > 0)
     {
     return atan(yx);//anglex = atan(yx);//return atan(yx);
     }
     else if (value.r == 0 && value.i < 0)
     return -0.5*PI;
     else if (value.r == 0 && value.i > 0)
     return 0.5*PI;
     else if (value.r == 0 && value.i == 0)
     return 0.0;
     else if (value.i < 0)
     anglex = atan(yx) - PI;
     else
     anglex = atan(yx) + PI;
     */
    //anglex = anglex - 2.0 * PI * round(anglex /(2.0*PI));
    return anglex;
}


TimeStretcher::TimeStretcher()
{
    
    //fs = fs_;
    //this->ratio = 1.0;
    this->buflen = TS_FRAME_LEN;
    this->hopsize = TS_HOP;
    
    
    useAngleBuffer = false;
    
    this->intbuffer1 = new kiss_fft_cpx*[TS_BUFFERS];
    
    angleBuffer = new float*[TS_BUFFERS];
    
    this->intbuffer2 = new kiss_fft_cpx*[TS_BUFFERS];
    int len =TS_BUFFERS;//(int)ceil(4.0 / ratio);
    for (int i=0; i < len; i++)
    {
        intbuffer2[i] = new kiss_fft_cpx[TS_FRAME_LEN+1];
        angleBuffer[i] = new float[1+TS_FRAME_LEN/2];
        intbuffer1[i] = new kiss_fft_cpx[TS_FRAME_LEN+1];
    }
    
    /*for (int i=0; i< 5; i++)
     {
     memset(intbuffer1[i],0,sizeof(double)*2049);
     }*/
    
    rbuffer_index = 0;
    rbuffer_start = -TS_HOPS - 1;
    outbuffer_len = 8192*2;//(int)ceil(4096.0/ratio);
    intbuffer3 = new float[outbuffer_len];
    //memset(intbuffer3,0,sizeof(double)*4096);
    intbuffer4 = new float[outbuffer_len];
    //memset(intbuffer4,0,sizeof(double)*9192);
    
    //this->timevec = new double[TS_FRAME_LEN];
    this->win = new double[TS_FRAME_LEN];
    
    
    for (int i = 0; i < TS_FRAME_LEN; i++)
    {
        win[i] = 0.5 * (1.0 - cos(2.0 * PI * (double)i / ((double)TS_FRAME_LEN - 1.0)));
    }
    
    cfg = kiss_fft_alloc(TS_FRAME_LEN, 0, 0, 0);
    icfg = kiss_fftr_alloc(TS_FRAME_LEN, 1, 0, 0);
    
    txx = 0;
    Initialize();
}

TimeStretcher::~TimeStretcher()
{
    free(cfg);
    free(icfg);
    
}

void TimeStretcher::Initialize()
{
    rbuffer_index = 0;
    rbuffer_start = -TS_BUFFERS -1;
    txx = 0;
    for (int i=0; i< TS_BUFFERS; i++)
    {
        memset(intbuffer2[i],0,sizeof(float)*(TS_FRAME_LEN+1));
        memset(intbuffer1[i],0,sizeof(float)*(TS_FRAME_LEN+1));
        memset(angleBuffer[i],0,sizeof(float)*(1+TS_FRAME_LEN/2));
    }
    memset(intbuffer3,0,sizeof(float)*outbuffer_len);
    memset(intbuffer4,0,sizeof(float)*outbuffer_len);
    samplesAvailableOut = 0;
    samplesAvailableIn = TS_FRAME_LEN;

    is_first_frame = true;
    buffers_available = 0;
    rbuffer_end_counter = 0;
    rbuffer_end_i = 0;
    rbuffer_end_t = 0;
    rbuffer_start_i = 0;//TS_BUFFERS - 1;
    rbuffer_start_t = 0;
    int buffers = stft(intbuffer3, intbuffer1, buflen, buflen, hopsize);
    
}

int TimeStretcher::Process(float* buffer, int inBufferLen, int outBufferLen, float tsCoeff)
{
   // if (bypass)
   //     return bufferLen;
    
    if (inBufferLen > 0)//if (samplesAvailableIn < buflen)
    {
        //memset(intbuffer3 + buflen + samplesAvailableIn,0,sizeof(float)*inBufferLen);
        memcpy(intbuffer3 + samplesAvailableIn,buffer,sizeof(float)*inBufferLen);
        samplesAvailableIn+= inBufferLen;
    }
    
    if (samplesAvailableIn >= TS_FRAME_LEN)
    {
        
        int buffers = stft(intbuffer3, intbuffer1, buflen, buflen, hopsize);
        memmove(intbuffer3,intbuffer3 + buffers*TS_HOP,sizeof(float)*(outbuffer_len-buffers*TS_HOP));
    }
    
    //if (buffers_available > 0)
    {
        int buffers = pvsample(intbuffer1, intbuffer2, timevec, hopsize, tsCoeff);
        int samples = istft(intbuffer2, intbuffer4,  buffers);
        //samples = TS_FRAME_LEN;
        /*for (int k=0; k < bufferLen; k++)
         {
         double temp = intbuffer4[k];
         //if (buffer[k] != temp)
         {
         int z = 0;
         // std::cout << "in:" << buffer[k] << " out:" << temp << "\n";
         z = 0;
         }
         //buffer[k] = temp;//buf[k].r;// *win[k]*2/3;
         }*/
        samplesAvailableOut += samples;
      //  samplesAvailableIn -= buflen;
        
        
        
    }
    
    if (samplesAvailableOut >= outBufferLen)
    {
        //std::cout << "samples out:" << samplesAvailableOut << " samples in:" << samplesAvailableIn <<"\n";
        
        memcpy(buffer,intbuffer4,sizeof(float)*outBufferLen);
        memmove(intbuffer4,intbuffer4 + outBufferLen,sizeof(float)*(outbuffer_len-outBufferLen));//(outbuffer_len-samples));
        memset(intbuffer4+(outbuffer_len-outBufferLen),0,sizeof(float)*outBufferLen);//(outbuffer_len-samples));
        samplesAvailableOut -= outBufferLen;
    } else {
        //std::cout << "samples zero:" << samplesAvailableOut << " samples in:" << samplesAvailableIn <<"\n";
        memset(buffer,0,sizeof(float)*outBufferLen);
    }
    /*

    float bi1[TS_FRAME_LEN];
    float bi2[TS_FRAME_LEN];
    float bi3[TS_FRAME_LEN];
    float bi4[TS_FRAME_LEN];
    float bi5[TS_FRAME_LEN];
    float bi6[TS_FRAME_LEN];
    float bi7[TS_FRAME_LEN];
    float bi8[TS_FRAME_LEN];
    
    float bin[4096];
    memcpy(&bin[0],intbuffer3,sizeof(float)*4096);
    float bout[4096];
    memcpy(&bout[0],intbuffer4,sizeof(float)*4096);
    
    float bx1[TS_FRAME_LEN];
    float bx2[TS_FRAME_LEN];
    float bx3[TS_FRAME_LEN];
    float bx4[TS_FRAME_LEN];
    float bx5[TS_FRAME_LEN];
    float bx6[TS_FRAME_LEN];
    float bx7[TS_FRAME_LEN];
    float bx8[TS_FRAME_LEN];
    
    memcpy(&bi1[0],intbuffer1[0],sizeof(float)*TS_FRAME_LEN);
    memcpy(&bi2[0],intbuffer1[1],sizeof(float)*TS_FRAME_LEN);
    memcpy(&bi3[0],intbuffer1[2],sizeof(float)*TS_FRAME_LEN);
    memcpy(&bi4[0],intbuffer1[3],sizeof(float)*TS_FRAME_LEN);
    memcpy(&bi5[0],intbuffer1[4],sizeof(float)*TS_FRAME_LEN);
    memcpy(&bi6[0],intbuffer1[5],sizeof(float)*TS_FRAME_LEN);
    memcpy(&bi7[0],intbuffer1[6],sizeof(float)*TS_FRAME_LEN);
    memcpy(&bi8[0],intbuffer1[7],sizeof(float)*TS_FRAME_LEN);
    
    memcpy(&bx1[0],intbuffer2[0],sizeof(float)*TS_FRAME_LEN);
    memcpy(&bx2[0],intbuffer2[1],sizeof(float)*TS_FRAME_LEN);
    memcpy(&bx3[0],intbuffer2[2],sizeof(float)*TS_FRAME_LEN);
    memcpy(&bx4[0],intbuffer2[3],sizeof(float)*TS_FRAME_LEN);
    memcpy(&bx5[0],intbuffer2[4],sizeof(float)*TS_FRAME_LEN);
    memcpy(&bx6[0],intbuffer2[5],sizeof(float)*TS_FRAME_LEN);
    memcpy(&bx7[0],intbuffer2[6],sizeof(float)*TS_FRAME_LEN);
    memcpy(&bx8[0],intbuffer2[7],sizeof(float)*TS_FRAME_LEN);
 */
    
#ifdef _DEBUG
    for (int ii=0; ii< bufferLen; ii++)
    {
        if (buffer[0][ii] < -2.0 || buffer[0][ii] > 2.0)
        {
            int ttt = 0;
        }
    }
#endif
    
    return samplesOut;
}

int TimeStretcher::pvsample(kiss_fft_cpx **buffer_in, kiss_fft_cpx **buffer_out,double* tvec, int hop, float ratio)
{
    
    //int N = buflen;
    double dphi[1+TS_FRAME_LEN/2];
    //kiss_fft_cpx din[TS_FRAME_LEN];
    //kiss_fft_cpx dout[TS_FRAME_LEN];
    
    dphi[0] = 0;
    for (int i=1; i < 1+TS_FRAME_LEN/2; i++)
    {
        dphi[i] = (double)i * 2.0f*PI*TS_HOP / TS_FRAME_LEN;//(double)i * PI_PER_2;//2.0 * PI * 512.0 * (double)i * ONE_PER_TS_FRAME_LEN;///((double)N/(double)i);
    }
    
    if (is_first_frame)
    {
        int tt1 = rbuffer_start_i;// + tt;
        if (tt1 >= TS_BUFFERS)
            tt1 = 0;
        
        int tt2 = tt1+1;
        if (tt2 >= TS_BUFFERS)
            tt2 = 0;
        
        //double tf = txx;// - (double)tt;
        
        ph[0] =angle(buffer_in[tt1][0]);
        for (int i=1; i < 1+TS_FRAME_LEN/2; i++)
        {
            ph[i] = angle(buffer_in[tt1][i]);
        }
        is_first_frame = false;
    }
    
    int i=0;
    while(rbuffer_start_t + txx < rbuffer_end_t )//&& i < TS_HOPS)//rbuffer_start+TS_HOPS)//for (int i=0; i < 4; i++)
    {
        if (txx > 1.0)
        {
            rbuffer_start_t+=1;
            rbuffer_start_i+=1;
            if (rbuffer_start_i >= TS_BUFFERS)
                rbuffer_start_i -= TS_BUFFERS;
            txx -= 1;
            
        }
        //int tt = (int)floor(txx);
        
        //int tt1 = tt-rbuffer_start;
        //if (tt1 >= 5)
        //    tt1 -= 5;
        /*long tt1 = tt;
        double tt1x = 0.2*(double)tt1;
        double temp2x = tt1x - floor(tt1x);
        if (tt > TS_HOPS)
        {
            tt1 = (int)roundf(temp2x*5.0);
        }
        //tt1--;
        long tt2 = tt1+1;
        if (tt2 >= TS_HOPS + 1)
            tt2 -= TS_HOPS+1;*/
        
        int tt1 = rbuffer_start_i;// + tt;
        if (tt1 >= TS_BUFFERS)
            tt1 = 0;
        
        int tt2 = tt1+1;
        if (tt2 >= TS_BUFFERS)
            tt2 = 0;
        
        double tf = txx;// - (double)tt;
        
        double angle1 = 0;
        double angle2 = 0;
        //std::cout << "tt1:" << tt1 << " tt2:" << tt2 << " txx:" << txx << " rbs:" << rbuffer_start_t + txx<< " i:" << i <<"\n";
        for (int k=0; k < 1+TS_FRAME_LEN/2;k++)
        {
            double bmag = (1.0 - tf) * absri(buffer_in[tt1][k]) + tf * absri(buffer_in[tt2][k]);
            if (useAngleBuffer)
            {
                angle1 = angleBuffer[tt1][k];
                angle2 = angleBuffer[tt2][k];
                
            } else {
                angle1 =angle(buffer_in[tt1][k]);
                angle2 = angle(buffer_in[tt2][k]);
            }
            double dp = angle2 - angle1 - dphi[k];
            dp = dp - 2.0 * PI * roundf(dp * ONE_PER_2PI);///(2.0*PI));
            
            /* double dpold = angle(buffer_in[tt1][k]);
             double dpold2 = angle(buffer_in[tt2][k]);
             kiss_fft_cpx temp = buffer_in[tt1][k];
             double bmagold = absri(temp);*/
            
            buffer_out[i][k].r =  bmag * sinf(PI_PER_2 - ph[k]);//cos(ph[k]);//exp(ph[k]*j);
            buffer_out[i][k].i =  bmag * sinf(ph[k]);//exp(ph[k]*j);
            if (k > 0)
            {
                buffer_out[i][TS_FRAME_LEN-k].r =buffer_out[i][k].r;
                buffer_out[i][TS_FRAME_LEN-k].i = -buffer_out[i][k].i;
            }
            // kiss_fft_cpx temp2 = buffer_out[i][k];
            //std::cout << "temp.r:" << temp.r << " temp.i:" << temp.i << "temp2.r:" << temp2.r << " temp2.i:" << temp2.i<< "\n";
            
            /* if ((temp.r-temp2.r)*(temp.r-temp2.r) > 0.01 || (temp.i-temp2.i)*(temp.i-temp2.i) > 0.01)
             {
             double dpnew = ph[k];
             
             int iii= 0;
             iii= 2;
             
             }
             double oldph = ph[k];*/
            ph[k] = ph[k] + dphi[k] + dp;
            
            while (ph[k] > 2*PI)
            {
                ph[k] -= 2*PI;
            }
            //std::cout << "ph[k]:" << ph[k] << " dpold:" << dpold << " dpold2:" << dpold2 <<  " dp:" << dp << " dphi[k]" << dphi[k] << " oldph:" << oldph <<"\n";
            
        }
        
        /* for (int k=0; k < TS_FRAME_LEN; k++)
         {
         //buffer_out[i][k] = buffer_in[tt1][k];
         kiss_fft_cpx temp2 = buffer_out[i][k];
         kiss_fft_cpx temp = buffer_in[tt1][k];
         din[k] = buffer_in[tt1][k];
         dout[k] = buffer_out[i][k];
         if ((temp.r-temp2.r)*(temp.r-temp2.r) > 0.01 || (temp.i-temp2.i)*(temp.i-temp2.i) > 0.01)
         {
         //double dpnew = ph[k];
         
         int iii= 0;
         iii= 2;
         
         }
         }*/
        
        txx += ratio;
        buffers_available--;
        i++;
        //std::cout << "txx:" << txx << " ratio:"<<ratio<<"\n";
        if (txx >= 1)
        {
            int step = floor(txx);
            while (step > 0 && rbuffer_start_t+step > rbuffer_end_t)
            {
                step--;
            }
            rbuffer_start_t+=step;
            rbuffer_start_i+=step;
            if (rbuffer_start_i >= TS_BUFFERS)
                rbuffer_start_i -= TS_BUFFERS;
            txx -= (float)step;
            if (rbuffer_end_i >= rbuffer_start_i)
            {
                if (rbuffer_end_t - rbuffer_start_t != rbuffer_end_i - rbuffer_start_i)
                {
                    std::cout << "INDEX PROBLEM \n";
                }
            } else {
                if (rbuffer_end_t - rbuffer_start_t !=  TS_BUFFERS - rbuffer_start_i + rbuffer_end_i)
                {
                    std::cout << "INDEX PROBLEM \n";
                }
                
            }
        }
    }
    //std::cout << "pvsample i:" << i<< "\n";

    return i;
    /*
     [rows,cols] = size(b);
     
     N = 2*(rows-1);
     
     if hop == 0
     % default value
     hop = N/2;
     end
     
     % Empty output array
     c = zeros(rows, length(t));
     
     % Expected phase advance in each bin
     dphi = zeros(1,N/2+1);
     dphi(2:(1 + N/2)) = (2*pi*hop)./(N./(1:(N/2)));
     
     % Phase accumulator
     % Preset to phase of first frame for perfect reconstruction
     % in case of 1:1 time scaling
     ph = angle(b(:,1));
     
     % Append a 'safety' column on to the end of b to avoid problems
     % taking *exactly* the last frame (i.e. 1*b(:,cols)+0*b(:,cols+1))
     b = [b,zeros(rows,1)];
     
     ocol = 1;
     for tt = t
     % Grab the two columns of b
     bcols = b(:,floor(tt)+[1 2]);
     tf = tt - floor(tt);
     bmag = (1-tf)*abs(bcols(:,1)) + tf*(abs(bcols(:,2)));
     % calculate phase advance
     dp = angle(bcols(:,2)) - angle(bcols(:,1)) - dphi';
     % Reduce to -pi:pi range
     dp = dp - 2 * pi * round(dp/(2*pi));
     % Save the column
     c(:,ocol) = bmag .* exp(j*ph);
     % Cumulate phase, ready for next frame
     ph = ph + dphi' + dp;
     ocol = ocol+1;
     end*/
}

int TimeStretcher::stft(float *buffer_in, kiss_fft_cpx **buffer_out, int fftlen, int winlen, int hop)
{
    double temp;
    kiss_fft_cpx buf[TS_FRAME_LEN];
    int start = TS_HOP;
    int i=0;
    int samples = 0;
    while (samplesAvailableIn >= TS_FRAME_LEN)//for(int i=0; i < TS_HOPS; i++)
    {
        start = 0 + i*TS_HOP;
        i++;
        for (int k=0; k < TS_FRAME_LEN; k++)
        {
            temp = buffer_in[start + k] * win[k];
            buf[k].i = 0.0;
            buf[k].r = temp;
            /*if (isnan(temp))
             {
             int z = 0;
             z = 0;
             }*/
        }
        
        kiss_fft(cfg,buf,buffer_out[rbuffer_index]);
        
        if (useAngleBuffer)
        {
            for (int kk=0;kk<1+TS_FRAME_LEN/2;kk++)
            {
                angleBuffer[rbuffer_index][kk] = angle(buffer_out[rbuffer_index][kk]);
            }
        }
        
        // std::cout << "R.i[0]:" << buffer_out[rbuffer_index][0].r << "R.i[2047]:" << buffer_out[rbuffer_index][2047].r << "\n";
        rbuffer_end_t = rbuffer_end_counter;
        rbuffer_end_i = rbuffer_index;
        //std::cout << "stft i:" << i<< " rbs:"<< rbuffer_end_t << " rbi:" << rbuffer_index <<"\n";
        rbuffer_end_counter++;

        buffers_available++;
        if (++rbuffer_index >= TS_BUFFERS)
        {
            rbuffer_index = 0;
        }
        
        if (rbuffer_end_i >= rbuffer_start_i)
        {
            if (rbuffer_end_t - rbuffer_start_t != rbuffer_end_i - rbuffer_start_i)
            {
                std::cout << "INDEX PROBLEM \n";
            }
        } else {
            if (rbuffer_end_t - rbuffer_start_t !=  TS_BUFFERS - rbuffer_start_i + rbuffer_end_i)
            {
                std::cout << "INDEX PROBLEM \n";
            }
            
        }
        
        
        rbuffer_start++;
        samplesAvailableIn -= TS_HOP;
        
    }
    //rbuffer_start += TS_HOPS;
    
    /*for b = 0:h:(s-f)
     u = win.*x((b+1):(b+f));
     t = fft(u);
     d(:,c) = t(1:(1+f/2))';
     c = c+1;
     end;*/
    return i;
}

int TimeStretcher::istft(kiss_fft_cpx **buffer_in, float *buffer_out, int buffers_in)
{
    kiss_fft_scalar buf[TS_FRAME_LEN];
    int start = 0;
    
    /* double buffer1[TS_FRAME_LEN];
     double buffer2[TS_FRAME_LEN];
     double buffer3[TS_FRAME_LEN];
     double buffer4[TS_FRAME_LEN];
     
     double *bufferz = &buffer1[0];*/
    
    for(int i=0; i < buffers_in; i++)
    {
        
        kiss_fftri(icfg,buffer_in[i],buf);
        start = samplesAvailableOut + i*TS_HOP;
        
        float energy = 0;
        
        // std::cout << "i:" << i << " start:" << start  <<"\n";
        
        for (int k=0; k < TS_FRAME_LEN; k++)
        {
            // bufferz[k] = buf[k].r *win[k]* 0.66 / TS_FRAME_LEN.0;
            buffer_out[start+k] += buf[k] *win[k] * ONE_PER_TS_FRAME_LEN_066;//*0.66 *win[k]/ TS_FRAME_LEN.0;//;
            /*if (fabs(buffer_out[start+k]-samplemem)> 0.1 && k> 0)
            {
                 std::cout << "i:" << i << " start:" << rbuffer_start_t  <<" k:" << k <<"\n";
                
            }*/
            samplemem = buffer_out[start+k];
            energy += samplemem*samplemem;
        }
        
        if (energy < 0.1)
        {
            //std::cout << "energy:" << energy << "\n";
           
        }

        /*  switch(i)
         {
         case 0:
         bufferz = &buffer2[0];
         break;
         case 1:
         bufferz = &buffer3[0];
         break;
         case 2:
         bufferz = &buffer4[0];
         break;
         }*/
    }
   // std::cout << "buffers_in:" << buffers_in << "\n";

    return buffers_in * TS_HOP;
    
    /* win = 2/3*win;
     xlen = ftsize + (cols-1)*h;
     x = zeros(1,xlen);
     
     for b = 0:h:(h*(cols-1))
     ft = d(:,1+b/h)';
     ft = [ft, conj(ft([((ftsize/2)):-1:2]))];
     px = real(ifft(ft));
     x((b+1):(b+ftsize)) = x((b+1):(b+ftsize))+px.*win;
     end;*/
}
int TimeStretcher::GetInputFrameSize(float tsCoeff, int blen)
{
   // float targetBufferSize = tsCoeff * (float)blen;
    
    float samplesNeeded = (float)(blen - samplesAvailableOut);
    
    int  framesOut = (int)ceil(samplesNeeded / (float) TS_HOP);
    int framesIn = (int)ceil(framesOut * tsCoeff);// - buffers_available;
    
    int samplesNeededIn =  framesIn * TS_HOP - samplesAvailableIn + TS_FRAME_LEN-TS_HOP;
    if (samplesNeededIn + samplesAvailableIn < buflen && framesIn > 0)
        samplesNeededIn = buflen - samplesAvailableIn;
    if (samplesNeededIn < 0)
        samplesNeededIn = 0;
    //std::cout << "Samples needed:" << samplesNeededIn << " (blen:" << blen <<" sout:" << samplesAvailableOut << " sin:"<<samplesAvailableIn << "\n";
    
    return samplesNeededIn;
}


