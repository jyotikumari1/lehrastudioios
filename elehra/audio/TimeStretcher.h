//
//  TimeStretch.h
//  eLehra
//
//  Created by Jukka Rauhala on 06/05/15.
//  Copyright (c) 2015 AK Studios. All rights reserved.
//

#ifndef __eLehra__TimeStretch__
#define __eLehra__TimeStretch__

#include <stdio.h>
#include "../KissFFT/kiss_fft.h"
#include "../KissFFT/kiss_fftr.h"

#define TS_FRAME_LEN    512//512
#define TS_HOP           128//128//64
#define TS_BUFFERS        60
#define TS_HOPS           TS_FRAME_LEN / TS_HOP

class TimeStretcher
{
    
public:
    TimeStretcher();
    ~TimeStretcher();
    
    int GetInputFrameSize(float tsCoeff, int bufferLen);
    
    
    int Process(float* buffer, int inBufferLen, int outBufferLen, float tsCoeff);
    void Initialize();
private:
    
    int pvsample(kiss_fft_cpx **buffer_in, kiss_fft_cpx **buffer_out,double* tvec, int hop, float ratio);
    int stft(float *buffer_in, kiss_fft_cpx **buffer_out, int fftlen, int winlen, int hop);
    int istft(kiss_fft_cpx **buffer_in, float *buffer_out, int buffers_in);
    double angle(kiss_fft_cpx value);

    double total_ratio;
    int buflen, hopsize;
    // double fs = 44100;
    
    kiss_fft_cpx** intbuffer1;
    kiss_fft_cpx** intbuffer2;
    float** angleBuffer;
    bool useAngleBuffer;
    float* intbuffer3;
    float* intbuffer4;
    //double intbuffer5[4096];
    double* timevec;
    double* win;
    kiss_fft_cfg cfg;
    kiss_fftr_cfg icfg;
    double txx;
    float ph[257];
    int rbuffer_start;
    int rbuffer_index;
    
    int outbuffer_len;
    bool is_first_frame = true;
    bool bypass = true;
    int samplesAvailableIn = 0;
    int samplesAvailableOut = 0;
    int samplesOut=512;
    int samplesBeforeSconv = 512;

    long rbuffer_start_t, rbuffer_end_t;
    int rbuffer_start_i, rbuffer_end_i;
    long rbuffer_end_counter;
    int buffers_available = 0;
    
    float samplemem = 0;
};
#endif /* defined(__eLehra__TimeStretch__) */
