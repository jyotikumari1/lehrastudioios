//
//  TimeStretch.cpp
//  eLehra
//
//  Created by Jukka Rauhala on 06/05/15.
//  Copyright (c) 2015 AK Studios. All rights reserved.
//

#include "TimeStretcherPL.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <string.h>
#include <iostream>

#define USE_TRANSIENT_DETECTION 1
#define TRANSIENT_THRESHOLD 0.00035f
#define TRANSIENT_GAIN  1.0f
#define TRANSIENT_FRAMES 30

#define PI2 6.28318530717959
#define PI20 62.8318530717959
#define PI200 628.318530717959
#define PI2000 6283.18530717959


#define PI 3.141592653589793
#define ONE_PER_2PI   0.1591549430919
#define ONE_PER_TS_FRAME_LEN_066    0.000252265625
#define PI_PER_2    1.5707963267949
#define ONE_PER_TS_FRAME_LEN 0.00048828
//#define angle(x) (atan2(x.i/x.r))
#define absri(x) (sqrtf(x.i*x.i + x.r*x.r))

#define SIN_TABLE_SIZE  512
float sint[SIN_TABLE_SIZE*2];

#define ATAN_TABLE_SIZE  512
float atant[ATAN_TABLE_SIZE*2];

inline float TimeStretcherPL::atanf2(float x)
{
   // float x2 = x;
    //float y = atanf(x);
    
    float s = 1.0f;
    float s2 = 1.0f;
    float c = 0.0f;
    
    if (x < 0)
    {
        s = -1.0f;
        x = -x;
    }
    
    if (x > 1.0f)
    {
        c = PI_PER_2;
        x = 1.0f / x;
        s2 = -1.0f;
    }
    
    int i = (int)(x * (float)ATAN_TABLE_SIZE);
    float y2 = s*(c +atant[i] * s2);
    
    return y2;
}

//double maxa = 0.0f;
//double mina = 0.0f;


inline double TimeStretcherPL::angle(double rvalue, double ivalue)//kiss_fft_cpx value)
{
    double yx = ivalue / rvalue;//value.i / value.r;
    
   /* if (value.r != 0 && yx < mina)
        mina = yx;
    if (value.r != 0 && yx > maxa)
        maxa = yx;
    */
    
    double anglex = 0.0;
    if (rvalue > 0)
    {
        return atanf2(yx);//anglex = atan(yx);//return atan(yx);
    } else if (rvalue== 0)
    {
        if ( ivalue < 0)
            return -PI_PER_2;
        else if (ivalue > 0)
            return PI_PER_2;
        else
            return 0.0;
    } else {
        if (ivalue < 0)
            return atanf2(yx) - PI;
        else
            return atanf2(yx) + PI;
    }
    
    /*    double anglex = 0.0;
     if (value.r > 0)
     {
     return atan(yx);//anglex = atan(yx);//return atan(yx);
     }
     else if (value.r == 0 && value.i < 0)
     return -0.5*PI;
     else if (value.r == 0 && value.i > 0)
     return 0.5*PI;
     else if (value.r == 0 && value.i == 0)
     return 0.0;
     else if (value.i < 0)
     anglex = atan(yx) - PI;
     else
     anglex = atan(yx) + PI;
     */
    //anglex = anglex - 2.0 * PI * round(anglex /(2.0*PI));
    return anglex;
}


TimeStretcherPL::TimeStretcherPL()
{
    
    //fs = fs_;
    //this->ratio = 1.0;
    this->buflen = TS_FRAME_LEN;
    this->hopsize = TS_HOP;
    
    is_first_frame = true;
    bypass = true;
    samplesAvailableIn = 0;
    samplesAvailableOut = 0;
    samplesOut = 512;
    samplesBeforeSconv = 512;
    buffers_available = 0;
    samplemem = 0;
    stft_rem = 0;
    last_energy = 0;
    lastTransient = 30;
    
    useAngleBuffer = false;
    angleBuffer = new float*[TS_BUFFERS];
    
#ifdef USE_KISS_FFT
    this->intbuffer1 = new kiss_fft_cpx*[TS_BUFFERS];
    
    
    this->intbuffer2 = new kiss_fft_cpx*[TS_BUFFERS];
#endif
    
#ifdef USE_OOURA_FFT_WRAPPER
    this->intbuffer1 = new compbuffer[TS_BUFFERS];
    this->intbuffer2 = new compbuffer[TS_BUFFERS];
    fftWrapper = new audiofft::AudioFFT();
    fftWrapper->init(TS_FRAME_LEN);
#endif
    
    intbuffer5 = new fftvalue*[TS_BUFFERS];
    
    int len =TS_BUFFERS;//(int)ceil(4.0 / ratio);
    for (int i=0; i < len; i++)
    {
#ifdef USE_KISS_FFT
        intbuffer2[i] = new kiss_fft_cpx[TS_FRAME_LEN+1];
        intbuffer1[i] = new kiss_fft_cpx[TS_FRAME_LEN+1];
#endif
#ifdef USE_OOURA_FFT_WRAPPER
        intbuffer1[i].im = new float[TS_FRAME_LEN+1];
        intbuffer1[i].re = new float[TS_FRAME_LEN+1];
        intbuffer2[i].im = new float[TS_FRAME_LEN+1];
        intbuffer2[i].re = new float[TS_FRAME_LEN+1];
        
#endif
        
        intbuffer5[i] = new fftvalue[TS_FRAME_LEN+1];
        angleBuffer[i] = new float[TS_FRAME_LEN_2_1];
    }
    
    /*for (int i=0; i< 5; i++)
     {
     memset(intbuffer1[i],0,sizeof(double)*2049);
     }*/
    
    rbuffer_index = 0;
    rbuffer_start = -TS_HOPS - 1;
    outbuffer_len = 8192*8;//(int)ceil(4096.0/ratio);
    intbuffer3 = new float[outbuffer_len];
    //memset(intbuffer3,0,sizeof(double)*4096);
    intbuffer4 = new float[outbuffer_len];
    //memset(intbuffer4,0,sizeof(double)*9192);
    
    //this->timevec = new double[TS_FRAME_LEN];
    this->win = new double[TS_FRAME_LEN];
    
    
    for (int i = 0; i < TS_FRAME_LEN; i++)
    {
        win[i] = sin(PI * (double)i / ((double)TS_FRAME_LEN));
//        win[i] = 0.5 * (1.0 - cos(2.0 * PI * (double)i / ((double)TS_FRAME_LEN - 1.0)));
    }
#ifdef USE_KISS_FFT
    cfg = kiss_fft_alloc(TS_FRAME_LEN, 0, 0, 0);
    icfg = kiss_fftr_alloc(TS_FRAME_LEN, 1, 0, 0);
#endif
    txx = 0;
    
    
    int half = SIN_TABLE_SIZE/2;
    
    for (int i=0; i < SIN_TABLE_SIZE; i++)
    {
        sint[i] = sinf(PI * (float)(i - half) / (float)half);
    }
    for (int i=0; i < ATAN_TABLE_SIZE; i++)
    {
        atant[i] = atanf((float)(i+1) / (float)ATAN_TABLE_SIZE);
    }
    
    omega[0] = 0;
    for (int i=1; i < TS_FRAME_LEN_2_1; i++)
    {
        //dphi[i] = (double)i * 2.0f*PI*TS_HOP / TS_FRAME_LEN;//(double)i * PI_PER_2;//2.0 * PI * 512.0 * (double)i * ONE_PER_TS_FRAME_LEN;///((double)N/(double)i);
        omega[i] = (float)i * 2.0f*PI/ TS_FRAME_LEN;//(double)i * PI_PER_2;//2.0 * PI
    }

    
    Initialize();
}

TimeStretcherPL::~TimeStretcherPL()
{
#ifdef USE_KISS_FFT

    free(cfg);
    free(icfg);
#endif
}

void TimeStretcherPL::Initialize()
{
    rbuffer_index = 0;
    rbuffer_start = -TS_BUFFERS -1;
    txx = 0;
    for (int i=0; i< TS_BUFFERS; i++)
    {
#ifdef USE_KISS_FFT
        memset(intbuffer2[i],0,sizeof(kiss_fft_cpx)*(TS_FRAME_LEN+1));
        memset(intbuffer1[i],0,sizeof(kiss_fft_cpx)*(TS_FRAME_LEN+1));
#endif
        
#ifdef USE_OOURA_FFT_WRAPPER
        memset(intbuffer2[i].re,0,sizeof(float)*(TS_FRAME_LEN+1));
        memset(intbuffer1[i].re,0,sizeof(float)*(TS_FRAME_LEN+1));
        memset(intbuffer2[i].im,0,sizeof(float)*(TS_FRAME_LEN+1));
        memset(intbuffer1[i].im,0,sizeof(float)*(TS_FRAME_LEN+1));
        
#endif
        
        memset(angleBuffer[i],0,sizeof(float)*(TS_FRAME_LEN_2_1));
        memset(intbuffer5[i],0,sizeof(fftvalue)*(TS_FRAME_LEN_2_1));
    }
    memset(&transientBuffer[0],0,sizeof(int)*TS_BUFFERS);
    memset(&anaHopBuffer[0],0,sizeof(int)*TS_BUFFERS);
    memset(intbuffer3,0,sizeof(float)*outbuffer_len);
    memset(intbuffer4,0,sizeof(float)*outbuffer_len);
    samplesAvailableOut = 0;
    samplesAvailableIn = TS_FRAME_LEN;

    is_first_frame = true;
    buffers_available = 0;
    rbuffer_end_counter = 0;
    rbuffer_end_i = 0;
    rbuffer_end_t = 0;
    rbuffer_start_i = 0;//TS_BUFFERS - 1;
    rbuffer_start_t = 0;
    stft(intbuffer3, intbuffer1, buflen, buflen, hopsize, 1.0f);
    
}

int TimeStretcherPL::Process(float* buffer, int inBufferLen, int outBufferLen, float tsCoeff)
{
   // if (bypass)
   //     return bufferLen;
    
    if (inBufferLen > 0)//if (samplesAvailableIn < buflen)
    {
        //memset(intbuffer3 + buflen + samplesAvailableIn,0,sizeof(float)*inBufferLen);
        memcpy(intbuffer3 + samplesAvailableIn,buffer,sizeof(float)*inBufferLen);
        samplesAvailableIn+= inBufferLen;
    }
    
    while (samplesAvailableIn >= TS_FRAME_LEN)
    {
        
        int samples = stft(intbuffer3, intbuffer1, buflen, buflen, hopsize, tsCoeff);
        memmove(intbuffer3,intbuffer3 + samples,sizeof(float)*(outbuffer_len-samples));
    }
    
    //if (buffers_available > 0)
    {
        int buffers = pvsample(intbuffer1, intbuffer2, timevec, hopsize, tsCoeff);
        int samples = istft(intbuffer2, intbuffer4,  buffers);
        //samples = TS_FRAME_LEN;
        /*for (int k=0; k < bufferLen; k++)
         {
         double temp = intbuffer4[k];
         //if (buffer[k] != temp)
         {
         int z = 0;
         // std::cout << "in:" << buffer[k] << " out:" << temp << "\n";
         z = 0;
         }
         //buffer[k] = temp;//buf[k].r;// *win[k]*2/3;
         }*/
        samplesAvailableOut += samples;
      //  samplesAvailableIn -= buflen;
        
        
        
    }
    
    if (samplesAvailableOut >= outBufferLen)
    {
        //std::cout << "samples out:" << samplesAvailableOut << " samples in:" << samplesAvailableIn <<"\n";
        
        memcpy(buffer,intbuffer4,sizeof(float)*outBufferLen);
        memmove(intbuffer4,intbuffer4 + outBufferLen,sizeof(float)*(outbuffer_len-outBufferLen));//(outbuffer_len-samples));
        memset(intbuffer4+(outbuffer_len-outBufferLen),0,sizeof(float)*outBufferLen);//(outbuffer_len-samples));
        samplesAvailableOut -= outBufferLen;
    } else {
        std::cout << "samples zero:" << samplesAvailableOut << " samples in:" << samplesAvailableIn <<"\n";
        memset(buffer,0,sizeof(float)*outBufferLen);
    }
    
/*
    float bi1[TS_FRAME_LEN];
    float bi2[TS_FRAME_LEN];
    float bi3[TS_FRAME_LEN];
    float bi4[TS_FRAME_LEN];
    float bi5[TS_FRAME_LEN];
    float bi6[TS_FRAME_LEN];
    float bi7[TS_FRAME_LEN];
    float bi8[TS_FRAME_LEN];
    
    float bin[4096];
    memcpy(&bin[0],intbuffer3,sizeof(float)*4096);
    float bout[4096];
    memcpy(&bout[0],intbuffer4,sizeof(float)*4096);
    
    float bx1[TS_FRAME_LEN];
    float bx2[TS_FRAME_LEN];
    float bx3[TS_FRAME_LEN];
    float bx4[TS_FRAME_LEN];
    float bx5[TS_FRAME_LEN];
    float bx6[TS_FRAME_LEN];
    float bx7[TS_FRAME_LEN];
    float bx8[TS_FRAME_LEN];
    
    memcpy(&bi1[0],intbuffer1[0],sizeof(float)*TS_FRAME_LEN);
    memcpy(&bi2[0],intbuffer1[1],sizeof(float)*TS_FRAME_LEN);
    memcpy(&bi3[0],intbuffer1[2],sizeof(float)*TS_FRAME_LEN);
    memcpy(&bi4[0],intbuffer1[3],sizeof(float)*TS_FRAME_LEN);
    memcpy(&bi5[0],intbuffer1[4],sizeof(float)*TS_FRAME_LEN);
    memcpy(&bi6[0],intbuffer1[5],sizeof(float)*TS_FRAME_LEN);
    memcpy(&bi7[0],intbuffer1[6],sizeof(float)*TS_FRAME_LEN);
    memcpy(&bi8[0],intbuffer1[7],sizeof(float)*TS_FRAME_LEN);
    
    memcpy(&bx1[0],intbuffer2[0],sizeof(float)*TS_FRAME_LEN);
    memcpy(&bx2[0],intbuffer2[1],sizeof(float)*TS_FRAME_LEN);
    memcpy(&bx3[0],intbuffer2[2],sizeof(float)*TS_FRAME_LEN);
    memcpy(&bx4[0],intbuffer2[3],sizeof(float)*TS_FRAME_LEN);
    memcpy(&bx5[0],intbuffer2[4],sizeof(float)*TS_FRAME_LEN);
    memcpy(&bx6[0],intbuffer2[5],sizeof(float)*TS_FRAME_LEN);
    memcpy(&bx7[0],intbuffer2[6],sizeof(float)*TS_FRAME_LEN);
    memcpy(&bx8[0],intbuffer2[7],sizeof(float)*TS_FRAME_LEN);
 */
    
#ifdef _DEBUG
    for (int ii=0; ii< bufferLen; ii++)
    {
        if (buffer[0][ii] < -2.0 || buffer[0][ii] > 2.0)
        {
            int ttt = 0;
        }
    }
#endif
    
    return samplesOut;
}

#ifdef USE_KISS_FFT
int TimeStretcherPL::pvsample(kiss_fft_cpx **buffer_in, kiss_fft_cpx **buffer_out,double* tvec, int hop, float ratio)
#endif
#ifdef USE_OOURA_FFT_WRAPPER
    int TimeStretcherPL::pvsample(compbuffer *buffer_in, compbuffer *buffer_out,double* tvec, int hop, float ratio)
#endif
{
    
    //int N = buflen;
    //double dphi[TS_FRAME_LEN_2_1];
    //float omega[TS_FRAME_LEN_2_1];
    //kiss_fft_cpx din[TS_FRAME_LEN];
    //kiss_fft_cpx dout[TS_FRAME_LEN];
    
    /*omega[0] = 0;
    for (int i=1; i < TS_FRAME_LEN_2_1; i++)
    {
        //dphi[i] = (double)i * 2.0f*PI*TS_HOP / TS_FRAME_LEN;//(double)i * PI_PER_2;//2.0 * PI * 512.0 * (double)i * ONE_PER_TS_FRAME_LEN;///((double)N/(double)i);
                omega[i] = (float)i * 2.0f*PI/ TS_FRAME_LEN;//(double)i * PI_PER_2;//2.0 * PI
    }*/
    
    if (is_first_frame && buffers_available > 0)
    {
        int tt1 = rbuffer_start_i;// + tt;
        if (tt1 >= TS_BUFFERS)
            tt1 = 0;
        
        int tt2 = tt1+1;
        if (tt2 >= TS_BUFFERS)
            tt2 = 0;
        
        //double tf = txx;// - (double)tt;
        
#ifdef USE_KISS_FFT
#define xangle(k,t) angle(buffer_in[t][k].r,buffer_in[t][k].i)
#define buffer_in_r(t,k) buffer_in[t][k].r
#define buffer_in_i(t,k) buffer_in[t][k].i
#define buffer_out_r(t,k) buffer_out[t][k].r
#define buffer_out_i(t,k) buffer_out[t][k].i
        
#endif
#ifdef USE_OOURA_FFT_WRAPPER
#define xangle(k,t) angle(buffer_in[t].re[k],buffer_in[t].im[k])
#define buffer_in_r(t,k) buffer_in[t].re[k]
#define buffer_in_i(t,k) buffer_in[t].im[k]
#define buffer_out_r(t,k) buffer_out[t].re[k]
#define buffer_out_i(t,k) buffer_out[t].im[k]
#endif
        
        ph[0] =xangle(0,tt1);//angle(buffer_in[tt1][0].r,buffer_in[tt1][0].i);
        for (int i=1; i < TS_FRAME_LEN_2_1; i++)
        {
            ph[i] = xangle(i,tt1);//angle(buffer_in[tt1][i].r,buffer_in[tt1][i].i);
        }
        memcpy(&anaAngles[0],&ph[0],sizeof(float)*
               (TS_FRAME_LEN_2_1));
        memcpy(&synAngles[0],&ph[0],sizeof(float)*
               (TS_FRAME_LEN_2_1));
        is_first_frame = false;
        buffers_available--;
        rbuffer_start_i+=1;
        if (rbuffer_start_i >= TS_BUFFERS)
            rbuffer_start_i -= TS_BUFFERS;
    }
    
    int i=0;
    while(buffers_available > 0)//rbuffer_start_t + txx < rbuffer_end_t )//&& i < TS_HOPS)//rbuffer_start+TS_HOPS)//for (int i=0; i < 4; i++)
    {
        /*if (txx > 1.0)
        {
            rbuffer_start_t+=1;
            rbuffer_start_i+=1;
            if (rbuffer_start_i >= TS_BUFFERS)
                rbuffer_start_i -= TS_BUFFERS;
            txx -= 1;
            
        }*/
        
        
        
        //int tt = (int)floor(txx);
        
        //int tt1 = tt-rbuffer_start;
        //if (tt1 >= 5)
        //    tt1 -= 5;
        /*long tt1 = tt;
        double tt1x = 0.2*(double)tt1;
        double temp2x = tt1x - floor(tt1x);
        if (tt > TS_HOPS)
        {
            tt1 = (int)roundf(temp2x*5.0);
        }
        //tt1--;
        long tt2 = tt1+1;
        if (tt2 >= TS_HOPS + 1)
            tt2 -= TS_HOPS+1;*/
        
        //int tt1 = rbuffer_start_i;// + tt;
       // if (tt1 >= TS_BUFFERS)
        //    tt1 = 0;
        
       // int tt2 = tt1+1;
        //if (tt2 >= TS_BUFFERS)
        //    tt2 = 0;
        
      //  double tf = txx;// - (double)tt;
        
        double angle1 = 0;
       // double angle2 = 0;
        //std::cout << "tt1:" << tt1 << " tt2:" << tt2 << " txx:" << txx << " rbs:" << rbuffer_start_t + txx<< " i:" << i <<"\n";
        //#define PHASE_LOCKING 1
#ifdef PHASE_LOCKING
        // find peaks for phase-locking
        int prev_peaki = 0;
        int starti[TS_FRAME_LEN_2_1];
        int endi[TS_FRAME_LEN_2_1];
        int peaki[TS_FRAME_LEN_2_1];
        int peaks = 1;
        float spec[5+TS_FRAME_LEN/2];
        
        
        starti[0]=0;
        spec[0]=0;
        spec[1]=0;
        for (int k=0; k < TS_FRAME_LEN_2_1; k++)
        {

            spec[k+2] = absri(buffer_in[rbuffer_start_i][k]);
            starti[k] = k;
            endi[k] = k;
            peaki[k] = k;
        }
        spec[TS_FRAME_LEN_2_1]=0;
        spec[2+TS_FRAME_LEN/2]=0;

        
        int kk=2;
        while(kk < TS_FRAME_LEN/2 - 1)//for (int k=2; k < TS_FRAME_LEN_2_1; k++)
        {
            if (spec[kk-2]<spec[kk] && spec[kk-1]<spec[kk] && spec[kk+1]<spec[kk] && spec[kk+2]<spec[kk])
            {
                starti[peaks] = (kk+prev_peaki)/2;
                endi[peaks-1] = starti[peaks]-1;
                peaki[peaks] = kk-2;
                prev_peaki = kk-2;
                peaks++;
            }
            
                kk++;
            
        }
        endi[peaks-1] = TS_FRAME_LEN/2;

        
        
        
        
        //for (int k=0; k < TS_FRAME_LEN_2_1;k++)
        for (int kkk=0; kkk < peaks; kkk++)
        {
            int k =peaki[kkk];
            float dphi = omega[k] * anaHopBuffer[rbuffer_start_i];
            angle1 =angle(buffer_in[rbuffer_start_i][k].r,buffer_in[rbuffer_start_i][k].i);
            float hpi = angle1 - anaAngles[k] - dphi;
            hpi = hpi - 2.0f * PI * round(hpi/(2.0f*PI));
            float ipa_sample = (omega[k]+hpi / (float)anaHopBuffer[rbuffer_start_i]);
            float ipa_hop = ipa_sample * TS_HOP;
            
            // non phase-locking
            float theta = synAngles[k] + ipa_hop - angle1;
            
            
            float r1 = sinf(PI_PER_2 - theta);
            float img1 = sinf(theta);
            float r2 = buffer_in[rbuffer_start_i][k].r;
            float img2 = buffer_in[rbuffer_start_i][k].i;
            
            float r3 =r1*r2 - img1*img2;
            float img3 =r1*img2 + r2*img1;
            for (int ii=starti[kkk]; ii <= endi[kkk]; ii++)
            {
                float r2 = buffer_in[rbuffer_start_i][ii].r;
                float img2 = buffer_in[rbuffer_start_i][ii].i;
                
                float r3 =r1*r2 - img1*img2;
                float img3 =r1*img2 + r2*img1;
            buffer_out[i][ii].r = r3;//r1*r2 - img1*img2;
            buffer_out[i][ii].i = img3;//r1*img2 + r2*img1;
            if (ii > 0)
            {
                buffer_out[i][TS_FRAME_LEN-ii].r =buffer_out[i][ii].r;
                buffer_out[i][TS_FRAME_LEN-ii].i = -buffer_out[i][ii].i;
            }
            anaAngles[ii] = angle1;
            synAngles[ii] = angle(buffer_out[i][ii].r,buffer_out[i][ii].i);
            }
#else
            float* anaAnglesp = &anaAngles[0];
            float* synAnglesp = &synAngles[0];
            float* omegap = &omega[0];
            
            for (int k=0; k < TS_FRAME_LEN_2_1;k++)
            {
                float dphi = *omegap * anaHopBuffer[rbuffer_start_i];
                angle1 =xangle(k,rbuffer_start_i);//angle(buffer_in[rbuffer_start_i][k].r,buffer_in[rbuffer_start_i][k].i);
                float hpi = angle1 - *anaAnglesp - dphi;
//#define double2int(i, d) \
//{double t = ((d) + 6755399441055744.0); i = *((int *)(&t));}
               // int ii = 0;
                //double2int(ii,hpi*ONE_PER_2PI);
                //float rx1 =(float)ii;
                //float rx2 =roundf(hpi*ONE_PER_2PI);
                //float hpi2 = hpi;
                hpi = hpi - PI2 * roundf(hpi*ONE_PER_2PI);
                
                /*if (hpi > PI2000)
                    hpi -= PI2000;
                if (hpi > PI200)
                    hpi -= PI200;
                if (hpi > PI20)
                    hpi -= PI20;
                if (hpi > PI2)
                    hpi -= PI2;*/
                
                
                //hpi = hpi - 2.0f * PI * rx2;
                float ipa_sample = (*omegap++ +hpi / (float)anaHopBuffer[rbuffer_start_i]);
                float ipa_hop = ipa_sample * TS_HOP;
                
                // non phase-locking
                float theta = *synAnglesp + ipa_hop - angle1;
                theta = theta - PI2 * roundf(theta*ONE_PER_2PI);
                
                float indxf =(float)SIN_TABLE_SIZE*((theta + PI)*ONE_PER_2PI);
                long indx = (long)indxf;//roundf();
                float rem = indxf - (float)indx;
                indx = indx + SIN_TABLE_SIZE;
                indx = indx % SIN_TABLE_SIZE;

                if (indx < 0)
                    indx = 0;
                
                float img1 = 0;
                if (indx < SIN_TABLE_SIZE-1)
                {
                    img1 = (1.0f-rem)*sint[(int)indx] + rem*sint[(int)indx+1];
                } else {
                    img1 = 1.0f;
                }
                int indx2 = 5*SIN_TABLE_SIZE/4 - (int)indx;
                indx2 = indx2 % SIN_TABLE_SIZE;
                
                float r1 = 0;
                if (indx < SIN_TABLE_SIZE-1)
                {
                    r1 = (1.0f-rem)*sint[indx2] + rem*sint[indx2+1];
                } else {
                    r1 = 1.0f;
                }                //float r4 = img1b*r1;
                
                float r2 = buffer_in_r(rbuffer_start_i,k);//buffer_in[rbuffer_start_i][k].r;
                float img2 = buffer_in_i(rbuffer_start_i,k);//buffer_in[rbuffer_start_i][k].i;
                
                float r3 =r1*r2 - img1*img2;
                float img3 =r1*img2 + r2*img1;
#if USE_TRANSIENT_DETECTION
                if (transientBuffer[rbuffer_start_i])
                {
                    buffer_out_r(i,k) = r2*TRANSIENT_GAIN;//r1*r2 - img1*img2;
                    buffer_out_i(i,k) = img2*TRANSIENT_GAIN;//r1*img2 + r2*img1;
                    *synAnglesp++ = angle1;//angle(buffer_out[i][k]);
                
                    
                } else {
                    buffer_out_r(i,k) = r3;//r1*r2 - img1*img2;
                    *synAnglesp++ = angle1+theta;//angle(buffer_out[i][k]);
                    buffer_out_i(i,k) = img3;//r1*img2 + r2*img1;
                    
                }
#else
                    buffer_out_r(i,k) = r3;//r1*r2 - img1*img2;
                    buffer_out_i(i,k) = img3;//r1*img2 + r2*img1;
                *synAnglesp++ = angle1+theta;//angle(buffer_out[i][k]);
#endif
                    if (k > 0)
                    {
                        buffer_out_r(i,TS_FRAME_LEN-k) =buffer_out_r(i,k);
                        buffer_out_i(i,TS_FRAME_LEN-k) = -buffer_out_i(i,k);
                    }
                    *anaAnglesp++ = angle1;
            
                //}
          
#endif
 
            
        }
        

        
        //txx += ratio;
        buffers_available--;
        i++;
        rbuffer_start_i+=1;
        if (rbuffer_start_i >= TS_BUFFERS)
            rbuffer_start_i -= TS_BUFFERS;

    }
    //std::cout << "pvsample i:" << i<< "\n";

    return i;

}

#ifdef USE_KISS_FFT
int TimeStretcherPL::stft(float *buffer_in, kiss_fft_cpx **buffer_out, int fftlen, int winlen, int hop, float ratio)
#endif
#ifdef USE_OOURA_FFT_WRAPPER
int TimeStretcherPL::stft(float *buffer_in, compbuffer *buffer_out, int fftlen, int winlen, int hop, float ratio)
#endif
{
    double temp;
#ifdef USE_KISS_FFT
    kiss_fft_cpx buf[TS_FRAME_LEN];
#endif
#ifdef USE_OOURA_FFT_WRAPPER
    float buf[TS_FRAME_LEN];
#endif
    int start = 0;
    int i=0;
    int samples = 0;
    while (samplesAvailableIn >= TS_FRAME_LEN)//for(int i=0; i < TS_HOPS; i++)
    {
        //start = 0 + i*TS_HOP;
        i++;
        float energy = 0.0f;
        for (int k=0; k < TS_FRAME_LEN; k++)
        {
            temp = buffer_in[start + k] * win[k];
#ifdef USE_KISS_FFT
            buf[k].i = 0.0;
            buf[k].r = temp;
#endif
#ifdef USE_OOURA_FFT_WRAPPER
            buf[k] = temp;
#endif
            
#if USE_TRANSIENT_DETECTION
            energy += temp*temp;
            
#endif
            /*if (isnan(temp))
             {
             int z = 0;
             z = 0;
             }*/
        }
#if USE_TRANSIENT_DETECTION
            energy = sqrt(energy) / (float) TS_FRAME_LEN;
        
            if (energy - last_energy> TRANSIENT_THRESHOLD && lastTransient > TRANSIENT_FRAMES)
            {
                transientBuffer[rbuffer_index] = 1;
                lastTransient = 0;
            } else {
                transientBuffer[rbuffer_index] = 0;
                lastTransient++;
                
            }
        last_energy = energy;
#endif
        
#ifdef USE_KISS_FFT
        kiss_fft(cfg,buf,buffer_out[rbuffer_index]);
#endif
#ifdef USE_OOURA_FFT_WRAPPER
        fftWrapper->fft(buf, buffer_out[rbuffer_index].re, buffer_out[rbuffer_index].im);
#endif
        /*if (useAngleBuffer)
        {
            for (int kk=0;kk<TS_FRAME_LEN_2_1;kk++)
            {
                angleBuffer[rbuffer_index][kk] = angle(buffer_out[rbuffer_index][kk]);
            }
        }*/
        
        // std::cout << "R.i[0]:" << buffer_out[rbuffer_index][0].r << "R.i[2047]:" << buffer_out[rbuffer_index][2047].r << "\n";
        rbuffer_end_t = rbuffer_end_counter;
        rbuffer_end_i = rbuffer_index;
        rbuffer_end_counter++;

        float next = stft_rem + TS_HOP*ratio;
        int nexti = round(next);
        stft_rem = (float)next - nexti;
        start += nexti;
        rbuffer_start++;
        samplesAvailableIn -= nexti;//TS_HOP;
        samples += nexti;
        anaHopBuffer[rbuffer_index] = nexti;
        //std::cout << "stft i:" << i<< " rbs:"<< nexti << " rbi:" << rbuffer_index <<"\n";
        
        buffers_available++;

        
        
        if (++rbuffer_index >= TS_BUFFERS)
        {
            rbuffer_index = 0;
        }
        
       /* if (rbuffer_end_i >= rbuffer_start_i)
        {
            if (rbuffer_end_t - rbuffer_start_t != rbuffer_end_i - rbuffer_start_i)
            {
                std::cout << "INDEX PROBLEM \n";
            }
        } else {
            if (rbuffer_end_t - rbuffer_start_t !=  TS_BUFFERS - rbuffer_start_i + rbuffer_end_i)
            {
                std::cout << "INDEX PROBLEM \n";
            }
            
        }*/
        
        

        
    }
    //rbuffer_start += TS_HOPS;
    
    /*for b = 0:h:(s-f)
     u = win.*x((b+1):(b+f));
     t = fft(u);
     d(:,c) = t(1:(1+f/2))';
     c = c+1;
     end;*/
    return samples;
}

#ifdef USE_KISS_FFT
int TimeStretcherPL::istft(kiss_fft_cpx **buffer_in, float *buffer_out, int buffers_in)
#endif
#ifdef USE_OOURA_FFT_WRAPPER
int TimeStretcherPL::istft(compbuffer *buffer_in, float *buffer_out, int buffers_in)
#endif
{
#ifdef USE_KISS_FFT
    kiss_fft_scalar buf[TS_FRAME_LEN];
#endif
#ifdef USE_OOURA_FFT_WRAPPER
    float buf[TS_FRAME_LEN];
#endif
    int start = 0;
    
    /* double buffer1[TS_FRAME_LEN];
     double buffer2[TS_FRAME_LEN];
     double buffer3[TS_FRAME_LEN];
     double buffer4[TS_FRAME_LEN];
     
     double *bufferz = &buffer1[0];*/
    
    for(int i=0; i < buffers_in; i++)
    {
#ifdef USE_KISS_FFT
        kiss_fftri(icfg,buffer_in[i],buf);
#endif
#ifdef USE_OOURA_FFT_WRAPPER
        fftWrapper->ifft(buf, buffer_in[i].re, buffer_in[i].im);
#endif
        start = samplesAvailableOut + i*TS_HOP;
        
        float energy = 0;
        
         //std::cout << "i:" << i << " start:" << start  <<"\n";
        
        for (int k=0; k < TS_FRAME_LEN; k++)
        {
            // bufferz[k] = buf[k].r *win[k]* 0.66 / TS_FRAME_LEN.0;
            buffer_out[start+k] += buf[k] *win[k]*OUTPUT_GAIN;// * ONE_PER_TS_FRAME_LEN_066;//*0.66 *win[k]/ TS_FRAME_LEN.0;//;
            /*if (fabs(buffer_out[start+k]-samplemem)> 0.1 && k> 0)
            {
                 std::cout << "i:" << i << " start:" << rbuffer_start_t  <<" k:" << k <<"\n";
                
            }*/
            //samplemem = buf[k];//buffer_out[start+k];
            //energy += samplemem*samplemem;
        }
        
        if (energy < 0.1)
        {
            //std::cout << "energy:" << energy << "\n";
           
        }

        /*  switch(i)
         {
         case 0:
         bufferz = &buffer2[0];
         break;
         case 1:
         bufferz = &buffer3[0];
         break;
         case 2:
         bufferz = &buffer4[0];
         break;
         }*/
    }
   // std::cout << "buffers_in:" << buffers_in << "\n";

    return buffers_in * TS_HOP;
    
    /* win = 2/3*win;
     xlen = ftsize + (cols-1)*h;
     x = zeros(1,xlen);
     
     for b = 0:h:(h*(cols-1))
     ft = d(:,1+b/h)';
     ft = [ft, conj(ft([((ftsize/2)):-1:2]))];
     px = real(ifft(ft));
     x((b+1):(b+ftsize)) = x((b+1):(b+ftsize))+px.*win;
     end;*/
}
int TimeStretcherPL::GetInputFrameSize(float tsCoeff, int blen)
{
   // float targetBufferSize = tsCoeff * (float)blen;
    
    float samplesNeeded = (float)(blen - samplesAvailableOut);
    
    int  framesOut = (int)ceil(samplesNeeded / (float) TS_HOP);
    int framesIn = 0;
    if (framesOut > 0)
    {
    
        framesIn = 1 + (int)ceil((framesOut-1) * tsCoeff);// - buffers_available;
    }
    
    int samplesNeededIn =  framesIn * TS_HOP - samplesAvailableIn + TS_FRAME_LEN-TS_HOP;
    if (samplesNeededIn + samplesAvailableIn < buflen && framesIn > 0)
        samplesNeededIn = buflen - samplesAvailableIn;
    if (samplesNeededIn < 0)
        samplesNeededIn = 0;
   // std::cout << "Samples needed:" << samplesNeededIn << " (blen:" << blen <<" sout:" << samplesAvailableOut << " sin:"<<samplesAvailableIn << "\n";
    
    return samplesNeededIn;
}
/*
int TimeStretcherPL::Preprocess(float* buffer, int inBufferLen, FILE* preFile)
{
    if (inBufferLen > 0)//if (samplesAvailableIn < buflen)
    {
        //memset(intbuffer3 + buflen + samplesAvailableIn,0,sizeof(float)*inBufferLen);
        memcpy(intbuffer3 + samplesAvailableIn,buffer,sizeof(float)*inBufferLen);
        samplesAvailableIn+= inBufferLen;
    }
    
    if (samplesAvailableIn >= TS_FRAME_LEN)
    {
        
        int buffers = stft(intbuffer3, intbuffer1, buflen, buflen, hopsize, 1.0);
        memmove(intbuffer3,intbuffer3 + buffers*TS_HOP,sizeof(float)*(outbuffer_len-buffers*TS_HOP));
        float buffer[TS_FRAME_LEN*2];
        
        for (int i=0; i < buffers; i++)
        {
            kiss_fft_cpx *buf = intbuffer1[rbuffer_start_i++];
            if (rbuffer_start_i >= TS_BUFFERS)
                rbuffer_start_i = 0;
            
            for (int k=0; k < TS_FRAME_LEN_2_1;k++)
            {
                float r = absri(buf[k]);
                float a = angle(buf[k]);
                buffer[k*2] = r;
                buffer[k*2+1] = a;
            }
            fwrite(&buffer[0], 1, sizeof(float)*(TS_FRAME_LEN+2), preFile);
        }
    
    }
    
    return inBufferLen;
}

int TimeStretcherPL::Process(FILE* preFile, int outBufferLen, float tsCoeff)
{
    float samplesNeeded = (float)(outBufferLen - samplesAvailableOut);
    
    int  framesOut = (int)ceil(samplesNeeded / (float) TS_HOP);
    int framesIn = (int)ceil(framesOut * tsCoeff);// - buffers_available;
    
    int framesAvailable = floor((float)(samplesAvailableIn-TS_FRAME_LEN+TS_HOP)/(float)TS_HOP);
    if (framesAvailable < 0)
        framesAvailable = 0;
    int framesNeeded = framesIn - framesAvailable;
    
    // load preprocessed fft data
    
    
    return 0;
}*/
