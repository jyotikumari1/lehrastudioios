//
//  TimeStretch.h
//  eLehra
//
//  Created by Jukka Rauhala on 06/05/15.
//  Copyright (c) 2015 AK Studios. All rights reserved.
//

#ifndef __eLehra__TimeStretchPL__
#define __eLehra__TimeStretchPL__

#include <stdio.h>

#define USE_KISS_FFT
//#define USE_OOURA_FFT_WRAPPER


#ifdef USE_OOURA_FFT_WRAPPER
//#define AUDIOFFT_APPLE_ACCELERATE
//#define AUDIOFFT_APPLE_ACCELERATE_USED
#define AUDIOFFT_OOURA
#include "../FFT/AudioFFT.h"
#include <stdlib.h>
#include <math.h>
#include <string.h>
struct compbuffer {
    float* re;
    float* im;
};
#define OUTPUT_GAIN 2000.0f
#endif

#ifdef USE_KISS_FFT
#include "../KissFFT/kiss_fft.h"
#include "../KissFFT/kiss_fftr.h"
#define OUTPUT_GAIN 1.0f
#endif

#define TS_FRAME_LEN    2048//512//512
#define TS_HOP           512//128//128//64
#define TS_BUFFERS        60
#define TS_HOPS           TS_FRAME_LEN / TS_HOP
#define TS_FRAME_LEN_2_1 1025

struct fftvalue {
    float r; // magnitude
    float a; // angle
};

// Improved version of the time stretching component

class TimeStretcherPL
{
    
public:
    TimeStretcherPL();
    ~TimeStretcherPL();
    
    int GetInputFrameSize(float tsCoeff, int bufferLen);
    
    
    int Process(float* buffer, int inBufferLen, int outBufferLen, float tsCoeff);
    void Initialize();
    
    //int Preprocess(float* buffer, int inBufferLen, FILE* preFile);
    //int Process(FILE* preFile, int outBufferLen, float tsCoeff);
    
private:
    
#ifdef USE_KISS_FFT
    int pvsample(kiss_fft_cpx **buffer_in, kiss_fft_cpx **buffer_out,double* tvec, int hop, float ratio);
    int stft(float *buffer_in, kiss_fft_cpx **buffer_out, int fftlen, int winlen, int hop, float ratio);
    int istft(kiss_fft_cpx **buffer_in, float *buffer_out, int buffers_in);
   // double angle(kiss_fft_cpx value);
#endif
#ifdef USE_OOURA_FFT_WRAPPER
    int pvsample(compbuffer *buffer_in, compbuffer *buffer_out,double* tvec, int hop, float ratio);
    int stft(float *buffer_in, compbuffer *buffer_out, int fftlen, int winlen, int hop, float ratio);
    int istft(compbuffer *buffer_in, float *buffer_out, int buffers_in);
#endif
        double angle(double rvalue,double ivalue);
    float atanf2(float x);

    double total_ratio;
    int buflen, hopsize;
    // double fs = 44100;
    
    fftvalue** intbuffer5;
    float** angleBuffer;
    bool useAngleBuffer;
    float* intbuffer3;
    float* intbuffer4;
    int anaHopBuffer[TS_BUFFERS];
    int transientBuffer[TS_BUFFERS];
    //double intbuffer5[4096];
    double* timevec;
    double* win;
#ifdef USE_KISS_FFT
    kiss_fft_cfg cfg;
    kiss_fftr_cfg icfg;
    kiss_fft_cpx **intbuffer1;
    kiss_fft_cpx **intbuffer2;
#endif
    
#ifdef USE_OOURA_FFT_WRAPPER
    compbuffer *intbuffer1;
    compbuffer *intbuffer2;
    audiofft::AudioFFT* fftWrapper;
#endif
    
    double txx;
    float ph[1+TS_FRAME_LEN/2];
    float anaAngles[1+TS_FRAME_LEN/2];
    float synAngles[1+TS_FRAME_LEN/2];
    float omega[1+TS_FRAME_LEN/2];
    int rbuffer_start;
    int rbuffer_index;
    
    int outbuffer_len;
    bool is_first_frame;// = true;
    bool bypass;// = true;
    int samplesAvailableIn;// = 0;
    int samplesAvailableOut;// = 0;
    int samplesOut;//=512;
    int samplesBeforeSconv;// = 512;

    long rbuffer_start_t, rbuffer_end_t;
    int rbuffer_start_i, rbuffer_end_i;
    long rbuffer_end_counter;
    int buffers_available;// = 0;
    
    float samplemem;// = 0;
    
    float stft_rem;// = 0;
    float last_energy;// = 0;
    int lastTransient;// = 30;
};

#endif /* defined(__eLehra__TimeStretch__) */
