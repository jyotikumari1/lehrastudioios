//
//  OggVorbisDecoder.h
//  OggVorbis_iOS
//
//  Created by Jukka Rauhala on 1/31/14.
//  Copyright (c) 2014 Soundical. All rights reserved.
//

#ifndef __OggVorbis_iOS__OggVorbisDecoder__
#define __OggVorbis_iOS__OggVorbisDecoder__

#include <iostream>
#include "OggVorbis-Prefix.pch"

#include <iostream>
#include <string>
#include "include/vorbis/codec.h"
#include "include/vorbis/vorbisenc.h"
#include "include/ogg/ogg.h"
//#include <IAudioEngine.h>
typedef unsigned char byte;
//using namespace AudioEngineInterface;

class OggVorbisDecoder  {
public:
    int Initialize(FILE* f, int samplerate);//byte* buffer_out, int buflen_out);
    int Uninitialize();
    int Decode(double*  buffer_out, int buflen_in, FILE* f);//byte*  buffer_out, int buflen_out);
    //void CatchException(String^ func, String^ message);
    
    bool IsEof();
    
private:
    ogg_sync_state   oy; /* sync and verify incoming physical bitstream */
    ogg_stream_state os; /* take physical pages, weld into a logical
                          stream of packets */
    ogg_page         og; /* one Ogg bitstream page.  Vorbis packets are inside */
    ogg_packet       op; /* one raw packet of data for decode */
    
    vorbis_info      vi; /* struct that stores all the static vorbis bitstream
                          settings */
    vorbis_comment   vc; /* struct that stores all the user comments */
    
    vorbis_dsp_state vd; /* central working state for the packet->PCM decoder */
    vorbis_block     vb; /* local working space for packet->PCM decode */
    
    int eos;
    int convsize;
    
    int mode;
    
};

#endif /* defined(__OggVorbis_iOS__OggVorbisDecoder__) */
