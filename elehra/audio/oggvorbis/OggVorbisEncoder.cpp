﻿
#include "OggVorbisEncoder.h"





int OggVorbisEncoder::Initialize(FILE* f, int samplerate)//byte*  _buffer_out, int buflen_out)
{
    int eos=0,ret;
	//byte* buffer_out;// = _buffer_out->Data;
	  vorbis_info_init(&vi);
	   ret=vorbis_encode_init_vbr(&vi,1,samplerate,0.4);//-1,128000,-1);
	   if (ret)
	   {
		   return ret;
	   }
	    vorbis_comment_init(&vc);
  vorbis_comment_add_tag(&vc,"ENCODER","encoder_example.c");

  /* set up the analysis state and auxiliary encoding storage */
  vorbis_analysis_init(&vd,&vi);
  vorbis_block_init(&vd,&vb);

  /* set up our packet->stream encoder */
  /* pick a random serial number; that way we can more likely build
     chained streams just by concatenation */
  //srand(time(NULL));
  ogg_stream_init(&os,rand());

    {
    ogg_packet header;
    ogg_packet header_comm;
    ogg_packet header_code;

    vorbis_analysis_headerout(&vd,&vc,&header,&header_comm,&header_code);
    ogg_stream_packetin(&os,&header); /* automatically placed in its own
                                         page */
    ogg_stream_packetin(&os,&header_comm);
    ogg_stream_packetin(&os,&header_code);

    /* This ensures the actual
     * audio data will start on a new page, as per spec
     */
	ret = 0;
    while(!eos){
      int result=ogg_stream_flush(&os,&og);
      if(result==0)break;
      fwrite(og.header,1,og.header_len,f);
      fwrite(og.body,1,og.body_len,f);
	  //memcpy(buffer_out + ret, og.header, og.header_len);
	  ret += og.header_len;
	  //memcpy(buffer_out + ret, og.body, og.body_len);
	  ret += og.body_len;
    }

  }
	return ret;
}

int OggVorbisEncoder::Encode(short*  _buffer_in, int buflen_in, FILE *f)//byte* _buffer_out, int buflen_out)
{

    int eos=0,ret;
  //int i, founddata;
  	//byte* buffer_out = _buffer_out;//->Data;
	short* buffer_in = _buffer_in;//->Data;

    /********** Encode setup ************/



  /* choose an encoding mode.  A few possibilities commented out, one
     actually used: */

  /*********************************************************************
   Encoding using a VBR quality mode.  The usable range is -.1
   (lowest quality, smallest file) to 1. (highest quality, largest file).
   Example quality mode .4: 44kHz stereo coupled, roughly 128kbps VBR

   ret = vorbis_encode_init_vbr(&vi,2,44100,.4);

   ---------------------------------------------------------------------

   Encoding using an average bitrate mode (ABR).
   example: 44kHz stereo coupled, average 128kbps VBR

   ret = vorbis_encode_init(&vi,2,44100,-1,128000,-1);

   ---------------------------------------------------------------------

   Encode using a quality mode, but select that quality mode by asking for
   an approximate bitrate.  This is not ABR, it is true VBR, but selected
   using the bitrate interface, and then turning bitrate management off:

   ret = ( vorbis_encode_setup_managed(&vi,2,44100,-1,128000,-1) ||
           vorbis_encode_ctl(&vi,OV_ECTL_RATEMANAGE2_SET,NULL) ||
           vorbis_encode_setup_init(&vi));

   *********************************************************************/

  

  /* do not continue if setup failed; this can happen if we ask for a
     mode that libVorbis does not support (eg, too low a bitrate, etc,
     will return 'OV_EIMPL') */

  //if(ret)exit(1);

  /* add a comment */


  /* Vorbis streams begin with three headers; the initial header (with
     most of the codec setup parameters) which is mandated by the Ogg
     bitstream spec.  The second header holds any comment fields.  The
     third header holds the bitstream codebook.  We merely need to
     make the headers, then pass them to libvorbis one at a time;
     libvorbis handles the additional Ogg bitstream constraints */
  ret = 0;


  //while(!eos){
    long i;
	long bytes=(long)buflen_in;//fread(readbuffer,1,READ*4,stdin); /* stereo hardwired here */

    if(bytes==0){
      /* end of file.  this can be done implicitly in the mainline,
         but it's easier to see here in non-clever fashion.
         Tell the library we're at end of stream so that it can handle
         the last frame and mark end of stream in the output properly */
      vorbis_analysis_wrote(&vd,0);

    }else{
      /* data to encode */

      /* expose the buffer to submit data */
      float **buffer=vorbis_analysis_buffer(&vd,buflen_in / 2);

      /* uninterleave samples */
      for(i=0;i<bytes/2;i++){

		//short shortSample = (short)(buffer_in[i*2] + (((int)buffer_in[i*2 + 1]) << 8));
		buffer[0][i] = (float)buffer_in[i] / 32768.f;
       // buffer[0][i]=((buffer_in[i*2+1]<<8)|
        //              (0x00ff&(int)buffer_in[i*2]))/32768.f;
      //  buffer[1][i]=((readbuffer[i*4+3]<<8)|
      //                (0x00ff&(int)readbuffer[i*4+2]))/32768.f;
      }

      /* tell the library how much we actually submitted */
      vorbis_analysis_wrote(&vd,(int)i);
    }

    /* vorbis does some data preanalysis, then divvies up blocks for
       more involved (potentially parallel) processing.  Get a single
       block for encoding now */
    while(vorbis_analysis_blockout(&vd,&vb)==1){

      /* analysis, assume we want to use bitrate management */
      vorbis_analysis(&vb,NULL);
      vorbis_bitrate_addblock(&vb);

      while(vorbis_bitrate_flushpacket(&vd,&op)){

        /* weld the packet into the bitstream */
        ogg_stream_packetin(&os,&op);

        /* write out pages (if any) */
        while(!eos){
          int result=ogg_stream_pageout(&os,&og);
          if(result==0)break;
          fwrite(og.header,1,og.header_len,f);
          fwrite(og.body,1,og.body_len,f);
		  //memcpy(buffer_out + ret, og.header, og.header_len);
		  ret += og.header_len;
		  //memcpy(buffer_out + ret, og.body, og.body_len);
		  ret += og.body_len;
          /* this could be set above, but for illustrative purposes, I do
             it here (to show that vorbis does know where the stream ends) */

          if(ogg_page_eos(&og))eos=1;
        }
      }
    }
 // }

  /* clean up and exit.  vorbis_info_clear() must be called last */
  return ret;

}

int OggVorbisEncoder::Uninitialize()
{
	//ogg_page_eos(&og);
  ogg_stream_clear(&os);
  vorbis_block_clear(&vb);
  vorbis_dsp_clear(&vd);
  vorbis_comment_clear(&vc);
  vorbis_info_clear(&vi);

  return 0;
}
/*
void OggVorbisEncoder::CatchException(String^ func, String^ message)
{
	SYSTEMTIME st;
    
    GetSystemTime(&st);
	wchar_t tempf[90];
	swprintf(tempf,  90,L"\\%04d-%02d-%02d-%02d-%02d-%02d-%04d.exception",(int)st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);

	FILE *f1;
			    auto local = Windows::Storage::ApplicationData::Current->LocalFolder;
				std::wstring str;
				str = local->Path->Data();
				str += tempf;

				_wfopen_s(&f1, str.c_str(), L"w");
			fwrite(func->Data(), 1, wcslen(func->Data()) * sizeof(wchar_t),f1);
			fwrite(message->Data(), 1, wcslen(message->Data()) * sizeof(wchar_t),f1);
					fclose(f1);
}*/