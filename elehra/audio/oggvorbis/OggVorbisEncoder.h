﻿#pragma once
//#include "pch.h"
//#include <collection.h> 
//#include <ppltasks.h>
#include "OggVorbis-Prefix.pch"

#include <iostream>
#include <string>
#include <stdlib.h>
#include "include/vorbis/codec.h"
#include "include/vorbis/vorbisenc.h"
#include "include/ogg/ogg.h"
//#include <IAudioEngine.h>
typedef unsigned char byte;
//using namespace AudioEngineInterface;

	class OggVorbisEncoder  {
		public:
        int Initialize(FILE* f, int samplerate);//byte* buffer_out, int buflen_out);
			int Uninitialize();
        int Encode(short*  buffer_in, int buflen_in, FILE* f);//byte*  buffer_out, int buflen_out);
			//void CatchException(String^ func, String^ message);

	private:
			  ogg_stream_state os; /* take physical pages, weld into a logical
                          stream of packets */
  ogg_page         og; /* one Ogg bitstream page.  Vorbis packets are inside */
  ogg_packet       op; /* one raw packet of data for decode */

  vorbis_info      vi; /* struct that stores all the static vorbis bitstream
                          settings */
  vorbis_comment   vc; /* struct that stores all the user comments */

  vorbis_dsp_state vd; /* central working state for the packet->PCM decoder */
  vorbis_block     vb; /* local working space for packet->PCM decode */

	};

