//
//  main.m
//  eLehra
//
//  Created by Aman Kalyan on 21/04/2015.
//  Copyright (c) 2015 AK Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
